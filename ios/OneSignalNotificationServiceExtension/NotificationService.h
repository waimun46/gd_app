//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by waimun on on 1/13/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
