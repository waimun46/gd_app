package com.golden.myapp;

import android.app.Application;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.facebook.react.ReactApplication;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.lewin.qrcode.QRScanReaderPackage;
import org.wonday.pdf.RCTPdfView;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.reactcommunity.rnlanguages.RNLanguagesPackage;
import kim.taegon.rnintl.ReactNativeIntlPackage;
import com.imagepicker.ImagePickerPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.rnfs.RNFSPackage;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import org.reactnative.camera.RNCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.brentvatne.react.ReactVideoPackage;




import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RNCWebViewPackage(),
            new RNFetchBlobPackage(),
            new VectorIconsPackage(),
            new ReactNativeRestartPackage(),
            new QRScanReaderPackage(),
            new RCTPdfView(),
            new RNLanguagesPackage(),
            new ReactNativeIntlPackage(),
            new ImagePickerPackage(),
            new RNGestureHandlerPackage(),
            new RNGeocoderPackage(),
            new RNFSPackage(),
            new RNFileViewerPackage(),
            new RNCameraPackage(),
            new AsyncStoragePackage(),
            new ReactVideoPackage(),
            new ReactNativeOneSignalPackage()
            
            
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }

}
