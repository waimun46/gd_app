/************************* Homepage image Api *************************/
export function TourImgApi() {
    let Url = "https://goldendestinations.com/api/new/tour_highlights.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* Agent List Api *************************/
export function AgentListApi(type) {
    let Url = "https://goldendestinations.com/api/new/agent.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&title=";
    return new Promise((resolve, reject) => {
        fetch(Url + type, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* News List Api *************************/
export function NewsArticleApi() {
    let Url = "https://goldendestinations.com/api/new/news.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* Blog List Api *************************/
export function BlogArticleApi() {
    let Url = "https://goldendestinations.com/api/new/blog.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* Promotions List Api *************************/
export function PromotionArticleApi() {
    let Url = "https://goldendestinations.com/api/new/promotions.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error); 
                reject(error)
            })
    })
}

/************************* Destination Api *************************/
export function DestinationApi(type) {
    let Url = "https://goldendestinations.com/api/new/destination.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&type=";
    return new Promise((resolve, reject) => {
        fetch(Url + type, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* GD_Consortiums Api *************************/
export function GDConsortiumsApi() {
    let Url = "https://goldendestinations.com/api/new/gdconsortiums.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* Destination Tour Get Detial Api *************************/
export function TourGetDetialApi() {
    let Url = "https://goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&end=";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}


/************************* Homepage Tour Get Month Detial Api *************************/
export function MonthGetDetialApi() {
    let Url = "https://goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&end=";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}


/************************* Currency List Api *************************/
export function CurrencyListApi() {
    let Url = "https://goldendestinations.com/api/new/currency_list.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/*************************  UserProfile Api *************************/
export function UserProfileApi(token) {
    let Url = "https://goldendestinations.com/api/new/user_profile.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&access_token=";
    return new Promise((resolve, reject) => {
        fetch(Url + token, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}


/*************************  DepartingDetail List Api *************************/
export function DepartingDetailApi(id) {
    let Url = "https://goldendestinations.com/api/new/departing_detail.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&id=";
    return new Promise((resolve, reject) => {
        fetch(Url + id, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}



/************************* ImageWelcome Api *************************/
export function ImageWelcome() {
    let Url = "https://www.goldendestinations.com/api/new/slide.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}


/************************* state Api *************************/
export function StateApi() {
    let Url = "https://www.goldendestinations.com/api/new/state.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}


/************************* terms Api *************************/
export function TermsApi() {
    let Url = "https://www.goldendestinations.com/api/new/terms.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* privacy Api *************************/
export function PrivacyApi() {
    let Url = "https://www.goldendestinations.com/api/new/privacy.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}

/************************* gtrip Api *************************/
export function GtripApi() {
    let Url = "https://www.goldendestinations.com/api/new/gtrip.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}


/************************* gtrip Api *************************/
export function LandingApi() {
    let Url = "https://www.goldendestinations.com/api/new/topslider.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW";
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: 'GET'
        })
            .then((res) => res.json())
            .then((resJson) => {
                //console.log(resJson);
                resolve(resJson);
            })
            .catch((error) => {
                console.log(error);
                reject(error)
            })
    })
}





