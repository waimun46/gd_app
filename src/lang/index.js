import RNLanguages from 'react-native-languages';
import i18n from 'i18n-js';
import AsyncStorage from '@react-native-community/async-storage';

import en from './locales/en.json';
import zh from './locales/zh.json';


determinLanguage = async () => {
  try {
    global.language = await AsyncStorage.getItem("language")
  } catch (error) {
    global.language = "en"
    console.error(error)
  }
  console.info("== language is : " + global.language)
}

determinLanguage()

i18n.fallbacks = true;
i18n.translations = { en, zh };

global.t = (key) => {
  return i18n.t(key, { locale: global.language })
}

export default i18n;