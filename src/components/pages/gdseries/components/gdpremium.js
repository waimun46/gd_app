import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Dimensions, TouchableOpacity, ImageBackground, } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Text } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import CountryData from './components/countrydata';
import { DestinationApi, GtripApi } from '../../../../../PostApi';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';


const numColumns = 3;

const dataImg = [
   {
      "imgrp": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e"
   }
]

class GDPremium extends Component {

   constructor(props) {
      super(props);
      this.state = {
         Type_id: 0,
         dataU: [],
         uniqueFilterData: [],
         langPrint: '',
         gtripData: [],
         typeID: '',

      }
   }

   componentDidMount() {
      /************************************ Get Language Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem("language").then(langStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Note')
         this.setState({
            langPrint: langStorageRes
         })
      })

      AsyncStorage.getItem("SAVE_TYPE").then(typeStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Note')
         this.setState({
            typeID: typeStorageRes
         })
      })
      this.gtripData();
   }

   /******************************************* gtripData api *******************************************/
   gtripData() {
      GtripApi().then((fetchData) => {
         this.setState({
            gtripData: fetchData
         })
      })
   }

   /************************* Render Item in FlatList *************************/
   renderItem = ({ item }) => {
      console.log(lang, 'GDPremium------------imgrp-------item');
      const lang = this.state.langPrint;
      return (
         // item.pic_url === "" ? null :
         <View style={{ width: '33.33%', }}>
            <TouchableOpacity onPress={() => Actions.countryinfor({ data: item })} style={{ width: '100%', }}>
               <View>
                  <ImageBackground source={{ uri: item.pic_url === "" ? dataImg[0].imgrp : item.pic_url }} style={styles.item}>
                     <Text style={styles.TextImage}>{lang === 'zh' ? item.title_cn : item.title}</Text>
                  </ImageBackground>
               </View>
            </TouchableOpacity>

         </View>
      )
   }

   /************************* renderItemGtrip Item in FlatList  *************************/
   renderItemGtrip = ({ item }) => {
      console.log(lang, 'GDPremium------------imgrp-------item');
      const lang = this.state.langPrint;
      return (
         // item.pic_url === "" ? null :
         <View style={{ width: '33.33%', }}>
            <TouchableOpacity onPress={() => Actions.gtriplist({ data: item })} style={{ width: '100%', }}>
               <View>
                  <ImageBackground source={{ uri: item.pic_url === "" ? dataImg[0].imgrp : item.pic_url }} style={styles.item}>
                     <Text style={styles.TextImage}>{lang === 'zh' ? item.title_cn : item.title}</Text>
                  </ImageBackground>
               </View>
            </TouchableOpacity>

         </View>
      )
   }

   render() {
      const { langPrint, uniqueFilterData, gtripData, typeID } = this.state;
      const htmlContent_en = this.props.data[0].type_description;
      const htmlContent_cn = this.props.data[0].type_description_cn;

      //console.log(this.props.data[0].country_list, '--------props')
      const unique = this.state.dataU;
      this.props.data[0].country_list.map(x => unique.filter(a => a.country_list == x.destination_list).length > 0 ? null : unique.push(x));

      const uniqueFilter = this.state.uniqueFilterData;
      unique.map(x => uniqueFilter.filter(a => a.destination_list == x.destination_list).length > 0 ? null : uniqueFilter.push(x));

      console.log(typeID, 'typeID');

      console.log('gtripData', gtripData)

      return (
         <View style={{ backgroundColor: 'white' }}>
            <ScrollView>
               <View >
                  <Image source={{ uri: this.props.data[0].type_pic }} style={styles.PremiumImage} />
               </View>
               <View>
                  <Tabs style={{ backgroundColor: 'white' }} locked tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}>
                     <Tab
                        heading={global.t('Information')}
                        tabStyle={{ backgroundColor: "white" }}
                        textStyle={{ color: '#ccc', }}
                        activeTextStyle={{ color: '#de2d30' }}
                        activeTabStyle={{ backgroundColor: "white", }}
                        tabContainerStyle={{ backgroundColor: "#de2d30" }}

                     >
                        <View style={{ paddingBottom: 100 }}>
                           <HTML html={langPrint === 'zh' ? htmlContent_cn : htmlContent_en}
                              imagesInitialDimensions={{ width: '60%', height: 200 }}
                              tagsStyles={{
                                 p: { width: '100%' },
                                 h2: { padding: 20, fontSize: 16, marginBottom: -30 },
                                 em: { fontSize: 30, },
                              }}
                              ignoredStyles={['font-family', 'display',]}
                              ignoredTags={[...IGNORED_TAGS, 'iframe', 'img',]}
                           />
                        </View>

                     </Tab>

                     <Tab heading={global.t('Country')}
                        tabStyle={{ backgroundColor: "white" }}
                        textStyle={{ color: '#ccc', }}
                        activeTextStyle={{ color: '#de2d30' }}
                        activeTabStyle={{ backgroundColor: "white", }}
                        tabContainerStyle={{ backgroundColor: "#de2d30" }}
                     >
                        {
                           typeID === '4' ? (
                              <FlatList
                                 data={gtripData}
                                 renderItem={this.renderItemGtrip}
                                 numColumns={numColumns}
                                 extraData={this.state}
                                 style={styles.Flatcontainer}
                                 contentContainerStyle={{
                                    marginBottom: 50
                                 }}

                              />
                           ) : (
                                 <FlatList
                                    data={uniqueFilter}
                                    renderItem={this.renderItem}
                                    numColumns={numColumns}
                                    extraData={this.state}
                                    style={styles.Flatcontainer}
                                    contentContainerStyle={{
                                       marginBottom: 50
                                    }}

                                 />
                              )
                        }
                     </Tab>

                  </Tabs>
               </View>
            </ScrollView>
         </View>
      );
   }
}


export default GDPremium;

const styles = StyleSheet.create({
   PremiumImage: { width: "100%", resizeMode: 'cover', height: 180, },
   Flatcontainer: { flex: 1, marginVertical: 10, },
   item: { alignItems: 'center', justifyContent: 'center', flex: 1, margin: 2, height: Dimensions.get('window').width / numColumns, },
   TextImage: {
      fontSize: 18, color: 'white', fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
      textShadowRadius: 2, textShadowColor: '#000', textAlign: 'center'
   },


})
