import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, ActivityIndicator, TouchableWithoutFeedback } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconFOU from "react-native-vector-icons/Foundation";
import { TourGetDetialApi } from '../../../../../../PostApi';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import IconANT from "react-native-vector-icons/AntDesign";

keyExtractor = (item) => item.key;




class CountryInforList extends Component {

   constructor(props) {
      super(props);
      this.state = {
         data: [],
         dataU: [],
         isLoading: true,
         fetching_Status: false,
         dataInfor: [],
         type: 0,
         country: this.props.country_id,
         keywordId: this.props.dataTour.keyword,
         langPrint: ''
      }
      //this.page = 0
      //console.log(this.props);
   }

   componentDidMount() {

      /************************************ Get Language Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem("language").then(langStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Note')
         this.setState({
            langPrint: langStorageRes
         })
      })


      /************************************ Get Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem('SAVE_TYPE').then(asyncStorageRes => {
         console.log(asyncStorageRes, '----------------AsyncStorage')
         this.setState({
            type: asyncStorageRes
         })
         //console.log(this.state.tokenId, '-------------fetchDataApi1111')
         this.fetchApiTour();
      });


      // this.page = this.page + 1;
      // /************************* Fetch Data in Apitour *************************/
      // fetch("https://goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&limit=50" + this.page)
      //     .then((response) => response.json())
      //     .then((responseJson) => {
      //         console.log(responseJson, 'api');
      //         this.setState({ data: [...this.state.data, ...responseJson], isLoading: false });
      //     })
      //     .catch((error) => {
      //         console.error(error);
      //     });

   }



   fetchApiTour(type = this.state.type, country = this.state.country, keyword = this.state.keywordId) {
      console.log('CountryInforList - fetchApiTour');
      console.log(this.props);
      console.log(keyword, type, country, 'keywordfetchApiTour');
      let Url = `https://goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&type=${type}&country=${country}&keyword=${keyword}`;

      fetch(Url)
         .then(res => res.json())
         .then(json => {
            console.log(json, 'fetchApiTour');
            this.setState({
               dataInfor: json,
               isLoading: false,


            });
         });
   }



   // /************************* Load More Date *************************/
   // fetch_more_data = () => {
   //     this.page = this.page + 1;

   //     this.setState({ fetching_Status: true }, () => {
   //         fetch('https://goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&limit=50' + this.page)
   //             .then((response) => response.json())
   //             .then((responseJson) => {
   //                 this.setState({ data: [...this.state.data, ...responseJson], fetching_Status: false });
   //             })
   //             .catch((error) => {
   //                 console.error(error);
   //             });

   //     });
   // }

   // /************************* Render Footer by onPress load more data *************************/
   // Render_Footer = () => {
   //     return (
   //         <View style={styles.footerStyle}>
   //             <TouchableOpacity
   //                 activeOpacity={0.7}
   //                 style={styles.TouchableOpacity_style}
   //                 onPress={this.fetch_more_data}
   //             >
   //                 <Text style={styles.TouchableOpacity_Inside_Text}>{global.t('More_List')} ></Text>
   //                 {
   //                     (this.state.fetching_Status)
   //                         ?
   //                         <ActivityIndicator color="#000" style={{ marginLeft: 6 }} />
   //                         :
   //                         null
   //                 }
   //             </TouchableOpacity>
   //         </View>
   //     )
   // }


   /************************* Render Footer by onScroll load more data *************************/
   // _scrolled() {
   //     this.setState({ fetching_Status: true })
   // }

   // loadMore = () => {
   //     {
   //         (this.state.fetching_Status)
   //             ?
   //             <ActivityIndicator color="#000" size="large" />
   //             :
   //             null
   //     }
   // }



   // /************************* Download PDF File *************************/
   // downloadFile(item) {
   //     function getLocalPath(url) {
   //         const filename = url.split('/').pop();
   //         return `${RNFS.DocumentDirectoryPath}/${filename}`;
   //     }

   //     const url = item.itinerary;
   //     const localFile = getLocalPath(url);

   //     const options = {
   //         fromUrl: url,
   //         toFile: localFile
   //     };
   //     RNFS.downloadFile(options).promise
   //         .then(() => FileViewer.open(localFile))
   //         .then(() => {
   //             // success
   //         })
   //         .catch(error => {
   //             // error
   //         });
   // }



   /************************* RenderItem in FlatList *************************/
   renderItem = ({ item }) => {
      //const items = item;
      //console.log(items, 'items')
      //console.log(this.state.keywordId, 'CountryInforList------item')
      //console.log(items.title, 'CountryInforList------titleid')
      const { langPrint } = this.state;

      return (
         <List style={{ backgroundColor: 'white' }}>
            <ListItem thumbnail>
               <Body style={{ marginLeft: 0 }}>
                  <Text note style={styles.TextTitle}>{langPrint === 'zh' ? item.cn_title : item.title}</Text>
                  <TouchableOpacity
                     onPress={() => Actions.viewdetails({
                        dataInfor: item,
                        type_id: this.props.type_id,
                        country_id: this.state.country,
                        keyword_Id: this.props.dataTour.keyword,
                     })}>
                     <Text style={styles.viewmore} uppercase={false}>
                        <IconANT name="infocirlceo" size={12} /> {global.t('More_Details')} >
                     </Text>
                  </TouchableOpacity>
               </Body>
            </ListItem>
         </List>
      )
   }


   ListEmpty = () => {
      return (
         <View style={styles.MainContainer}>
            <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
            <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
         </View>
      );
   };


   render() {

      const { isLoading } = this.state;

      const unique = this.state.dataU;
      this.state.dataInfor.map(x => unique.filter(a => a.title == x.title).length > 0 ? null : unique.push(x));
      console.log(unique, 'unique');

      console.log(this.state.type, '------------type')

      return (
         <Content>

            {
               isLoading ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                     <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                     <ActivityIndicator size="large" color="#de2d30" />
                  </View>
               ) : (
                     <FlatList
                        //onScroll={this._scrolled.bind(this)}
                        data={unique}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}
                        extraData={this.state}
                        //onEndReached={this.fetch_more_data}
                        //onEndReachedThreshold={0.5}
                        //ListFooterComponent={this.Render_Footer}
                        ListEmptyComponent={this.ListEmpty}
                     />
                  )
            }
         </Content>
      );
   }
}


export default CountryInforList;

const styles = StyleSheet.create({
   IconColor: { color: '#de2d30' },
   TextTitle: { fontWeight: 'bold', color: '#606c77' },
   TextPrice: { marginTop: 5, color: '#00953b' },
   TouchableOpacity_Inside_Text: { padding: 30, textAlign: 'right', color: '#de2d30' },
   viewmore: { color: '#de2d30', paddingRight: 10, paddingLeft: 8, textAlign: 'right', marginTop: 10, fontSize: 14, },
   MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },

})
