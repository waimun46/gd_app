import React, { Component } from 'react';
import { Platform, FlatList, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconION from "react-native-vector-icons/Ionicons";
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
import _ from 'lodash'
import IconANT from "react-native-vector-icons/AntDesign";


const dataImg = [
  {
    "imgrp": "https://www.iol.co.za/assets/images/general/no-image.png"
  }
]


class GtripList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      langPrint: ''
    }
  }

  componentDidMount() {
    /************************************ Get Language Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes
      })
    })

  }



  render() {
    /************************* Pass props data in htmlContent using(react-native-render-html) *************************/
    const htmlContent_en = this.props.data.description;
    const htmlContent_cn = this.props.data.description_cn;
    const { langPrint } = this.state;
    const title_en = this.props.data.title;
    const title_cn = this.props.data.title_cn;
    console.log(this.props.data, 'data g');



    /************************* IGNORED TAGS Api Data *************************/
    const tags = _.without(IGNORED_TAGS,
      'table', 'caption', 'col', 'colgroup', 'tbody', 'td', 'tfoot', 'th', 'thead', 'tr', 'iframe', 'pre',
    )
    /************************* Render Table *************************/
    const renderers = {
      table: (x, c) => <ScrollView horizontal={true}><View style={tableColumnStyle}>{c}</View></ScrollView>,
      col: (x, c) => <View style={tableColumnStyle}>{c}</View>,
      colgroup: (x, c) => <View style={tableRowStyle}>{c}</View>,
      tbody: (x, c) => <View style={tableColumnStyle}>{c}</View>,
      tfoot: (x, c) => <View style={tableRowStyle}>{c}</View>,
      th: (x, c) => <View style={thStyle}>{c}</View>,
      thead: (x, c) => <View style={tableRowStyle}>{c}</View>,
      caption: (x, c) => <View style={tableColumnStyle}>{c}</View>,
      tr: (x, c) => <View style={tableRowStyle}>{c}</View>,
      td: (x, c) => <View style={tdStyle}>{c}</View>,
      iframe: (x, c) => <WebView source={{ uri: video }}
        style={{ height: 200, width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'black' }}
      />,
      pre: (x, c) => <View style={imgStyle}>{c}</View>,
      // a: (x, c) => <TouchableOpacity onPress={(event, href) => { Linking.openURL(href)}}><Text>{c}</Text></TouchableOpacity>,
      // p: (x, c) => <View style={{paddingTop: 20}}>{c}</View>

    }
    /************************* Style Table *************************/
    const tableDefaultStyle = { flex: 1, justifyContent: 'flex-start', }
    const tableColumnStyle = { ...tableDefaultStyle, flexDirection: 'column', alignItems: 'stretch', width: 800, }
    const tableRowStyle = { ...tableDefaultStyle, flexDirection: 'row', alignItems: 'stretch', }
    const tdStyle = { ...tableDefaultStyle, padding: 2, backgroundColor: '#f6f8fa', borderWidth: .5, }
    const thStyle = { ...tdStyle, backgroundColor: '#fcf5f5', alignItems: 'center', }
    const imgStyle = {
      width: '100%',
      //    height: 1000
    }



    return (

      <View>
        <ScrollView>
          <View style={{ backgroundColor: '#fff' }}>
            <View>
              <Image
                source={{ uri: this.props.data.pic_url === "" ? dataImg[0].imgrp : this.props.data.pic_url }}
                style={styles.CountryInforImage}
              />
            </View>
            <View style={{ padding: 20 }} >
              <IconION name="ios-briefcase" size={35} style={styles.IconStyle} />
              <Text style={styles.TitleText}>{langPrint === 'zh' ? title_cn : title_en}</Text>
              {/* <Text style={styles.ContentText}>{this.props.data.description}</Text> */}
              {/* <HTML
                html={langPrint === 'zh' ? htmlContent_cn : htmlContent_en}
                ignoredStyles={['font-family', 'display',]}
                style={styles.ContentText}
              /> */}
            </View>
          </View>
          <View >
            <HTML
              alterChildren={(node) => {
                if (node.name === 'iframe') {
                  delete node.attribs.width;
                  delete node.attribs.height;
                }
                return node.children;
              }}
              html={langPrint === "zh" ? htmlContent_cn : htmlContent_en}
              imagesInitialDimensions={{ width: '100%' }}
              tagsStyles={{
                p: { lineHeight: 20, fontSize: 14, paddingBottom: 10 },
                h2: { fontSize: 14, lineHeight: 20, paddingBottom: 10 },
                h3: { fontSize: 14, lineHeight: 20, paddingBottom: 10 },
              }}
              ignoredStyles={[
                'font-family', 'display', 'width', 'padding', 'font-size',
                // 'border-collapse','border-spacing','font-weigh','color','border','margin-top','margin-bottom','margin','page-break-after',
                // 'font-variant-numeric','font-variant-east-asian','list-style', 'height','table-layout','direction','unicode-bidi',
                // 'position','-webkit-font-smoothing','overflow','padding-bottom','line-height','cursor','text-decoration-line',
                // 'white-space','box-sizing','overflow-wrap','font-weight',
              ]}
              ignoredTags={tags}
              //ignoredTags={[...IGNORED_TAGS,'iframe',]}
              renderers={renderers}
              onLinkPress={(event, href) => { Linking.openURL(href) }}
              //staticContentMaxWidth={Dimensions.get('window').width}s
              //imagesMaxWidth={Dimensions.get('window').width}
              //debug={true}
              //containerStyle={{padding: 10}}
              classesStyles={{
                _3dgx: { fontSize: 14, paddingLeft: 5, paddingRight: 5 },
                _4lmk: { paddingLeft: 5, paddingRight: 5 },
                _50a1: { marginLeft: 0, marginRight: 0, marginTop: 30, marginBottom: 10, paddingLeft: 5, paddingRight: 5 },
              }}


            />

          </View>
        </ScrollView>
      </View>
    );
  }
}



export default GtripList;

const styles = StyleSheet.create({
  CountryInforImage: { width: "100%", resizeMode: 'cover', height: 180, },
  IconStyle: {
    textAlign: 'center',
    ...Platform.select({
      ios: { color: 'black' },
      android: { color: 'black' }
    })
  },
  TitleText: {
    textAlign: 'center', fontSize: 18, marginTop: 5, fontWeight: 'bold',
    ...Platform.select({
      ios: { color: 'black' },
      android: { color: 'black' }
    })
  },
  ContentText: { textAlign: 'center', marginTop: 20, paddingLeft: 10, paddingRight: 10, color: '#606c77' },
  FlatContainer: { flex: 1, marginVertical: 10, },
  ImageWarpperContainer: { flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' },
  ImageWarpper: { width: '50%', paddingLeft: 10, paddingRight: 10, marginBottom: 20, },
  ImageInner: { backgroundColor: 'white', borderRadius: 10, },
  ImageInnerWarp: { borderTopRightRadius: 5, borderTopLeftRadius: 5, overflow: 'hidden', },
  ImageStyle: { height: 150, width: null, flex: 1, },
  TextImage: {
    textAlign: 'center', padding: 10, fontWeight: 'bold',
    ...Platform.select({
      ios: { color: 'black' },
      android: { color: 'black' }
    })
  },
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 100, },

})
