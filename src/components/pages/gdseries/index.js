import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Image, ScrollView, TouchableOpacity, ActivityIndicator, Modal, } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { DestinationApi } from '../../../../PostApi';

class GdSeries extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {
        countries: [],
        type_id: 0,
      },
      imgType1: [],
      imgType2: [],
      imgType4: [],
      isSelect: false
    }
  }

  componentDidMount() {
    /************************* Fetch Data Type Image *************************/
    DestinationApi('1').then((fetchData) => { this.setState({ imgType1: fetchData[0] }) })
    DestinationApi('2').then((fetchData) => { this.setState({ imgType2: fetchData[0] }) })
    DestinationApi('4').then((fetchData) => { this.setState({ imgType4: fetchData[0] }) })
  }


  /************************* Fetch Data in Api and Pass in onPress Button *************************/
  _onPressButton(type_name) {
    this.setState({
      isSelect: true
    })
    DestinationApi(type_name).then((fetchData) => {
      console.log(fetchData, '----------GdSeries');
      AsyncStorage.setItem('SAVE_TYPE', type_name);
      fetchData.forEach(function (element) {
        element['type_id'] = type_name;
      });

      this.setState({
        data: Actions.gdPremium({ data: fetchData }),
        isSelect: false
      })
      //console.log(this.state);
    })
  }


  render() {

    const { imgType1, imgType2, imgType4, gtripData, isSelect } = this.state;


    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        {
          isSelect ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSelect}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator size="large" animating={isSelect} />
                </View>
              </View>
            </Modal>
            : null
        }
        <ScrollView>
          <View style={{ padding: 30, }}>
            <View style={styles.ImageContainer} >
              <TouchableOpacity onPress={() => this._onPressButton('1')}>
                {/* <TouchableOpacity onPress={() => Actions.gdPremium()}> */}
                <Image source={{ uri: imgType1.type_pic }} style={styles.ImageStyle} />
              </TouchableOpacity>
            </View>
            <View style={styles.ImageContainer}>
              <TouchableOpacity onPress={() => this._onPressButton('2')}>
                <Image source={{ uri: imgType2.type_pic }} style={styles.ImageStyle}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.ImageContainer}>
              <TouchableOpacity onPress={() => this._onPressButton('4')}>
                <Image source={{ uri: imgType4.type_pic }} style={styles.ImageStyle}
                />
              </TouchableOpacity>
            </View>

            {/* 
            <View style={styles.ImageContainer}>
              <Image source={require('../../../assets/images/img/gd04.png')} style={styles.ImageStyle} />
            </View>
            */}

          </View>
        </ScrollView>
      </View>

    );
  }
}



export default GdSeries;

const styles = StyleSheet.create({
  ImageContainer: { marginBottom: 20, height: 150 },
  ImageStyle: { width: "100%", resizeMode: 'cover', height: 150, borderRadius: 3, },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

})