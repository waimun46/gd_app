import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, ImageBackground, Platform, Image, TouchableOpacity, Linking, Button, Modal, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import Carousel from 'react-native-looped-carousel';
import { ImageWelcome } from '../../../../PostApi';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
import _ from 'lodash';

const { width, height } = Dimensions.get('window');

const datalocal = [
   {
      id: 1,
      image: 'https://images.pexels.com/photos/1591447/pexels-photo-1591447.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      link: 'Newspage'
   },
   {
      id: 2,
      image: 'https://i.pinimg.com/originals/7e/93/4f/7e934f480dd2081df255df662f84e8cb.jpg',
      link: 'Homepage'
   }
]



class WelcomePage extends Component {
   constructor(props) {
      super(props);
      this.state = {
         size: { width, height },
         dataWelcome: [],
         modalVisble: false,
         madalData: { pic_url: '', title: '', title_cn: '', description_en: '', description_cn: '', video: '', },
         langPrint: '',

      };
   }


   componentDidMount() {
      /************************************ Get Language Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem("language").then(langStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Note')
         this.setState({
            langPrint: langStorageRes
         })
      })

      ImageWelcome().then((fetchData) => {
         console.log(fetchData, '---fetchData')
         this.setState({
            dataWelcome: fetchData
         })
      })
   }



   /************************* Set page Carousel event width and height *************************/
   _onLayoutDidChange = (e) => {
      const layout = e.nativeEvent.layout;
      this.setState({ size: { width: layout.width, height: layout.height } });
   }

   /****************************************** closeModal ********************************************/
   closeModal = () => {
      console.log('closeModal')
      this.setState({
         modalVisble: false
      })
   }

   /****************************************** closeModal ********************************************/
   homeActions = () => {
      this.setState({
         modalVisble: false
      })
      Actions.Homepage()
   }

   /****************************************** madalHandle ********************************************/
   modalHandle(id) {
      const madalData = this.state.dataWelcome.find(ele => {
         return ele.id === id
      })
      this.setState({
         modalVisble: true,
         madalData: {
            pic_url: madalData.pic_url, title: madalData.title, title_cn: madalData.title_cn, description_en: madalData.description_en,
            description_cn: madalData.description_cn, video: madalData.video,
         }
      })
   }




   render() {
      const { dataWelcome, size, modalVisble, madalData, langPrint } = this.state;
      const images = dataWelcome;
      if (!images || images.length === 0) {
         return null;
      }

      console.log('images', images)
      console.log(madalData, 'madalData')

      const htmlContent_en = madalData.description_en;
      const htmlContent_cn = madalData.description_cn;
      const video = madalData.video;
      console.log(video, 'video');


      /************************* IGNORED TAGS Api Data *************************/
      const tags = _.without(IGNORED_TAGS,
         'table', 'caption', 'col', 'colgroup', 'tbody', 'td', 'tfoot', 'th', 'thead', 'tr', 'iframe', 'pre',
      )
      /************************* Render Table *************************/
      const renderers = {
         table: (x, c) => <ScrollView horizontal={true}><View style={tableColumnStyle}>{c}</View></ScrollView>,
         col: (x, c) => <View style={tableColumnStyle}>{c}</View>,
         colgroup: (x, c) => <View style={tableRowStyle}>{c}</View>,
         tbody: (x, c) => <View style={tableColumnStyle}>{c}</View>,
         tfoot: (x, c) => <View style={tableRowStyle}>{c}</View>,
         th: (x, c) => <View style={thStyle}>{c}</View>,
         thead: (x, c) => <View style={tableRowStyle}>{c}</View>,
         caption: (x, c) => <View style={tableColumnStyle}>{c}</View>,
         tr: (x, c) => <View style={tableRowStyle}>{c}</View>,
         td: (x, c) => <View style={tdStyle}>{c}</View>,
         iframe: (x, c) => <WebView source={{ uri: video }}
            style={{ height: 200, width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'black' }}
         />,
         pre: (x, c) => <View style={imgStyle}>{c}</View>,
         // a: (x, c) => <TouchableOpacity onPress={(event, href) => { Linking.openURL(href)}}><Text>{c}</Text></TouchableOpacity>,
         // p: (x, c) => <View style={{paddingTop: 20}}>{c}</View>

      }
      /************************* Style Table *************************/
      const tableDefaultStyle = { flex: 1, justifyContent: 'flex-start', }
      const tableColumnStyle = { ...tableDefaultStyle, flexDirection: 'column', alignItems: 'stretch', width: 800, }
      const tableRowStyle = { ...tableDefaultStyle, flexDirection: 'row', alignItems: 'stretch', }
      const tdStyle = { ...tableDefaultStyle, padding: 2, backgroundColor: '#f6f8fa', borderWidth: .5, }
      const thStyle = { ...tdStyle, backgroundColor: '#fcf5f5', alignItems: 'center', }
      const imgStyle = {
         width: '100%',
         //    height: 1000
      }


      return (
         <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
            {/******************************** Modal  ********************************/}
            <Carousel
               delay={4000}
               style={size}
               onAnimateNextPage={(p) => console.log(p)}
               pageInfo
               bullets
               autoplay
               bulletsContainerStyle={{ bottom: 100 }}
            >
               {
                  images.map((item, i) => {
                     return (

                        <View style={size} key={i}>
                           <TouchableOpacity onPress={() => this.modalHandle(item.id)}>
                              <Image
                                 source={{ uri: item.image }}
                                 style={{ width: "100%", height: '100%', resizeMode: 'cover', }}
                              />
                           </TouchableOpacity>
                        </View>

                        // <View style={size} key={i}>
                        //    {/* <TouchableOpacity onPress={() => Actions[item.link].call()}> */}
                        //    <TouchableOpacity onPress={() => this.modalHandle(item.id)}>
                        //       <Image
                        //          source={{ uri: item.image }}
                        //          style={{ width: "100%", height: '100%', resizeMode: 'cover', }}
                        //       />
                        //    </TouchableOpacity>
                        // </View>

                     )
                  })
               }

            </Carousel>
            {/* <View style={[this.state.size]}>
                        <Image
                            source={{ uri: this.state.imgData01.image }}
                            style={{ width: "100%", height: '100%', resizeMode: 'cover', }}
                        />
                        {/* <View style={{ position: 'absolute', bottom: '48%', left: 30 }}>
                            <Text style={styles.texttitle}>{global.t('Taiwan')}</Text>
                        </View>
                    </View>
                    <View style={[this.state.size]}>
                        <Image
                            source={{ uri: this.state.imgData02.image }}
                            style={{ width: "100%", height: '100%', resizeMode: 'cover', }}
                        />
                        {/* <View style={{ position: 'absolute', bottom: '48%', left: 30 }}>
                            <Text style={[styles.texttitle, { fontFamily: Fonts.Helvetica }]}>{global.t('Dubai')}</Text>
                        </View> 
                    </View> */}

            {/* <View style={styles.logowarp}>
                    <Image
                        source={require('../../../assets/images/logotext.png')}
                        style={{ width: 200, height: 100, resizeMode: 'contain', }}
                    />
                </View> */}
            <View style={styles.textenterwarp} >
               <View style={styles.textenterwarpinner}>
                  <TouchableOpacity onPress={() => Actions.Homepage()} style={{}}>
                     <Text style={styles.textenter} >{global.t('Enter')}</Text>
                  </TouchableOpacity>
               </View>
            </View>

            {/******************************** Modal  ********************************/}
            <Modal
               animationType="fade"
               //transparent={true}
               visible={modalVisble}
               onRequestClose={() => {
                  Alert.alert('Modal has been closed.');
               }}>
               <ScrollView>
                  {/******************************** IMAGE  ********************************/}
                  <Image source={{ uri: madalData.pic_url }} style={styles.modalImage} />

                  {/******************************** OVERLAY  ********************************/}
                  <View style={styles.ovalayWarp}>
                     <View style={styles.iconModalWarp}>
                        <View>
                           <IconANT name='home' style={styles.iconModal} onPress={this.homeActions} />
                        </View>
                        <View>
                           <IconANT name='close' style={styles.iconModal} onPress={this.closeModal} />
                        </View>
                     </View>
                     <View style={styles.modalTitleWarp}>
                        <Text style={styles.modalText}>{madalData.title}</Text>
                     </View>
                  </View>

                  {/******************************** HTML RENDER  ********************************/}
                  <View style={{ marginTop: 30, marginBottom: 50 }}>
                     <HTML
                        alterChildren={(node) => {
                           if (node.name === 'iframe') {
                              delete node.attribs.width;
                              delete node.attribs.height;
                           }
                           return node.children;
                        }}
                        html={langPrint === "zh" ? htmlContent_cn : htmlContent_en}
                        imagesInitialDimensions={{ width: '100%' }}
                        tagsStyles={{
                           p: { lineHeight: 20, fontSize: 14, paddingBottom: 10 },
                           h2: { fontSize: 14, lineHeight: 20, paddingBottom: 10 },
                           h3: { fontSize: 14, lineHeight: 20, paddingBottom: 10 }
                        }}
                        ignoredStyles={[
                           'font-family', 'display', 'width', 'padding', 'font-size',
                           // 'border-collapse','border-spacing','font-weigh','color','border','margin-top','margin-bottom','margin','page-break-after',
                           // 'font-variant-numeric','font-variant-east-asian','list-style', 'height','table-layout','direction','unicode-bidi',
                           // 'position','-webkit-font-smoothing','overflow','padding-bottom','line-height','cursor','text-decoration-line',
                           // 'white-space','box-sizing','overflow-wrap','font-weight',
                        ]}
                        ignoredTags={tags}
                        //ignoredTags={[...IGNORED_TAGS,'iframe',]}
                        renderers={renderers}
                        onLinkPress={(event, href) => { Linking.openURL(href) }}
                        //staticContentMaxWidth={Dimensions.get('window').width}s
                        //imagesMaxWidth={Dimensions.get('window').width}
                        //debug={true}
                        //containerStyle={{padding: 10}}
                        classesStyles={{
                           _3dgx: { fontSize: 14, paddingLeft: 5, paddingRight: 5 },
                           _4lmk: { paddingLeft: 5, paddingRight: 5 },
                           _50a1: { marginLeft: 0, marginRight: 0, marginTop: 30, marginBottom: 10, paddingLeft: 5, paddingRight: 5 },
                        }}
                     />
                  </View>
               </ScrollView>
            </Modal>



         </View>
      );
   }

}


export default WelcomePage;

const styles = StyleSheet.create({
   logowarp: { position: 'absolute', top: 80, left: 20, width: '100%', },
   texttitle: {
      color: '#fff', fontSize: 50, fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
      textShadowRadius: 2, textShadowColor: '#000',
   },
   textenterwarp: { position: 'absolute', bottom: 30, width: '100%', },
   textenterwarpinner: { width: '90%', marginLeft: 'auto', marginRight: 'auto', },
   textenter: { color: '#fff', fontSize: 20, textAlign: 'center', padding: 10, paddingLeft: 10 },
   modalImage: { width: '100%', height: 250, resizeMode: 'cover', position: 'relative', },
   ovalayWarp: { position: 'absolute', height: 250, width: '100%', backgroundColor: '#000000b5' },
   iconModalWarp: {
      marginTop: 30, marginBottom: 55, paddingLeft: 20, paddingRight: 20, width: '100%', flexDirection: 'row',
      justifyContent: 'space-between'
   },
   iconModal: { fontSize: 22, color: 'white' },
   modalTitleWarp: { paddingLeft: 20, paddingRight: 20, alignItems: 'center', },
   modalText: { color: '#fff', fontWeight: 'bold', fontSize: 25, textAlign: 'center' },

})
