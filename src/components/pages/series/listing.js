import React, { Component } from 'react';
import { StyleSheet, View, ImageBackground, ScrollView, Dimensions, Image, FlatList, Modal, TouchableOpacity, ActivityIndicator, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, Card, CardItem, Button, ListItem, Left, Body, Right, CheckBox } from 'native-base';
import bgimg from '../../../assets/images/background/wc02.png';
import Carousel from 'react-native-snap-carousel';
import IconENT from "react-native-vector-icons/Entypo";
import IconANT from "react-native-vector-icons/AntDesign";
import { Actions } from 'react-native-router-flux';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { Select, Option } from "react-native-chooser";
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';

const sliderWidth = Dimensions.get('window').width;
const slideWidthReviews = 300;
const itemWidthReviews = slideWidthReviews;
const dwnLoadingHeight = Dimensions.get('window').height;

const typeGD = [
  { type: 'All' }, { type: 'GD Standard' }, { type: 'GD Premium' },
]

const tempCheckValues = [];

class SeriesListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      langPrint: '',
      modalVisible: false,
      modalData: { description: '' },
      isLoading: true,
      dwnLoading: false,
      dwnDisabled: false,
      switcbtn: false,
      // dataPremium: [],
      // dataStandard: [],
      checkBoxChecked: [],
      gdType: global.t('All'),
      typeObj: [],
    };

  }

  componentDidMount() {
    /********************************************* Get Language Store Data From AsyncStorage  ********************************************/
    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes
      })
    })
    this.fetchSeriesListing();
  }

  /*************************************** fetchSeriesListing ***************************************/
  fetchSeriesListing(keyword = this.props.keyword, year = this.props.year, month = this.props.month, line = this.props.year === "" ? "" : "/") {
    let url = `https://iceb2b.my/api/series?key=GDApiYEK&keyword=${keyword}&month=${year}${line}${month}`;
    console.log('url-listing', url);
    console.log('year-----', year)

    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        // let filterPremium = fetchData.itineraries.filter(ele => { return ele.category === 'GD Premium' })
        // let filterStandard = fetchData.itineraries.filter(ele => { return ele.category === 'GD Standard' })
        this.setState({
          data: fetchData.itineraries,
          isLoading: false,
          // dataPremium: filterPremium,
          // dataStandard: filterStandard
        })
      })
  }

  /*************************************** Download PDF File ***************************************/
  downloadFile(item) {
    function getLocalPath(url) {
      const filename = url.split('/').pop();
      return `${RNFS.DocumentDirectoryPath}/${filename}`;
    }

    this.setState({
      dwnLoading: true,
      dwnDisabled: true
    })

    const url = item.file_url;
    const localFile = getLocalPath(url);

    const options = {
      fromUrl: url,
      toFile: localFile
    };
    RNFS.downloadFile(options).promise
      .then(() => FileViewer.open(localFile))
      .then(() => {
        this.setState({
          dwnLoading: false,
          dwnDisabled: false
        })
      })
      .catch(error => {
        // error
      });
  }

  /************************************************************ openModalProduct ***********************************************************/
  openModal(id) {
    const modalData = this.state.data.find(element => {
      return element.id === id
    })
    this.setState({
      modalVisible: true,
      modalData: { description: modalData.description, }
    });
  };

  /************************************************************ closeImage ***********************************************************/
  closeModal = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false
    })
  }


  /**************************************************** _renderItemSlider ***************************************************/
  renderItemSlider({ item, index }) {
    return (
      <ImageBackground source={{ uri: item.img }} style={[styles.imgBg, { justifyContent: 'flex-end' }]}>
        <View style={styles.overlay} />
        <View style={styles.textWarp}>
          <Text style={styles.titleHeader}>{item.title}</Text>
          <View style={styles.lineHeader} />
        </View>
      </ImageBackground>
    );
  }

  /**************************************************** renderItemListingDate ***************************************************/
  renderItemListingDate({ item, index }) {
    return (
      <View style={styles.dateWarp}>
        <Text style={styles.dateSty}>{item}</Text>
      </View>
    );
  }

  /**************************************************** renderItemListing ***************************************************/
  renderItemListing = ({ item, index }) => {
    const { langPrint, dwnDisabled } = this.state;
    // console.log('item-----', item.images[0])
    return (
      <Card style={{ marginBottom: 20 }}>
        <CardItem cardBody>
          <Image source={{ uri: item.images[0] }} style={styles.listImg} />
        </CardItem>
        <CardItem style={{ paddingLeft: 5, paddingRight: 5 }}>
          <View style={{ flexDirection: 'row', }}>
            <Text style={{ width: '92%' }}>{langPrint === 'zh' ? item.other_caption : item.caption}</Text>
            <View style={{ width: '8%' }}>
              <TouchableOpacity onPress={() => this.openModal(item.id)}>
                <IconANT name="questioncircleo" size={18} style={{ color: '#fa8c16', textAlign: 'right' }} />
              </TouchableOpacity>
            </View>
          </View>
        </CardItem>
        <CardItem style={{ paddingTop: 0, paddingLeft: 5, paddingRight: 5 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flexDirection: 'row', width: '90%' }}>
              <View style={styles.iconWarp}>
                <IconENT size={15} name="location-pin" style={{ marginRight: 3, color: 'red' }} />
                <Text note>{item.country}</Text>
              </View>
              <View style={styles.iconWarp}>
                <IconENT size={15} name="clipboard" style={{ marginRight: 3, color: '#40a9ff' }} />
                <Text note>{item.code}</Text>
              </View>
              <View style={styles.iconWarp}>
                {
                  item.category === 'GD Standard' ?
                    <Image source={require('../../../assets/images/logo.png')} style={{ height: 20, width: 20, marginRight: 3 }} /> :
                    item.category === 'GD Premium' ?
                      <Image source={require('../../../assets/images/premium_logo.png')} style={{ height: 20, width: 20, marginRight: 3 }} /> :
                      item.category === 'Four Season' ?
                        <Image source={require('../../../assets/images/fs_logo.png')} style={{ height: 20, width: 20, marginRight: 3 }} /> :
                        null
                }
                <Text note>{item.category}</Text>
              </View>
            </View>
            <View style={{ width: '10%' }}>
              <TouchableOpacity onPress={this.downloadFile.bind(this, item)} disabled={dwnDisabled} style={{ alignItems: 'flex-end' }}>
                <View style={[styles.iconWarp,]}>
                  <IconENT size={20} name="download" style={{ color: dwnDisabled === true ? '#eff0f1' : 'red', textAlign: 'right' }} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </CardItem>
        <CardItem style={{ paddingTop: 0, paddingLeft: 5, paddingRight: 5 }}>
          <Text note numberOfLines={1} style={{ fontWeight: 'bold' }}>{global.t('Departure')}</Text>
        </CardItem>
        <CardItem style={{ paddingTop: 0, marginBottom: 5 }}>
          <FlatList
            data={item.departure_date}
            renderItem={this.renderItemListingDate}
            keyExtractor={(item, index) => index.toString()}
            horizontal={true}
          // contentContainerStyle={{ marginTop: 5 }}
          />
        </CardItem>
        <CardItem style={styles.priceList}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text note>{global.t('from')}</Text>
            <Text style={styles.priceSty}>RM {item.price}</Text>
            <Text note>{global.t('All_In')}</Text>
          </View>
          <View>
            <Button danger style={{ height: 30, }} onPress={() => Actions.seriesShowpage({
              id: item.id, month: this.props.month, year: this.props.year
            })}>
              <Text>{global.t('Select')}</Text>
            </Button>
          </View>
        </CardItem>
      </Card>

    )
  }


  /**************************************************** checkBoxChanged ***************************************************/
  checkBoxChanged(type, value) {
    this.setState({
      checkBoxChecked: tempCheckValues
    })

    let tempCheckBoxChecked = this.state.checkBoxChecked;
    tempCheckBoxChecked[type] = !value;

    this.setState({
      checkBoxChecked: tempCheckBoxChecked
    })

  }

  /****************************************** Select Option State ******************************************/
  onSelect(value, label) {
    console.log(value, 'value-----------')
    typeObj = this.state.data.filter(element => {
      return element.category === value
    })
    this.setState({
      gdType: value,
      typeObj: typeObj
    });
  }

  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('no_tour_packages')}</Text>
      </View>
    );
  };



  render() {
    const { data, modalVisible, modalData, isLoading, dwnLoading, switcbtn, dataPremium, checkBoxChecked, dataStandard, langPrint,
      gdType, typeObj } = this.state;
    const htmlContent = modalData.description;
    //console.log('propspass', this.props.keyword, this.props.month, this.props.year);
    // const uniKeys = [...(new Set(data.map(({ category }) => category)))];
    console.log('data--------', data)
    console.log('data--------length', data.length)
    // console.log('dataPremium-----', dataPremium)
    // console.log('dataStandard------', dataStandard)
    // console.log('checkBoxChecked', checkBoxChecked)
    console.log('typeObj----', typeObj)


    return (
      <View style={[styles.container, { position: 'relative' }]}>
        {
          isLoading === true ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
              <ActivityIndicator size="large" color="#de2d30" />
            </View>
            :
            <ScrollView>
              <ImageBackground source={bgimg} style={styles.imgBg}>
                <View style={styles.overlay} />
                {
                  data.length === 0 ? null :
                    <View style={styles.textWarp}>
                      <Text style={styles.titleHeader}>{data[0].country}</Text>
                      <View style={styles.lineHeader} />
                    </View>
                }
              </ImageBackground>

              {
                data.length === 0 ? null :
                  <View style={{ borderBottomWidth: 0.5, borderBottomColor: "#fff", width: '99%' }}>
                    <Select
                      onSelect={this.onSelect.bind(this)}
                      defaultText={(<Text>
                        <Text style={{ color: '#de2d30', }}>{gdType}   </Text>
                        <IconANT name='filter' style={{ fontSize: 15, color: '#de2d30', }} />
                      </Text>)}
                      style={styles.selectSty}
                      textStyle={styles.selectText}
                      backdropStyle={{ backgroundColor: "#00000078" }}
                      optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                      animationType='none'
                      transparent={true}
                      indicator='none'
                    // indicatorColor='#de2d30'
                    // indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                    // indicatorStyle={{ marginTop: Platform.OS === 'ios' ? -15 : -15, marginLeft: -10 }}
                    >
                      {
                        [global.t('All'), 'GD Standard', 'GD Premium', 'Four Season'].map((item, i) => {
                          return (
                            <Option value={item} key={i}>{item}</Option>
                          )
                        })

                      }
                    </Select>
                  </View>
              }

              {/* <ListItem style={{ backgroundColor: '#fff', marginLeft: 0, paddingLeft: 0 }}>
                <Body >
                  <View style={{ flexDirection: 'row' }}>
                    {
                      typeGD.map((item) => {
                        { tempCheckValues[item.type] = false }
                        return (
                          <View key={item.type} style={{ flexDirection: 'row' }}>
                            <CheckBox
                              checked={checkBoxChecked[item.type]}
                              onPress={() => this.checkBoxChanged(item.type, checkBoxChecked[item.type])}
                              style={{width: 18, height: 18}}
                            />
                            <Text style={{paddingLeft:5, fontSize: 14}}>{item.type}</Text>
                          </View>
                        )
                      })
                    }
                  </View>
                </Body>
              </ListItem> */}


              {/* <View style={styles.aboutWarp}>
                <Text style={styles.textSub}>About our Packages</Text>
                <View style={[styles.lineHeader, { backgroundColor: '#eee', width: 60 }]} />
                <Text style={styles.subText}>China Primum Trips</Text>
                <Text note style={{ textAlign: 'center' }}>
                  China tourism has developed rapidly for the last few decades, since the the open policy had been implemented in 1978.
                  The glorious ancient architectures, the splendid landscapes, the hospitable Chinese people, all of these make the tourism
                  industry prosper in the past few decades. The emergence of a newly rich middle class and an easing of restrictions on movement
                  by the Chinese authorities are both fueling this travel boom. China has become one of the world’s most- watched and hottest
                  outbound tourist markets. There are 1349 international travel agencies existing in China, and 248 of them are in Beijing,
                  Shanghai, Tianjin and Chongqing-the four municipalities. The domestic and international transportation also has great
                  improvement that it is easier for tourists to travel to China.
                </Text>
              </View> */}

              {/* <View style={{ marginTop: 30 }}>
                <Text style={styles.packageTitle}>Package Tour</Text>
                <Carousel
                  data={localData}
                  renderItem={this.renderItemSlider}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidthReviews}
                  // inactiveSlideScale={0.99}
                  inactiveSlideOpacity={1}
                  enableMomentum={false}
                  activeSlideAlignment={'center'}
                  activeAnimationType={'decay'}
                  // activeAnimationOptions={{
                  //   friction: 4,
                  //   tension: 40
                  // }}
                  autoplay={true}
                  autoplayDelay={4000}
                  autoplayInterval={4000}
                  loop={true}
                />
              </View> */}

              <View style={{ marginTop: 10, paddingLeft: 5, paddingRight: 5, }}>
                <FlatList
                  data={gdType === 'All' ? data : typeObj}
                  extraData={this.state}
                  renderItem={this.renderItemListing}
                  keyExtractor={(item, index) => index.toString()}
                  contentContainerStyle={{ paddingBottom: 50 }}
                  ListEmptyComponent={this.ListEmpty}
                />
              </View>
            </ScrollView>
        }


        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <IconANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>
            <Text style={{ fontWeight: 'bold', color: '#fff', marginBottom: 20, fontSize: 18 }}>{global.t('Description')}</Text>
            <View style={{ height: 400, backgroundColor: '#fff', width: '90%', borderRadius: 10 }}>
              <ScrollView style={{height: 400}}>
                <View style={{ padding: 20 ,  paddingBottom: 30}}>
                  <HTML
                    html={modalData.description}
                    tagsStyles={{
                      img: { width: '100%', resizeMode: 'contain' },
                      // p: { padding: 10, }
                    }}
                  />
                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>

        {
          dwnLoading === true ?
            <View style={{
              justifyContent: 'center', alignItems: 'center', backgroundColor: '#eff0ee94',
              position: 'absolute', width: '100%', height: dwnLoadingHeight
            }}>
              <ActivityIndicator size="large" color="#de2d30" />
            </View>
            : null
        }


      </View>
    );
  }
}

export default SeriesListing;

const styles = StyleSheet.create({
  container: { flex: 1, },
  imgBg: { width: '100%', height: 150, resizeMode: 'cover', justifyContent: 'center' },
  overlay: { backgroundColor: '#00000059', height: 150, position: 'relative' },
  textWarp: { position: 'absolute', alignItems: 'center', width: '100%', padding: 10, },
  titleHeader: { color: '#fff', fontWeight: 'bold', fontSize: 25, marginBottom: 5, },
  lineHeader: { width: 100, height: 2, backgroundColor: '#fff', marginBottom: 10 },
  aboutWarp: { backgroundColor: '#fff', padding: 20, alignItems: 'center' },
  textSub: { textAlign: 'center', color: '#000', fontWeight: 'bold', fontSize: 14, marginBottom: 10 },
  subText: { textAlign: 'center', color: '#de2d30', fontWeight: 'bold', fontSize: 18, marginBottom: 10 },
  packageTitle: { textAlign: 'center', marginBottom: 15, fontWeight: 'bold', fontSize: 18 },
  dateWarp: {
    backgroundColor: '#fff7e6', borderWidth: 1, borderColor: '#ffd591', paddingLeft: 5, paddingRight: 5,
    marginRight: 5, justifyContent: 'center',
  },
  dateSty: { color: '#fa8c16', textAlign: 'center', marginRight: 0, fontSize: 14 },
  listImg: { height: 180, width: '100%', flex: 1, resizeMode: 'cover' },
  iconWarp: { flexDirection: 'row', alignItems: 'center', marginRight: 5, },
  priceList: { flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#f1f1f1' },
  priceSty: { paddingLeft: 10, paddingRight: 10, fontWeight: 'bold' },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  imgModal: { width: '100%', height: 400, resizeMode: 'contain', marginTop: 20 },
  selectSty: { borderWidth: 0, width: '100%', backgroundColor: '#fff', },
  selectText: { color: '#de2d30', textAlign: 'center', width: '100%', fontWeight: 'bold', },
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },


})


