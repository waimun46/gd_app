import React, { Component } from 'react';
import {
  StyleSheet, View, ScrollView, ImageBackground, FlatList, TouchableOpacity, Image, Modal, Platform, ActivityIndicator,
  Linking, Share, Dimensions
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Tab, Tabs, ScrollableTab, Text, Accordion, Card, CardItem, Body, Button, Badge } from 'native-base';
import { ScrollableTabView, DefaultTabBar, ScrollableTabBar, } from '@valdio/react-native-scrollable-tabview'
import bgimg from '../../../assets/images/background/wc02.png';
import IconANT from "react-native-vector-icons/AntDesign";
import IconMTI from "react-native-vector-icons/MaterialIcons";
import IconETY from "react-native-vector-icons/Entypo";
import IconFAS from "react-native-vector-icons/FontAwesome";
import IconMIC from "react-native-vector-icons/MaterialCommunityIcons";
import logo from '../../../assets/images/logo.png';
import { Select, Option } from "react-native-chooser";
import empty from '../../../assets/images/img/empty.png';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { StateApi } from '../../../../PostApi';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';

const dwnLoadingHeight = Dimensions.get('window').height;


/************************* If Api Data Empty Show Local data *************************/
const localdata = [
  { "airlinelocal": "To Be Confirmed", "statuslocal_en": "Selling Fast", "statuslocal_cn": "热销中" }
]


class SeriesShowPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      agentVisble: false,
      data: [],
      langPrint: '',
      imageCover: [],
      tourData: [],
      isLoading: true,
      stateData: [],
      stateValue: global.t('State'),
      cityValue: global.t('City'),
      stateObj: { f_name: 'State' },
      cityData: [],
      isLoadingAgent: true,
      dwnLoading: false,
      dwnDisabled: false
    };
  }

  componentDidMount() {
    /********************************************* Get Language Store Data From AsyncStorage  ********************************************/
    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes
      })
    })
    this.fetchShowpage();
    this.fetchCity();

    StateApi().then((fetchData) => {
      //console.log(fetchData, '-------state')
      this.setState({
        stateData: fetchData
      })
    })

  }

  /************************************************************ fetchShowpage ***********************************************************/
  fetchShowpage(idKey = this.props.id, year = this.props.year, month = this.props.month, line = this.props.year === "" ? "" : "/") {
    let url = `https://iceb2b.my/api/series/${idKey}?key=GDApiYEK&month=${year}${line}${month}`;
    // let url = `https://iceb2b.my/api/series/1119?key=GDApiYEK&month=2020/4`;
    console.log('fetchShowpage-----', url);

    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          data: fetchData,
          imageCover: fetchData.images[0],
          tourData: fetchData.tours,
          isLoading: false
        })
      })
  }

  /************************************************************ fetchShowpage ***********************************************************/
  fetchCity(state = this.state.stateValue) {
    let url = `https://www.goldendestinations.com/api/new/city.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&state=${state}`;
    console.log('fetchCity-----', url);

    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          cityData: fetchData,
        })
      })
  }

  /************************************************************ fetchAgent ***********************************************************/
  fetchAgent(state = this.state.stateValue, city = this.state.cityValue) {
    let url = `https://www.goldendestinations.com/api/new/agent_list.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&state=${state}&city=${city}`;
    console.log('fetchAgent-----', url);

    this.setState({
      agentVisble: true
    })

    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          agentData: fetchData,
          isLoadingAgent: false,
          stateValue: global.t('State'),
          cityValue: global.t('City'),
        })
      })
  }

  /************************************************************ openModal ***********************************************************/
  openModal() {
    this.setState({
      modalVisible: true,
    });
  };

  /************************************************************ closeModal ***********************************************************/
  closeModal() {
    this.setState({
      modalVisible: false,
      agentVisble: false
    })
  }

  /****************************************** Select Option State ******************************************/
  onSelect(value, label) {
    //console.log(value, 'value-----------')
    // stateObj = this.state.stateData.find(element => {
    //   return element.f_id === value
    // })
    this.setState({
      stateValue: value,
      // stateObj: stateObj
    }, this.fetchCity);
  }

  /****************************************** Select Option City ******************************************/
  onSelectCity(value, label) {
    this.setState({
      cityValue: value
    });
  }

  // /****************************************** findAgent ******************************************/
  // findAgent() {
  //   this.setState({
  //     agentVisble: true
  //   })
  // }

  /*************************************** Download PDF File ***************************************/
  downloadFile(data) {
    // console.log('downloadFile-----------')
    function getLocalPath(url) {
      const filename = url.split('/').pop();
      return `${RNFS.DocumentDirectoryPath}/${filename}`;
    }

    this.setState({
      dwnLoading: true,
      dwnDisabled: true
    })

    const url = data.file_url;
    const localFile = getLocalPath(url);
    // console.log('downloadFile-----------url', url)

    const options = {
      fromUrl: url,
      toFile: localFile
    };
    RNFS.downloadFile(options).promise
      .then(() => FileViewer.open(localFile))
      .then(() => {
        this.setState({
          dwnLoading: false,
          dwnDisabled: false
        })
      })
      .catch(error => {
        // error
      });
  }

  /**************************************************** renderItemListingDate ***************************************************/
  // renderItemListingDate({ item, index }) {
  //   return (
  //     <View style={styles.dateWarp}>
  //       <Text style={styles.dateSty}>{item.category} : {item.title}</Text>
  //     </View>
  //   );
  // }


  /**************************************************** callPhone ***************************************************/
  callPhone(item) {
    // let phoneNumber = '';
    // if (Platform.OS === 'android') {
    //     phoneNumber = `tel:${this.props.data.tel}`;
    // }
    // else {
    //     phoneNumber = `telprompt:${this.props.data.tel}`;
    // }
    // Linking.openURL(phoneNumber);
    let phoneNumber = item.mobile;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phoneNumber}`;
    }
    else {
      phoneNumber = `tel:${phoneNumber}`;
    }
    Linking.openURL(phoneNumber)
      // .then(supported => {
      // if (!supported) {
      //     console.log('Phone number is not available');
      //   } else {
      //     return Linking.openURL(phoneNumber);
      // }
      // })
      .catch(err => console.log(err));
    console.log(phoneNumber, '----phoneNumber')
  };

  /**************************************************** callWhatapps ***************************************************/
  callWhatapps(item) {
    // let phoneNumber = '';
    // if (Platform.OS === 'android') {
    //     phoneNumber = `tel:${this.props.data.tel}`;
    // }
    // else {
    //     phoneNumber = `telprompt:${this.props.data.tel}`;
    // }
    // Linking.openURL(phoneNumber);
    let phoneNumber = item.mobile;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phoneNumber}`;
    }
    else {
      phoneNumber = `tel:${phoneNumber}`;
    }
    Linking.openURL(`whatsapp://send?phone=${phoneNumber}`)
      // .then(supported => {
      // if (!supported) {
      //     console.log('Phone number is not available');
      //   } else {
      //     return Linking.openURL(phoneNumber);
      // }
      // })
      .catch(err => console.log(err));
    console.log(phoneNumber, '----phoneNumber')
  };

  /************************************ sharing function ***********************************/
  sharing(item) {
    const { langPrint, data } = this.state;
    console.log('sharing');
    Share.share(
      {
        message: langPrint === 'zh' ?
          `${global.t('Destination')}: ${data.other_caption}` + "\n" +
          `${global.t('Country')}: ${data.country}` + "\n" +
          `${global.t('Tour_Code')}: ${item.code}` + "\n" +
          `${global.t('Departure')}: ${item.departure_date}` + "\n" +
          `${global.t('Arrival_Date')}: ${item.arrival_date}` + "\n" +
          `${global.t('cut_off_date')}: ${item.cut_off_date}` + "\n" +
          `${item.guaranteed_departure === true ? `${global.t('Status')}: ${global.t('Guaranteed_Departure')}` : null}` + "\n" +
          `${global.t('Price')}: RM ${item.price}` + "\n" +
          `${global.t('Itinerary')}: ${data.file_url}`
          :
          `Destination: ${data.caption}` + "\n" +
          `Country: ${data.country}` + "\n" +
          `Tourcode: ${item.code}` + "\n" +
          `Departure: ${item.departure_date}` + "\n" +
          `Arrival Date: ${item.arrival_date}` + "\n" +
          `Cut off Date: ${item.cut_off_date}` + "\n" +
          `${item.guaranteed_departure === true ? "Status: Guaranteed Departure" : null}` + "\n" +
          `Price: RM ${item.price}` + "\n" +
          `Itinerary: ${data.file_url}`
        ,
        title: 'Golden Destinations',
        url: data.file_url
      }
    )
  }


  render() {
    const { modalVisible, agentVisble, data, langPrint, imageCover, tourData, isLoading, stateData, stateValue, cityValue,
      cityData, agentData, isLoadingAgent, dwnLoading, dwnDisabled } = this.state;
    const htmlContent = data.description;
    console.log('SeriesShowPage', this.props.id);
    console.log('data---SeriesShowPage', data)
    // console.log('data---tours', tourData);
    // console.log('data---imageCover', imageCover);
    // console.log('stateData', stateData)
    console.log('agentData', agentData)

    return (
      <View style={{ flex: 1 }}>
        {
          isLoading === true ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
              <ActivityIndicator size="large" color="#de2d30" />
            </View> :
            <ScrollView>
              <ImageBackground source={{ uri: imageCover }} style={styles.imgBg}>
                <View style={styles.overlay} />
                <View style={styles.textWarp}>
                  <Text style={styles.titleHeader}>{data.country}</Text>
                  <View style={styles.lineHeader} />
                </View>
              </ImageBackground>

              <View style={{ padding: 10 }}>
                <View style={{ marginTop: 20 }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 18, }}>{langPrint === 'zh' ? data.other_caption : data.caption}</Text>
                  <ScrollView horizontal={true} style={{ marginTop: 10 }}>
                    <View style={styles.dateWarp}>
                      <Text style={styles.dateSty}>{data.category} : {data.code}</Text>
                    </View>
                  </ScrollView>

                  <View style={{ marginTop: 20 }}>
                    <Tabs renderTabBar={() => <ScrollableTab style={{ height: 80, backgroundColor: '#fff' }} tabsContainerStyle={{ backgroundColor: '#fff' }} />} locked={true} >
                      {
                        data && tourData.map((item, index) => {
                          return (
                            <Tab key={index} heading={(
                              <View style={{ flexDirection: 'column', }}>
                                {
                                  item.guaranteed_departure === false ? <Text>{item.departure_date}</Text> :
                                    <View style={{ flexDirection: 'row' }}>
                                      <Text>{item.departure_date}</Text>
                                      <Badge success style={styles.badge}>
                                        <Text style={{ fontSize: 12 }}>G</Text>
                                      </Badge>
                                    </View>
                                }
                                <Text note>{global.t('from')}</Text>
                                <Text >RM {item.price}</Text>
                              </View>
                            )}>
                              <View style={{ backgroundColor: '#e9ecef' }}>

                                {/*************************** things to know ***************************/}
                                <View style={{ backgroundColor: '#fff', marginTop: 20, padding: 20 }}>

                                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', }}>{global.t('things_know')}</Text>
                                    <TouchableOpacity onPress={this.sharing.bind(this, item)} >
                                      <IconETY name="share" size={24} style={{ color: '#0095ff', }} />
                                    </TouchableOpacity>
                                  </View>

                                  <View style={{ marginTop: 20 }}>
                                    {
                                      item.insurance === "" ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="user" size={18} style={styles.iconThings} />
                                          <Text>{langPrint === 'zh' ? "免费旅行保险" : item.insurance}</Text>
                                        </View>
                                    }
                                    {
                                      item.guaranteed_departure === false ?
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="male" size={18} style={styles.iconThings} />
                                          <Text>{global.t('Deposit_pax')} RM {item.deposit}</Text>
                                        </View> :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconMIC name="cash-multiple" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Collect_Payment')}</Text>
                                        </View>
                                    }
                                    {
                                      item.guaranteed_departure === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10, }}>
                                          <Badge success style={[styles.badge, { marginRight: 15, marginLeft: 5 }]}><Text>G</Text></Badge>
                                          <Text>{global.t('Guaranteed_Departure')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['airport_taxes'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconMTI name="receipt" size={18} style={styles.iconThings} />
                                          <Text>{global.t('Airport_Taxes')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['group_departure'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="group" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Group_Departure')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['tour_leader'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconMIC name="account-check" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Tour_Leader')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['luggage'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="shopping-bag" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Check_baggage')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['wifi'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="wifi" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Share_Wifi')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['meal_onboard'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconMIC name="food-fork-drink" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Meal_Onboard')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['hotel'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="bed" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Hotel')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['gratuities'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="dollar" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Gratuities')}</Text>
                                        </View>
                                    }
                                    {
                                      data.includings['acf'] === false ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="money" size={20} style={styles.iconThings} />
                                          <Text>{global.t('Agency_Collection')} (ACF)</Text>
                                        </View>
                                    }
                                    {
                                      data.file_url === "" ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="clipboard" size={18} style={styles.iconThings} />
                                          <TouchableOpacity onPress={this.downloadFile.bind(this, data)} disabled={dwnDisabled}>
                                            <Text style={{ color: dwnDisabled === true ? '#d6d9dc' : '#0095ff' }} >{global.t('Itinerary_Download')}</Text>
                                          </TouchableOpacity>
                                        </View>
                                    }
                                    {
                                      item.cut_off_date === "" ? null :
                                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                          <IconFAS name="clock-o" size={18} style={styles.iconThings} />
                                          <Text>{global.t('cut_off_date')} : {item.cut_off_date}</Text>
                                        </View>
                                    }
                                  </View>
                                </View>

                                {/*************************** flights ***************************/}

                                {
                                  item.flights.map((flight, i) => {
                                    return (
                                      <View key={i} style={{ backgroundColor: '#fff', marginTop: 20, padding: 20 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                          <IconMTI name="flight-takeoff" size={20} />
                                          <Text style={{ paddingLeft: 10, flex: 1, flexWrap: 'wrap', fontSize: 14 }}>{flight.from_airport}</Text>
                                          <IconANT name="arrowright" size={20} style={{ paddingLeft: 10, }} />
                                          <Text style={{ paddingLeft: 10, flex: 1, flexWrap: 'wrap', fontSize: 14 }}>{flight.to_airport}</Text>
                                        </View>
                                        <Text note style={{ fontWeight: 'bold', marginTop: 10 }}>{flight.departure_date}</Text>
                                        <View style={{ marginTop: 20, flexDirection: 'row' }}>
                                          <Image source={{ uri: flight.airline_logo }} style={{ width: 60, height: 60 }} />
                                          <View style={{ marginLeft: 10 }}>
                                            <View style={{ flexDirection: 'row' }}>
                                              <Text style={{ fontWeight: 'bold', paddingRight: 10 }}>{flight.airline}</Text>
                                              <Text style={{ fontWeight: 'bold', color: 'gray' }}>{flight.flight_no}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                              <View style={{ paddingRight: 20 }}>
                                                <Text style={{ flex: 1, flexWrap: 'wrap', }}>{flight.departure_time}</Text>
                                                <Text style={{ flex: 1, flexWrap: 'wrap', color: 'gray', fontSize: 12 }}>{flight.from_airport}</Text>
                                              </View>
                                              <View style={{ paddingRight: 20 }}>
                                                <IconANT name="arrowright" size={20} />
                                              </View>
                                              <View>
                                                <Text style={{ flex: 1, flexWrap: 'wrap' }}>{flight.arrival_time}</Text>
                                                <Text style={{ flex: 1, flexWrap: 'wrap', color: 'gray', fontSize: 12 }}>{flight.to_airport}</Text>
                                              </View>
                                            </View>
                                          </View>
                                        </View>
                                      </View>
                                    )
                                  })
                                }



                                {/*************************** Price Details ***************************/}
                                <View style={{ marginTop: 20, }}>
                                  <Text style={{ fontWeight: 'bold', marginBottom: 10 }}>{global.t('Price_Details')}</Text>
                                  <View style={{ backgroundColor: '#fff', padding: 20, borderTopWidth: 4, borderTopColor: 'red' }}>
                                  <ScrollView horizontal={true} style={{width: '100%'}}>
                                    <View style={{ position: 'relative', flexDirection: 'row', width: '100%', paddingTop: 60 }}>
                                      <View style={{ width: 450, }}>
                                        {
                                          [global.t('Adult'), global.t('Child_twin'), global.t('Child_extra_bed'), global.t('Child_no_bed')].map((item) => {
                                            return (
                                              <View style={styles.lefthader}>
                                                <Text style={{ fontWeight: 'bold' }}>{item}</Text>
                                              </View>
                                            )
                                          })
                                        }
                                      </View>
                                      <View style={{ position: 'absolute', bottom: 0, right: 0}}>
                                        <View style={{ flexDirection: 'row', }}>
                                          {
                                            item.prices.map((price, i) => {
                                              return (
                                              
                                                <View style={{
                                                  marginRight: 3,
                                                  backgroundColor:
                                                    price.type === 'normal' ? '#f89623c4' :
                                                      price.type === 'early_bird' ? '#fda53ead' :
                                                        price.type === 'specialoffer' ? '#ed8812d9' :
                                                          price.type === 'specialdeal' ? '#ed8812d9' :
                                                            price.type === 'superpromo' ? '#ed8812d9' :
                                                              price.type === 'promo' ? '#e07900e6' : ''
                                                }}>
                                                  <View style={styles.pricelistheader}>
                                                    <Text style={{ color: '#fff', fontWeight: 'bold' }}>
                                                      {
                                                        price.type === 'normal' ? 'Normal' :
                                                          price.type === 'early_bird' ? 'Early Bird' :
                                                            price.type === 'specialoffer' ? 'Special Offer' :
                                                              price.type === 'specialdeal' ? 'Special Deal' :
                                                                price.type === 'superpromo' ? 'Super Promo' :
                                                                  price.type === 'promo' ? 'Promo' : ''
                                                      }
                                                    </Text>
                                                    <Text note style={{ color: '#fff', fontWeight: 'bold', paddingTop: 3 }}>Left</Text>
                                                  </View>
                                                  <View style={styles.listcontent}><Text>RM {price.adult}</Text></View>
                                                  <View style={styles.listcontent}><Text>RM {price.child_twin}</Text></View>
                                                  <View style={styles.listcontent}><Text>RM {price.child_with_bed}</Text></View>
                                                  <View style={styles.listcontent}><Text>RM {price.child_no_bed}</Text></View>
                                                </View>
                                            
                                              )
                                            })
                                          }
                                        </View>
                                      </View>
                                    </View>
                                    </ScrollView>
                                  </View>
                                </View>
                               <Text>{data.description}</Text>
                               <HTML
                                        html={htmlContent}
                                        // tagsStyles={{
                                        //   img: { width: '100%', resizeMode: 'contain' },
                                        //   // p: { padding: 10, }
                                        // }}
                                      />

                                <View style={{ backgroundColor: '#fff', marginTop: 20,}}>

                                  {/*************************** description ***************************/}
                                  <View style={{ borderBottomColor: '#eee', borderBottomWidth: .5, padding: 20,  }}>
                                    <Text style={{ fontWeight: 'bold', }}>{global.t('Description')}</Text>
                                    {/* <View style={{ marginTop: 10 , paddingBottom: 30}}>
                                     
                                      <HTML
                                        html={htmlContent}
                                        tagsStyles={{
                                          img: { width: '100%', resizeMode: 'contain' },
                                          // p: { padding: 10, }
                                        }}
                                      />
                                    </View> */}
                                     <HTML
                                        html={htmlContent}
                                        // tagsStyles={{
                                        //   img: { width: '100%', resizeMode: 'contain' },
                                        //   // p: { padding: 10, }
                                        // }}
                                      />
                                      <Text>{data.description}</Text>
                                  </View>
                                  <HTML
                                        html={htmlContent}
                                        
                                        // tagsStyles={{
                                        //   img: { width: '100%', resizeMode: 'contain' },
                                        //   // p: { padding: 10, }
                                        // }}
                                 
                                      />

                                  {/*************************** Overview of the itinerary ***************************/}
                                  {/* <View style={{ borderBottomColor: '#eee', borderBottomWidth: .5, padding: 20 }}>
                                    <Text style={{ fontWeight: 'bold', }}>Overview of the itinerary</Text>
                                    <View style={{ marginTop: 10 }}>
                                      <Accordion dataArray={item.itinerary} expanded={0} />
                                    </View>
                                    <View style={{ marginTop: 10 }}>
                                      <Text note >Tips : Please check the packing list and prepare the required equipment in advance.</Text>
                                    </View>
                                  </View> */}

                                  {/*************************** Terms and Conditions ***************************/}
                                  {/* <View style={{ padding: 20 }}>
                                    <View style={{ marginTop: 10 }}>
                                      <Accordion dataArray={item.terms} expanded={0} />
                                    </View>
                                  </View> */}




                                </View>






                              </View>

                            </Tab>
                          )
                        })
                      }
                    </Tabs>

                  </View>


                  {/* <ScrollableTabView
              renderTabBar={() => <ScrollableTabBar />}
              ref={(tabView) => { this.tabView = tabView; }}
            >
             <View  tabLabel='Tab #1'>
             </View>
            </ScrollableTabView> */}
                  {/* <TouchableOpacity tabLabel='Back' onPress={() => this.tabView.goToPage(0)}>
              <Text>Lets go back!</Text>
            </TouchableOpacity> */}

                </View>
              </View>
            </ScrollView>
        }


        {/*************************** Terms and Conditions ***************************/}
        <View style={styles.agent}>
          <TouchableOpacity onPress={() => this.openModal()}>
            <View style={styles.agentBtn}>
              <IconANT name="customerservice" size={25} style={{ color: '#fff', }} />
            </View>
          </TouchableOpacity>
        </View>


        {/*************************** Contact Agent modal ***************************/}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>

          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={() => this.closeModal()} style={styles.btnClose}>
              <IconANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>
            <ScrollView style={{ width: '90%', top: '15%' }}>
              <Card >
                <View style={{ padding: 20, borderBottomWidth: .5, borderBottomColor: '#ccc' }}>
                  <Text style={{ fontWeight: 'bold' }}>{global.t('Contact_Agent')}</Text>
                </View>
                <View style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 5, paddingBottom: 10 }}>
                  <View >
                    <Select
                      onSelect={this.onSelect.bind(this)}
                      defaultText={stateValue}
                      style={{ borderWidth: 0, width: '95%', borderBottomWidth: .5, borderBottomColor: '#eee' }}
                      textStyle={{ color: 'gray', textAlign: 'left', width: '100%', fontWeight: 'bold', }}
                      backdropStyle={{ backgroundColor: "#00000078" }}
                      optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                      animationType='none'
                      transparent={true}
                      indicator='down'
                      indicatorColor='#000'
                      indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                      indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 25 : 20, marginRight: -20 }}
                    >
                      {
                        stateData.map((state) => {
                          return (
                            <Option value={state.f_name}>
                              {state.f_name}
                            </Option>
                          )
                        })
                      }
                    </Select>
                  </View>
                  <View>
                    <Select
                      onSelect={this.onSelectCity.bind(this)}
                      defaultText={cityValue}
                      style={{ borderWidth: 0, width: '95%', borderBottomWidth: .5, borderBottomColor: '#eee' }}
                      textStyle={{ color: 'gray', textAlign: 'left', width: '100%', fontWeight: 'bold', }}
                      backdropStyle={{ backgroundColor: "#00000078" }}
                      optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                      animationType='none'
                      transparent={true}
                      indicator='down'
                      indicatorColor='#000'
                      indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                      indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 25 : 20, marginRight: -20 }}
                    >
                      {
                        cityData.map((item) => {
                          return (
                            <Option value={item.city}>{item.city}</Option>
                          )
                        })
                      }
                    </Select>
                  </View>

                  <Button full danger style={{ marginTop: 20, marginBottom: 20 }} onPress={() => this.fetchAgent()}>
                    <Text>{global.t('Find_Agent')}</Text>
                  </Button>

                  {/*************************** Contact Agent list ***************************/}
                  <View>
                    {
                      agentVisble === true ?
                        <Text style={{ fontWeight: 'bold', marginBottom: 20 }}>{global.t('Results')}</Text>
                        : null
                    }
                    {
                      agentVisble === true ?
                        <ScrollView style={{ height: 250 }}>
                          {
                            isLoadingAgent === true ?
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                                <ActivityIndicator size="large" color="#de2d30" />
                              </View> :
                              agentData.map((item, i) => {
                                return (
                                  <View style={styles.contectAgent}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                      <View style={{ paddingRight: 5, width: '30%' }}>
                                        <Image source={{ uri: item.agentlogo }} style={{ width: 80, height: 80, resizeMode: 'contain' }} />
                                      </View>
                                      <View style={{ width: '70%' }}><Text style={{ fontSize: 14 }}>{langPrint === 'zh' ? item.company_name_cn : item.company_name}</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row', width: '100%' }}>
                                      <TouchableOpacity onPress={this.callWhatapps.bind(this, item)} style={[styles.callBtnWarp, { backgroundColor: '#4fce5d' }]}>
                                        <IconFAS name="whatsapp" size={25} style={{ color: '#fff', }} />
                                      </TouchableOpacity>
                                      <TouchableOpacity onPress={this.callPhone.bind(this, item)} style={[styles.callBtnWarp, { backgroundColor: '#34b7f1' }]}>
                                        <IconETY name="old-phone" size={25} style={{ color: '#fff', }} />
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                )
                              })
                          }
                        </ScrollView>
                        : null
                    }
                  </View>


                </View>

              </Card>
            </ScrollView>
          </View>
        </Modal>


        {
          dwnLoading === true ?
            <View style={{
              justifyContent: 'center', alignItems: 'center', backgroundColor: '#eff0ee94',
              position: 'absolute', width: '100%', height: dwnLoadingHeight
            }}>
              <ActivityIndicator size="large" color="#de2d30" />
            </View>
            : null
        }



      </View >
    );
  }
}

export default SeriesShowPage;

const styles = StyleSheet.create({
  imgBg: { width: '100%', height: 150, resizeMode: 'cover', justifyContent: 'center' },
  overlay: { backgroundColor: '#00000059', height: 150, position: 'relative' },
  textWarp: { position: 'absolute', alignItems: 'center', width: '100%', padding: 10, },
  titleHeader: { color: '#fff', fontWeight: 'bold', fontSize: 25, marginBottom: 5, },
  lineHeader: { width: 100, height: 2, backgroundColor: '#fff', marginBottom: 10 },
  dateWarp: {
    backgroundColor: '#fff7e6', borderWidth: 1, borderColor: '#ffd591', paddingLeft: 5, paddingRight: 5,
    marginRight: 5, justifyContent: 'center',
  },
  dateSty: { color: '#fa8c16', textAlign: 'center', marginRight: 0, fontSize: 14 },
  agent: { position: 'absolute', bottom: 20, right: 20, },
  agentBtn: {
    backgroundColor: '#0095ff', width: 50, height: 50, justifyContent: 'center',
    alignItems: 'center', borderRadius: 50, shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2, elevation: 10,
  },
  imgModalWarp: { flex: 1, alignItems: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
  imgModal: { width: '100%', height: 400, resizeMode: 'contain', marginTop: 20 },
  inputstyle: { borderBottomWidth: .5, borderBottomColor: '#ccc', padding: 5, width: '100%', marginBottom: 10 },
  contectAgent: {
    borderBottomWidth: .5, borderBottomColor: '#eff0f1', paddingBottom: 10,
    marginBottom: 15, width: '100%'
  },
  lefthader: { backgroundColor: '#e8e8e8', padding: 10, marginBottom: 2, marginRight: 5, width: '100%' },
  pricelistheader: {
    backgroundColor: '#8e8e8e', padding: 10, paddingLeft: 20, paddingRight: 20, marginBottom: 2,
    alignItems: 'center'
  },
  listcontent: { padding: 10, paddingLeft: 20, paddingRight: 20, marginBottom: 2, marginRight: 5, alignItems: 'center' },
  iconThings: { marginRight: 10, width: '10%', textAlign: 'center', color: '#949494' },
  badge: { paddingLeft: 3, paddingRight: 3, paddingTop: 1, paddingBottom: 1, height: 'auto' },
  callBtnWarp: { width: '50%', alignItems: 'center', justifyContent: 'center', padding: 5, }

})
