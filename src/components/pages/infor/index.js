import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button, TouchableHighlight } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView, Linking, } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import RNRestart from "react-native-restart"

class InforScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tokenId: 0,
      langPrint: ''
    }
  }

  componentDidMount() {

    /************************************ Get Language Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes
      })
    })

    /************************************ Get Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
      console.log(asyncStorageRes, '----------------InforScreen')
      this.setState({
        tokenId: asyncStorageRes
      })
      //console.log(this.state.tokenId, '-------------InforScreen')
    });

  }

  /************************************ Logout ***********************************/
  _logout() {
    AsyncStorage.removeItem('USER_TOKEN');
    Actions.Home();
  }


  /****************************************** Change Language ******************************************/
  change = async (language) => {
    console.info("== changing language to:" + language)
    await AsyncStorage.setItem("language", language)
    RNRestart.Restart()
  }



  render() {
    const { tokenId, langPrint } = this.state;
    return (

      <ScrollView>

        <ListItem></ListItem>

        {/******************************** CompanyProfile ********************************/}
        <ListItem icon style={styles.ListItemStyle1} onPress={() => Actions.company()} >
          <Left><IconSIM active name="briefcase" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>{global.t('Company_Profile')}</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** GD Consortiums ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.consortiums()}>
          <Left><IconSIM active name="social-google" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }} ><Text>{global.t('GD_Preferred')}</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** favorite list ********************************/}
        {
          tokenId === null ? (
            null
          ) : (
              <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.favorite()} >
                <Left><IconSIM active name="star" style={styles.iconstyle} /></Left>
                <Body style={{ borderColor: '#ccc' }}><Text>{global.t('Wish_List')}</Text></Body>
                <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
              </ListItem>
            )
        }

        <ListItem></ListItem>

        {/******************************** Privacy Policy ********************************/}
        <ListItem icon style={styles.ListItemStyle1} onPress={() => Actions.privacy()}>
          <Left><IconSIM active name="lock" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>{global.t('Privacy_Policy')}</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Terms and Condition ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.terms()}>
          <Left><IconSIM active name="note" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>{global.t('Terms&Condition')}</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Feedback ********************************/}
        <ListItem icon style={styles.ListItemStyle2} onPress={() => Actions.feedback()} >
          <Left><IconSIM active name="bubbles" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>{global.t('Feedback')}</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        <ListItem></ListItem>

        {/******************************** Facebook ********************************/}
        <ListItem icon style={styles.ListItemStyle1}
          onPress={() => { Linking.openURL('https://www.facebook.com/goldendestinations/') }}>
          <Left><IconSIM active name="social-facebook" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>Facebook</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Instagram ********************************/}
        <ListItem icon style={styles.ListItemStyle2}
          onPress={() => { Linking.openURL('https://www.instagram.com/golden_destinations_official/') }}>
          <Left><IconSIM active name="social-linkedin" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>Instagram</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        {/******************************** Youtube ********************************/}
        <ListItem icon style={styles.ListItemStyle2}
          onPress={() => { Linking.openURL('https://www.youtube.com/channel/UC5-TV4XEQORpKLBXNHtsJfA') }}>
          <Left><IconSIM active name="social-youtube" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>YouTube</Text></Body>
          <Right style={{ borderColor: '#ccc' }}><Icon active name="arrow-forward" /></Right>
        </ListItem>

        <ListItem></ListItem>

        {/******************************** Change language ********************************/}
        <ListItem icon style={styles.ListItemStyle1} >
          <Left><IconSIM active name="globe" style={styles.iconstyle} /></Left>
          <Body style={{ borderColor: '#ccc' }}><Text>{global.t('Change_language')}</Text></Body>
          <Right style={{ borderColor: '#ccc',}}>
            <Button onPress={() => this.change('en')} style={[styles.btnstyle, {paddingRight :5}]}>
              {/* <Image source={require('../../../assets/images/icon/en.png')} style={styles.langicon} /> */}
              <Text style={{ fontWeight: 'bold', color: langPrint === "en" ? '#de2d30' : '#b5b5b5' }}>EN</Text>
            </Button>
            <Text style={{ fontWeight: 'bold', color: '#b5b5b5' }}>/</Text>
            <Button onPress={() => this.change('zh')} style={[styles.btnstyle, {paddingLeft: 5}]}>
              {/* <Image source={require('../../../assets/images/icon/cn.png')} style={styles.langicon} /> */}
              <Text style={{ fontWeight: 'bold', color: langPrint === "zh" ? '#de2d30' : '#b5b5b5' }}>中文</Text>
            </Button>
          </Right>
        </ListItem>

        <ListItem icon style={[styles.ListItemStyle2, { marginBottom: 60 }]}>
          <Left><Text style={{ paddingLeft: 15 }}>{global.t('Versions')}</Text></Left>
          <Body style={{ borderColor: '#ccc' }}></Body>
          <Right style={{ borderColor: '#ccc' }}><Text>4.0.8</Text></Right>
        </ListItem>

        {/******************************** Logout ********************************/}
        {/* {
          this.state.tokenId !== null ? (
            <ListItem icon style={[styles.ListItemStyle2, { marginBottom: 60 }]} onPress={() => this._logout()}>
              <Left><IconSIM active name="logout" style={styles.iconstyle} /></Left>
              <Body><Text>{global.t('Logout')}</Text></Body>
              <Right><Icon active name="arrow-forward" /></Right>
            </ListItem>
          ) : (
              null
            )
        } */}


      </ScrollView>

    );
  }
}



export default InforScreen;

const styles = StyleSheet.create({
  ListWarp: { backgroundColor: '#e9ecef', paddingBottom: 50 },
  ListItemStyle1: { backgroundColor: 'white', marginLeft: 0, },
  ListItemStyle2: { backgroundColor: 'white', marginLeft: 0 },
  iconstyle: { color: '#de2d30', fontSize: 25, marginLeft: 15 },
  langicon: { width: 40, height: 25, },
  btnstyle: {
    backgroundColor: 'transparent', borderColor: 'transparent', borderWidth: 0,
    borderRadius: 0, height: 'auto', elevation: 0
  },

})