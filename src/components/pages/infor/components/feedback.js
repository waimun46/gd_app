import React, { Component } from 'react';
import { Button, Textarea, Form, Toast } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView, TextInput, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import IconFTH from "react-native-vector-icons/Feather";

/************************ Validations Toast Style **************************/
const textStyleSuccess = { color: "#48e81a" };
const textStyle = { color: "yellow" };
const position = "top";
const buttonText = "Okay";
const duration = 3000;
const style = { top: '10%' };


class Feedback extends Component {

    constructor(props) {
        super(props);
        this.state = {
            feedback: '',
            tokenId: ''
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
            //console.log(JSON.parse(asyncStorageRes), 'Feedback----------------tokenId')
            console.log(asyncStorageRes, 'Feedback----------------tokenId')
            this.setState({
                tokenId: asyncStorageRes
            })
        });

    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        if (field == 'feedback') {
            this.setState({ feedback: text })
        }
    }

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(access_token = this.state.tokenId) {
        //console.log(access_token, 'access_token-------');

        if (this.state.feedback === '') {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Content'),
                    buttonText: buttonText, position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else {
            /************************************ Fetch API  ***********************************/
            let url = `https://goldendestinations.com/api/new/enquiry.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&access_token=${access_token}`;
            fetch(url).then((res) => res.json())
                .then(function (myJson) {
                    console.log(myJson, '-----------myJson');

                    if (myJson[0].status === 1) {
                        this.setState({
                            Success: Toast.show({
                                text: global.t('Feedback_Success'),
                                buttonText: buttonText, position: position,
                                duration: duration, textStyle: textStyleSuccess, style: style,
                            })
                        })
                    }
                })
        }
    }


    render() {
        const { feedback } = this.state;
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>
                    <Form style={styles.formwarp}>
                        <View style={styles.textAreaContainer} >
                            <TextInput
                                value={feedback}
                                style={styles.textArea}
                                underlineColorAndroid="transparent"
                                placeholder={global.t('Fill_Content')}
                                placeholderTextColor="grey"
                                numberOfLines={10}
                                multiline={true}
                                autoCorrect={false}
                                autoCapitalize={false}
                                onChangeText={(text) => this.updataValue(text, 'feedback')}
                            />
                        </View>
                    </Form>
                </ScrollView>
                <View style={styles.Buttonwarp}>
                    <Button block style={styles.bottomStyle} onPress={() => this.onSubmit()}>
                        <Text style={styles.BottomText} uppercase={false}>{global.t('Submit')}</Text>
                    </Button>
                </View>
            </View>
        );
    }
}



export default Feedback;
 
const styles = StyleSheet.create({
    formwarp: { padding: 20 },
    Buttonwarp: {
        position: 'absolute', flex: 0.1, left: 0, right: 0, alignItems: 'center', width: '100%',
        ...Platform.select({
          ios: { bottom: -30, },
          android: { bottom: -20, }
        }), flexDirection: 'row',
        ...Platform.select({
          ios: { height: 100, },
          android: { height: 80, }
        }),
      },
      bottomStyle: {
        width: '100%', backgroundColor: '#fc564e', borderRadius: 0,
        ...Platform.select({
          ios: { height: 70 },
          android: { height: 60 }
        }),
      },
      BottomText: { fontSize: 16 , color: '#fff'},
    textAreaContainer: { borderColor: '#ccc', borderWidth: 1, padding: 5 },
    textArea: { height: 150, justifyContent: "flex-start",textAlignVertical: 'top' } 


})