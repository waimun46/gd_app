import React, { Component } from 'react';
import { List, ListItem, } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView, TouchableOpacity, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { PrivacyApi } from '../../../../../PostApi';
import Item from '../../../../themes/components/Item';
import HTML from 'react-native-render-html';


class Privacy extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            langPrint: ''
        }
    }

    componentDidMount() {

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----TermsCondition')
            this.setState({
                langPrint: langStorageRes
            })
        })

        PrivacyApi().then((fetchData) => {
            this.setState({
                data: fetchData[0]
            })
        })


    }

    render() {
        const { data, langPrint } = this.state;
        const htmlContent_en = data.description_en;
        const htmlContent_cn = data.description_cn;
        console.log(data, '-------data')
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>

                    <View style={styles.topimgwarp}>
                        <Image source={require('../../../../assets/images/img/pp.png')} style={styles.topimg} />
                        <View style={styles.toptextwarp}>
                            <Text style={styles.toptext}>{langPrint === 'zh' ? data.title_cn : data.title}</Text>
                        </View>
                    </View>

                    <View style={{ padding: 20 , marginBottom: 50}}>
                        <HTML
                            html={langPrint === 'zh' ? htmlContent_cn : htmlContent_en}
                            tagsStyles={{
                                p: { marginBottom: 15, lineHeight: 20 },
                                h4: { color: '#de2d30',marginBottom: 10 },
                                strong: {color: '#de2d30', marginBottom: 5}
                            }}
                            onLinkPress={(event, href) => { Linking.openURL(href) }}
                        />
                    </View>

                </ScrollView>
            </View>
        );
    }
}



export default Privacy;

const styles = StyleSheet.create({
    topimgwarp: { height: 200, },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30 },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'center', color: '#777', },
    contenttitle: { textAlign: 'left', color: '#000', fontWeight: 'bold', },
    contenttitle2: { textAlign: 'left', color: '#000', fontWeight: 'bold', marginTop: 20 },
    contenttext: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttext2: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttext3: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttext4: { textAlign: 'left', color: '#777', marginTop: 10, flexDirection: 'row' },
    ListItemstyle: { borderBottomWidth: 0, marginLeft: 0, paddingTop: 5, paddingBottom: 5 },
    ListItemViewstyle: { paddingRight: 10 },
    ListItemiconstyle: { fontSize: 15 },
    icontext: { color: '#777' },


})