import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button, } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import IconFTH from "react-native-vector-icons/Feather";


class CompanyProfile extends Component { 
    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>
                    <View style={styles.topimgwarp}>
                        <Image source={require('../../../../assets/images/img/company.jpg')} style={styles.topimg} />
                        <View style={styles.toptextwarp}>
                            <Text style={styles.toptext}>{global.t('About_Us')}</Text>
                        </View>
                    </View>
                    <View style={{ paddingBottom: 100 }}>
                        <Text style={styles.contenttitle}>{global.t('Welcome_G_D')}</Text>
                        <Text style={styles.contenttext}>{global.t('Welcome_content_1')}</Text>
                        <Text style={styles.contenttext2}>{global.t('Welcome_content_2')}</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}



export default CompanyProfile;

const styles = StyleSheet.create({
    topimgwarp: { height: 200 },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30 },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'center', padding: 20, fontSize: 16, color: '#de2d30', fontWeight: 'bold' },
    contenttitle: { textAlign: 'center', marginTop: 20, padding: 20, fontSize: 23, color: '#de2d30', fontWeight: 'bold' },
    contenttext: { textAlign: 'center', padding: 20, color: '#777', lineHeight: 25, },
    contenttext2: { textAlign: 'center', padding: 20, marginTop: -10, color: '#777', lineHeight: 25, },


})