import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Share, Image, RefreshControl } from 'react-native';
import { List, ListItem, Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import IconFOU from "react-native-vector-icons/Foundation";
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import IconANT from "react-native-vector-icons/AntDesign";


/************************* If Api Data Empty Show Local data *************************/
const localdata = [
  { "airlinelocal_en": "To Be Confirmed", "airlinelocal_cn": "待确认", "statuslocal_en": "Selling Fast", "statuslocal_cn": "热销中" }
]


class AddFavorite extends Component {

  constructor(props) {
    super(props);
    this.state = {
      getFavorite: [],
      langPrint: '',
      tokenId: '',
      refreshing: true,

    }
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    fetchData().then(() => {
      this.setState({ refreshing: false });
    });
  }

  componentDidMount() {

    /************************************ Get Language Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes,

      })
    })


    /************************************ Get all Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
      //console.log(asyncStorageRes, '----------------AsyncStorage')
      this.setState({
        tokenId: asyncStorageRes
      })

      this.getFavoriteList();

    });


  }



  getFavoriteList(access_token = this.state.tokenId) {
    //console.log('access_token', access_token)

    let url = `https://www.goldendestinations.com/api/new/liked.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&access_token=${access_token}`;

    fetch(url).then((res) => res.json())
      .then((resJson) => {
        this.setState({
          getFavorite: resJson,
          refreshing: false
        })
      })


  }

  /************************* removeFavorite function *************************/
  removeFavorite(item, index, tourcode = item.tourcode, access_token = this.state.tokenId) {

    console.log(tourcode, '---tourcode', access_token, '---access_token')

    let url = `https://www.goldendestinations.com/api/new/unlike.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

    fetch(url).then((res) => res.json())
      .then(function (resJson) {
        //console.log(resJson, '-----------resJson remove');
        if (resJson[0].status === 1) {
          console.log(resJson, 'remove-------------remove');

        }
        else {
          alert(resJson[0].error)
        }
      })
  };

  // removeFavorite (index, e) {
  //   const list = Object.assign([],this.state.getFavorite);
  //   list.splice(index, 1);
  //   this.setState({
  //     getFavorite: list
  //   })
  // }


  /************************* sharing function *************************/
  sharing(item) {
    console.log('sharing');
    Share.share(
      {
        message: `Destination: ${item.title}` + "\n" + `Tourcode: ${item.tour_code}` + "\n" + `Departure: ${item.departure}` + "\n" +
          `Airline: ${item.airline}` + "\n" + `Status: ${item.status === "" ? localdata[0].statuslocal_en : item.status}` + "\n" +
          `Price: RM ${item.price}` + "\n" + `Itinerary: ${item.itinerary}`,
        title: 'Golden Destinations',
        url: item.itinerary
      }
    )
  }




  /************************* Download PDF File *************************/
  downloadFile(item) {
    function getLocalPath(url) {
      const filename = url.split('/').pop();
      return `${RNFS.DocumentDirectoryPath}/${filename}`;
    }

    const url = item.itinerary;
    const localFile = getLocalPath(url);

    const options = {
      fromUrl: url,
      toFile: localFile
    };
    RNFS.downloadFile(options).promise
      .then(() => FileViewer.open(localFile))
      .then(() => {
        // success
      })
      .catch(error => {
        // error
      });
  }



  /************************* _renderItem function *************************/
  _renderItem = ({ item, index }) => {
    const { langPrint, active: sd } = this.state;
    const localColor = { color: sd === index ? "#ffc43f" : "#fff" }

    return (
      <Card >
        <CardItem style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#fcf5f5', marginRight: 0 }}>
          <Body style={{ marginRight: 0, }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ alignItems: 'flex-start', width: '5%', }}>
                <Image source={{ uri: item.brand_logo }} style={{ width: 20, height: 20 }} />
              </View>
              <View style={{ width: '95%' }}>
                <Text note style={{ fontWeight: 'bold', textAlign: 'center', marginLeft: 5 }}>
                  {langPrint === 'zh' ? item.cn_title : item.title}
                </Text>
              </View>
            </View>
          </Body>
        </CardItem>
        <CardItem cardBody >
          <View style={{ flexDirection: 'row' }}>
            <View style={{ paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15 }}>
              <View style={styles.textWarp}>
                <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Tour_Code')}: </Text>
                <Text note style={styles.TextInner2}>{item.tourcode}</Text>
              </View>
              <View style={styles.textWarp}>
                <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Airline')}: </Text>
                {
                  langPrint === 'zh' ? (
                    <Text note style={styles.TextInner2}>{item.airline === null ? localdata[0].airlinelocal_cn : item.airline}</Text>
                  ) : (
                      <Text note style={styles.TextInner2}>{item.airline === null ? localdata[0].airlinelocal_en : item.airline}</Text>
                    )
                }

              </View>
              <View style={styles.textWarp}>
                <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Status')}: </Text>
                {
                  langPrint === "zh" ? (
                    <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                      {item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}
                    </Text>
                  ) : (
                      <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                        {item.status === "" ? localdata[0].statuslocal_en : item.status}
                      </Text>
                    )
                }
              </View>
              <View style={styles.textWarp}>
                <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Departure')}: </Text>
                <Text note style={styles.TextInner2}>{item.departure}</Text>
              </View>

              <View style={{ flexDirection: 'row', }}>
                <Text note style={{ width: '30%', color: '#00953b', marginTop: 2 }}>{global.t('Price')}: </Text>
                <Text note style={styles.Textprice}>{item.price}</Text>
              </View>
            </View>

          </View>
        </CardItem>
        <CardItem style={{ borderTopWidth: .5, borderTopColor: '#ccc' }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ width: '33.33%', }}>
              <TouchableOpacity onPress={this.removeFavorite.bind(this, item)} >
                <IconANT name="delete" size={20} style={{ textAlign: 'center', color: '#ccc' }} />
              </TouchableOpacity>
            </View>
            <View style={{ width: '33.33%', borderLeftWidth: .5, borderLeftColor: '#ccc', borderRightColor: '#ccc', borderRightWidth: .5 }}>
              <TouchableOpacity onPress={this.sharing.bind(this, item)} >
                <IconFOU name="share" size={20} style={{ textAlign: 'center', color: '#0095ff' }} />
              </TouchableOpacity>
            </View>
            <View style={{ width: '33.33%' }}>
              <TouchableOpacity onPress={this.downloadFile.bind(this, item)}>
                <IconFOU name="download" size={20} style={{ textAlign: 'center', color: '#de2d30' }} />
              </TouchableOpacity>
            </View>
          </View>

        </CardItem>
      </Card>
    )
  }



  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Wish_List')}</Text>
      </View>
    );
  };


  render() {

    const { getFavorite, getSearch, getUpcoming, getSeries, } = this.state;
    //console.log('getFavorite', getFavorite)


    return (

      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.getFavoriteList()}
          />
        }>
        <View style={styles.listWarp}>
          <FlatList
            data={getFavorite}
            extraData={this.state}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => String(index)}
            ListEmptyComponent={this.ListEmpty}
          />
        </View>
      </ScrollView>

    );
  }
}



export default AddFavorite;

const styles = StyleSheet.create({
  searchContainer: { backgroundColor: 'white', },
  listWarp: { marginBottom: 100 },
  IconColor: { color: '#de2d30', textAlign: 'center', paddingTop: 30, },
  TextTitle: { color: '#de2d30', fontWeight: 'bold', },
  TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
  TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, marginTop: 2 },
  Textprice: { color: '#00953b', width: '50%', marginTop: 2 },
  Textshare: { width: '20%', },
  Textsharetext: { color: '#0095ff', fontWeight: 'bold', textAlign: 'center' },
  textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
  textWarp: { flexDirection: 'row', marginTop: 2, },
  downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
  dwnwarp: { width: '100%', },
  bodystyle: {
    width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
    paddingTop: 0, borderBottomWidth: 0
  },
  texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
  dwntext: { marginTop: 15, textAlign: 'center', marginRight: 0, color: '#de2d30' },
  scrollBtn: {
    position: 'absolute', flex: 0.1, right: 10, bottom: -10, borderRadius: 50,
    backgroundColor: '#00000057', padding: 10, width: 50, height: 50, marginBottom: 100,
    color: 'red', textAlign: 'center'
  },
  iconTop: { fontSize: 22, color: 'white', textAlign: 'center', fontWeight: 'bold', paddingTop: 3 },
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },
})