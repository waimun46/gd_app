import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { TermsApi } from '../../../../../PostApi';
import HTML from 'react-native-render-html';

class TermsCondition extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            langPrint: ''
        }
    }

    componentDidMount() {

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----TermsCondition')
            this.setState({
                langPrint: langStorageRes
            })
        })

        TermsApi().then((fetchData) => {
            this.setState({
                data: fetchData[0]
            })
        })
    }

    render() {
        const { data, langPrint } = this.state;
        const htmlContent_en = data.description_en;
        const htmlContent_cn = data.description_cn;
        console.log(data, '----------data trems')
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <ScrollView>
                    <View style={styles.topimgwarp}>
                        <Image source={require('../../../../assets/images/img/tc.jpg')} style={styles.topimg} />
                        <View style={styles.toptextwarp}>
                            <Text style={styles.toptext}>{langPrint === 'zh' ? data.title_cn : data.title}</Text>
                        </View>
                    </View>

                    <View style={{ padding: 20,marginBottom: 50}}>
                        <HTML
                            html={langPrint === 'zh' ? htmlContent_cn : htmlContent_en}
                            tagsStyles={{
                                p: { marginBottom: 10 , lineHeight: 20},
                                strong: {color: '#de2d30'}
                            }}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}



export default TermsCondition;

const styles = StyleSheet.create({
    topimgwarp: { height: 200, },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30 },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    subtitle: { textAlign: 'center', color: '#de2d30', },
    contenttitle: { textAlign: 'left', color: '#000', fontWeight: 'bold', marginTop: 20 },
    contenttext: { textAlign: 'left', color: '#777', marginTop: 10 },
    contenttitle1: { textAlign: 'left', color: '#000', fontWeight: 'bold', marginTop: 30 },

})