import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button, Textarea, Form, } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar, ScrollView, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import IconFTH from "react-native-vector-icons/Feather";
import { Select, Option } from "react-native-chooser";
import { GDConsortiumsApi } from '../../../../../PostApi';


const numColumns = 2;

class GDConsortiums extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            data: [],
            value: "",
            selected_data: [],
            isLoading: true,
            childVisible: false,
            imageDisplay: [],
            fistValue: [],
            langPrint: ''
        };

    }


    componentDidMount() {

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })

        /************************* Fetch Data in Api *************************/
        GDConsortiumsApi().then((fetchData) => {
            this.setState({
                data: fetchData,
                selected_data: fetchData,
                isLoading: false,
                imageDisplay: fetchData[0],
                fistValue: fetchData[0]

            })
        })

    }

    /************************* Select Option Value filter *************************/
    onSelect(value) {
        this.setState({
            selected_data: this.state.data.filter((element) => {
                return this.state.langPrint === 'zh' ? element.cat_title_cn == value : element.cat_title == value
            }),
            value: value,
            childVisible: true

        });
    }

    /************************* Render Item In FlatList *************************/
    renderItem = ({ item }) => {
        //console.log(item, '----------item')
        return (

            <View style={styles.ImageWarpper}>

                <TouchableOpacity onPress={() => Actions.Agentinforgd({ data: item })}>
                    <View style={styles.ImageInner} >
                        <View style={styles.ImageInnerWarp}>
                            <Image source={{ uri: item.agentlogo }}
                                style={styles.ImageStyle} />
                        </View>
                        <View style={{ borderRadius: 5, padding: 10,backgroundColor: '#fcf5f5',}}>
                            <Text style={styles.TextImage} numberOfLines={2}>{item.company_name}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>

        )
    }
    render() {
        const { data, selected_data, isLoading, childVisible, imageDisplay, fistValue, langPrint } = this.state;
        console.log(this.state.data.cat_title_cn, '----------------data')
        console.log(fistValue, '----------------data')

        return (
            <View>
                <ScrollView>
                    <Select
                        onSelect={this.onSelect.bind(this)}
                        defaultText={this.state.value || (langPrint === 'zh' ? fistValue.cat_title_cn : fistValue.cat_title)}
                        onChange={(e) => this.onSelectCurrency(e.target.value)}
                        style={{ borderWidth: 0, width: '100%', height: 50, backgroundColor: '#fff' }}
                        textStyle={{ color: 'red', textAlign: 'center', fontSize: 17, width: '100%', }}
                        backdropStyle={{ backgroundColor: "#00000078" }}
                        optionListStyle={{ backgroundColor: "#F5FCFF", height: 200 }}
                        animationType='none'
                        transparent={true}
                        indicator='down'
                        indicatorColor='red'
                        indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                        indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 10 : 2, right: 30 }}
                    >
                        {
                            data.map((item, i) => {
                                return (
                                    <Option value={langPrint === "zh" ? item.cat_title_cn : item.cat_title} key={i}>
                                        {langPrint === "zh" ? item.cat_title_cn : item.cat_title}
                                    </Option>
                                )
                            })
                        }
                    </Select>
                    <View style={{ backgroundColor: '#fff', marginTop: 5, padding: 20 }}>
                        {
                            childVisible ? (
                                selected_data.map((item) => {
                                    return (
                                        <Image source={{ uri: item.logo }}
                                            style={styles.ImagelogoStyle} />
                                    )
                                })
                            ) : (
                                    <Image source={{ uri: imageDisplay.logo }} style={styles.ImagelogoStyle} />
                                )
                        }
                    </View>

                    <View style={{ padding: 15, }}>
                        {
                            isLoading ? (
                                <View style={{ marginTop: 100 }}>
                                    <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Please_Select_Consortiums')}</Text>
                                    <ActivityIndicator size="large" color="#de2d30" />
                                </View>
                            ) : (
                                    childVisible ? (
                                        <FlatList
                                            data={selected_data[0].agent}
                                            renderItem={this.renderItem}
                                            numColumns={numColumns}
                                        />
                                    ) : (
                                            <FlatList
                                                data={selected_data[0].agent}
                                                renderItem={this.renderItem}
                                                numColumns={numColumns}
                                            />
                                        )
                                )
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}



export default GDConsortiums;

const styles = StyleSheet.create({
    FlatContainer: { flex: 1, marginVertical: 10, },
    IconStyle: {
        textAlign: 'center',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    TitleText: {
        textAlign: 'center', fontSize: 18, marginTop: 5, fontWeight: 'bold',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    ContentText: { textAlign: 'center', marginTop: 20, paddingLeft: 10, paddingRight: 10, color: '#606c77' },
    ImageWarpper: { width: '50%', paddingLeft: 20, paddingRight: 20, marginBottom: 20, },
    ImageInner: { backgroundColor: '#fff', borderRadius: 5, },
    ImageInnerWarp: { borderTopRightRadius: 5, borderTopLeftRadius: 5, overflow: 'hidden', borderWidth: 1, borderColor: 'gray' },
    ImageStyle: { height: 150, resizeMode: 'contain', flex: 1, },
    TextImage: {
        textAlign: 'center', fontSize: 11,
        ...Platform.select({
            ios: { color: '#3b4045' },
            android: { color: '#3b4045' }
        })
    },
    TextImage2: { textAlign: 'left', fontSize: 12, color: '#0095ff', marginTop: 5 },
    TopIcon: { resizeMode: 'contain', width: 80, height: 80 },
    ImagelogoStyle: { resizeMode: 'contain', width: '100%', height: 100, }


})