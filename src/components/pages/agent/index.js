import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, Platform, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import { AgentListApi } from '../../../../PostApi';

import { Select, Option } from "react-native-chooser";


const numColumns = 2;



class Agent extends Component {

   constructor(props) {
      super(props);
      this.state = {
         text: '',
         data: [],
         selected_data: [],
         selectedState: null,
         unSelect: true,
         citydata: [],
         value: '',
         udata: [],
         dataDisplay: [],
         stateObj: [],
         langPrint: ''

      };
      this._onPressButton = this._onPressButton.bind(this);
   }



   onSelect(value, ) {
      this.setState({
         //data: this.state.data.slice(1, 2),
         selected_data: this.state.data.filter((element) => {
            return element.city == value
         }),
         stateObj: this.state.dataDisplay.filter(element => {
            return element.city === value
         }),
         value: value,
      });
   }



   componentDidMount() {

      AgentListApi('JOHOR').then((fetchData) => {
         //console.log(fetchData, '-----------dataDisplay');
         this.setState({
            dataDisplay: fetchData,
            unSelect: true,
            stateObj: fetchData,
            value: fetchData[0].city,
         })
      })

      /************************************ Get Language Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem("language").then(langStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Agent')
         this.setState({
            langPrint: langStorageRes
         })
      })
   }


   /************************* Fetch Data in Api and Pass in onPress Button *************************/
   _onPressButton(state_name) {
      AgentListApi(state_name).then((fetchData) => {
         //console.log(fetchData);
         this.setState({
            data: fetchData,
            selected_data: fetchData,
            selectedState: state_name,
            unSelect: false,
            citydata: fetchData.city,
            value: fetchData[0].city,
         })
      })
   }

   /************************* Render Item in FlatList *************************/
   renderItem = ({ item }) => {
      const { langPrint } = this.state;
      return (
         <View style={styles.ImageWarpper}>
            <TouchableOpacity onPress={() => Actions.Agentinfor({ data: item })}>
               <View style={styles.ImageInner} >
                  <View style={styles.ImageInnerWarp}>
                     <Image source={{ uri: item.agentlogo }}
                        style={styles.ImageStyle} />
                  </View>
                  <View style={styles.textWarp}>
                     <Text style={styles.TextImage} numberOfLines={1}>{langPrint === 'zh' ? item.company_name_cn : item.company_name}</Text>
                     {/* <Text style={styles.TextImage2}></Text>*/}
                  </View>
               </View>
            </TouchableOpacity>
         </View>
      )
   }

   /************************* Render Item in FlatList *************************/
   _renderItem = ({ item }) => {
      const { langPrint } = this.state;
      return (
         <View style={styles.ImageWarpper}>
            <TouchableOpacity onPress={() => Actions.Agentinfor({ data: item })}>
               <View style={styles.ImageInner} >
                  <View style={styles.ImageInnerWarp}>
                     <Image source={{ uri: item.agentlogo }}
                        style={styles.ImageStyle} />
                  </View>
                  <View style={styles.textWarp}>
                     <Text style={styles.TextImage} numberOfLines={1}>{langPrint === 'zh' ? item.company_name_cn : item.company_name}</Text>
                     {/* <Text style={styles.TextImage2}></Text>*/}
                  </View>
               </View>
            </TouchableOpacity>
         </View>
      )
   }


   render() {
      const { unSelect, dataDisplay, value } = this.state;
      console.log(value, 'value----------------city')

      /************************* Remove Value option remove dupicate *************************/
      const uniKeys = [...(new Set(this.state.data.map(({ city }) => city)))];
      const uniKeysJohor = [...(new Set(dataDisplay.map(({ city }) => city)))];

      return (
         <View >
            <ScrollView>
               <View style={{ backgroundColor: 'white', padding: 10 }}>
                  <ScrollView horizontal={true} scrollEventThrottle={16}>
                     <View style={{ padding: 10, flexDirection: 'row' }}>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('JOHOR')}>
                           {
                              unSelect ?
                                 <View style={{ opacity: this.state.selectedState === "JOHOR" ? .2 : .9 }}>
                                    <Image
                                       source={require('../../../assets/images/icon/johor.png')}
                                       style={[styles.TopIcon, { opacity: this.state.opacity }]}
                                    />
                                    <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Johor')}</Text>
                                 </View>
                                 :
                                 <View style={{ opacity: this.state.selectedState === "JOHOR" ? .9 : .2 }}>
                                    <Image
                                       source={require('../../../assets/images/icon/johor.png')}
                                       style={[styles.TopIcon, { opacity: this.state.opacity }]}
                                    />
                                    <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Johor')}</Text>
                                 </View>
                           }
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('KUALA LUMPUR')}>
                           <View style={{ opacity: this.state.selectedState === "KUALA LUMPUR" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/KL.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('KL')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('KELANTAN')}>
                           <View style={{ opacity: this.state.selectedState === "KELANTAN" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/kld.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Kelantan')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('LABUAN')}>
                           <View style={{ opacity: this.state.selectedState === "LABUAN" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/lb.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Labuan')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('MELAKA')}>
                           <View style={{ opacity: this.state.selectedState === "MELAKA" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/mlk.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Melaka')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('NEGERI SEMBILAN')}>
                           <View style={{ opacity: this.state.selectedState === "NEGERI SEMBILAN" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/ns.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('NS')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('PAHANG')}>
                           <View style={{ opacity: this.state.selectedState === "PAHANG" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/ph.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Pahang')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('PERAK')}>
                           <View style={{ opacity: this.state.selectedState === "PERAK" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/perak.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Perak')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('PULAU PINANG')}>
                           <View style={{ opacity: this.state.selectedState === "PULAU PINANG" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/penang.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('PP')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('SELANGOR')}>
                           <View style={{ opacity: this.state.selectedState === "SELANGOR" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/se.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Selangor')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.test} onPress={() => this._onPressButton('SABAH')}>
                           <View style={{ opacity: this.state.selectedState === "SABAH" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/sabah.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Sabah')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('SARAWAK')}>
                           <View style={{ opacity: this.state.selectedState === "SARAWAK" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/sw.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Sarawak')}</Text>
                           </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this._onPressButton('TERENGGANU')}>
                           <View style={{ opacity: this.state.selectedState === "TERENGGANU" ? .9 : .2 }}>
                              <Image
                                 source={require('../../../assets/images/icon/tg.png')}
                                 style={styles.TopIcon}
                              />
                              <Text style={{ textAlign: 'center', paddingTop: 10 }}>{global.t('Terengganu')}</Text>
                           </View>
                        </TouchableOpacity>
                     </View>
                  </ScrollView>
               </View>

               <View style={{ marginTop: 10, }}>
                  {/* <Text>{this.state.citydata}</Text> */}

                  <Select
                     onSelect={this.onSelect.bind(this)}
                     defaultText={value}
                     onChange={(e) => this.onSelectCurrency(e.target.value)}
                     style={{ borderWidth: 0, width: '100%', height: 50, backgroundColor: '#fff' }}
                     textStyle={{ color: 'red', textAlign: 'center', fontSize: 17, width: '100%', }}
                     backdropStyle={{ backgroundColor: "#00000078" }}
                     optionListStyle={{ backgroundColor: "#F5FCFF", height: 200 }}
                     animationType='none'
                     transparent={true}
                     indicator='down'
                     indicatorColor='red'
                     indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                     indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 10 : 2, right: 30 }}
                  >

                     {
                        unSelect ? (
                           uniKeysJohor.map((item, i) => {
                              return (
                                 <Option value={item} key={i}>{item}</Option>
                              )
                           })
                        ) : (
                              uniKeys.map((item, i) => {
                                 const datamap = this.state.data;
                                 console.log(datamap, '-----------datamap');
                                 console.log(uniKeys, '-----------uniKeys');
                                 return (
                                    <Option value={item} key={i}>{item}</Option>
                                 )
                              })
                           )
                     }



                  </Select>

                  <View style={{ padding: 15, }}>
                     {
                        unSelect ? (
                           <FlatList
                              data={this.state.stateObj}
                              renderItem={this._renderItem}
                              extraData={this.state}
                              numColumns={numColumns}
                              style={styles.FlatContainer}
                           />

                        ) : (
                              <FlatList
                                 data={this.state.selected_data}
                                 renderItem={this.renderItem}
                                 extraData={this.state}
                                 numColumns={numColumns}
                                 style={styles.FlatContainer}
                              //keyExtractor={(item, index) => item.city}

                              />
                           )
                     }
                  </View>

               </View>

            </ScrollView>
         </View >
      );
   }
}


export default Agent;

const styles = StyleSheet.create({
   FlatContainer: { flex: 1, marginVertical: 10, },
   IconStyle: {
      textAlign: 'center',
      ...Platform.select({
         ios: { color: 'black' },
         android: { color: 'black' }
      })
   },
   TitleText: {
      textAlign: 'center', fontSize: 18, marginTop: 5, fontWeight: 'bold',
      ...Platform.select({
         ios: { color: 'black' },
         android: { color: 'black' }
      })
   },
   ContentText: { textAlign: 'center', marginTop: 20, paddingLeft: 10, paddingRight: 10, color: '#606c77' },
   textWarp: { borderBottomLeftRadius: 5, borderBottomRightRadius: 5, padding: 10, backgroundColor: '#fcf5f5', justifyContent: 'center' },
   ImageWarpper: { width: '50%', paddingLeft: 20, paddingRight: 20, marginBottom: 20, },
   ImageInner: { backgroundColor: 'white', borderRadius: 5, },
   ImageInnerWarp: { borderTopRightRadius: 5, borderTopLeftRadius: 5, overflow: 'hidden', borderWidth: 1, borderColor: 'gray' },
   ImageStyle: { height: 150, flex: 1, resizeMode: 'contain' },
   TextImage: {
      textAlign: 'center', fontSize: 11,
      ...Platform.select({
         ios: { color: '#3b4045' },
         android: { color: '#3b4045' }
      })
   },
   TextImage2: { textAlign: 'left', fontSize: 12, color: '#0095ff', marginTop: 5 },
   TopIcon: { resizeMode: 'contain', width: 80, height: 80 },
   test: {
      marginRight: 10,


   }
})
