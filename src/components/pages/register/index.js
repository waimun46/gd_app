import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, ImageBackground, Image, Dimensions, TextInput, Platform, SafeAreaView } from 'react-native';
import { Container, Header, Content, Button, Text, Icon, Form, Item, Input, Label, Picker, Toast, } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { SignupApi } from '../../../../PostApi';
import { Select, Option } from "react-native-chooser";
import { StateApi } from "../../../../PostApi"


/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
const duration = 3000;
const style = { top: '15%' };



class RegisterScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            code: '',
            phone: '',
            password: '',
            city: '',
            address: '',
            postcode: '',
            state: global.t('State'),
            stateObj: { f_name: 'State', fname_cn: '州属' },
            data: [],
            Error: '',
            isEditable: false,
            showToast: true,
            stateData: [],
            langPrint: '',


        }

    }

    componentDidMount() {
        StateApi().then((fetchData) => {
            console.log(fetchData, '-------state')
            this.setState({
                stateData: fetchData
            })
        })

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })
    }


    /****************************************** Select Option State ******************************************/
    onSelect(value, label) {
        console.log(value, 'value-----------')
        stateObj = this.state.stateData.find(element => {
            return element.f_id === value
        })
        this.setState({
            state: value,
            stateObj: stateObj
        });
    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        if (field == 'username') { this.setState({ username: text }) }
        if (field == 'email') { this.setState({ email: text }) }
        //if (field == 'code') { this.setState({ code: text }) }
        if (field == 'phone') { this.setState({ phone: text }) }
        if (field == 'password') { this.setState({ password: text }) }
        if (field == 'city') { this.setState({ city: text }) }
        if (field == 'address') { this.setState({ address: text }) }
        if (field == 'postcode') { this.setState({ postcode: text }) }

    }

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(email = this.state.email, username = this.state.username, phone = this.state.phone,
        password = this.state.password, city = this.state.city, address = this.state.address,
        postcode = this.state.postcode, state = this.state.state) {
        console.log(email, 'email---------')
        console.log(username, 'username---------')

        let outputJson = [
            {
                email: this.state.email,
                username: this.state.username,
                phone: this.state.phone,
                password: this.state.password,
                state: this.state.state
            }
        ]
        console.log(outputJson, 'outputJson---------')

        /****************************** Validations form Toast *****************************/
        if (this.state.username === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Username'),
                    buttonText: global.t('close'),
                    position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.phone === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Phone'),
                    buttonText: global.t('close'),
                    position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.email === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Email'),
                    buttonText: global.t('close'),
                    position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.password === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Password'),
                    buttonText: global.t('close'),
                    position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        // else if (this.state.address === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Address'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // else if (this.state.city === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_City'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // else if (this.state.postcode === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Postcode'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        else if (this.state.state === global.t('State')) {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_State'),
                    buttonText: global.t('close'),
                    position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else {
            this.setState({ Error: '' })
            /************************************ Fetch API  ***********************************/
            let url = `https://goldendestinations.com/api/new/sign_up.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&email=${email}&username=${username}&phone=${phone}&password=${password}&city=${city}&address=${address}&postcode=${postcode}&state=${state}`;

            fetch(url).then((res) => res.json())
                .then(function (myJson) {
                    console.log(myJson, 'myJson-----------');
                    if (myJson[0].status == 1) {
                        return myJson.json(Actions.success())
                    } else {
                        alert(myJson[0].error);
                    }
                });

        }

    }

    render() {
        //console.log(this.state.data, 'data---------------')

        const { Error, stateData, langPrint } = this.state;

        return (
            <ImageBackground
                source={require('../../../assets/images/background/redbg.png')}
                style={styles.Container}
            >
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ width: '100%', }}>
                        <Image
                            source={require('../../../assets/images/logotext.png')}
                            style={styles.imagecontainer}
                        />
                        <View style={{ flex: .8, }}>
                            {/* <Text style={styles.errormsg}>{Error}</Text> */}
                            <Form style={styles.formContainer}>
                                <View style={{ width: '80%', }}>
                                    {/*************************** Username  ***************************/}
                                    <Item style={{ borderColor: 'white', width: '100%', marginLeft: 0, marginBottom: 5, flexDirection: 'row' }}>
                                        <View style={{ width: '30%' }}>
                                            <Text style={{ color: '#fff' }}>{global.t('Name')}</Text>
                                        </View>
                                        <View style={{ width: '70%' }}>
                                            <TextInput
                                                placeholder={global.t('Fill_Name')}
                                                placeholderTextColor='#ffffff85'
                                                style={styles.inputstyle4}
                                                onChangeText={(text) => this.updataValue(text, 'username')}
                                                autoCorrect={false}
                                                autoCapitalize="none"
                                            />
                                        </View>
                                    </Item>
                                    {/*************************** Phone  ***************************/}
                                    <Item style={{ width: '100%', flexDirection: 'row', borderColor: 'white', marginLeft: 0, }}>
                                        <View style={{ width: '30%' }}>
                                            <Text style={{ color: '#fff' }}>{global.t('Phone')}</Text>
                                        </View>
                                        <View style={{ width: '70%', }}>
                                            <TextInput
                                                placeholder="0121234567"
                                                placeholderTextColor='#ffffff85'
                                                style={styles.InputStyle2}
                                                onChangeText={(text) => this.updataValue(text, 'phone')}
                                                autoCorrect={false}
                                                autoCapitalize="none"
                                            />
                                        </View>
                                    </Item>
                                    {/*************************** Email ***************************/}
                                    <Item style={{ width: '100%', borderColor: 'white', marginLeft: 0, flexDirection: 'row' }}>
                                        <View style={{ width: '30%' }}>
                                            <Text style={{ color: '#fff' }}>{global.t('Email')}</Text>
                                        </View>
                                        <View style={{ width: '70%', }}>
                                            <TextInput
                                                placeholder={global.t('Fill_Email')}
                                                placeholderTextColor='#ffffff85'
                                                style={styles.inputstyle4}
                                                onChangeText={(text) => this.updataValue(text, 'email')}
                                                autoCorrect={false}
                                                autoCapitalize="none"
                                            />
                                        </View>
                                    </Item>

                                    {/*************************** Password ***************************/}
                                    <Item style={{ width: '100%', borderColor: 'white', marginLeft: 0, flexDirection: 'row' }}>
                                        <View style={{ width: '30%' }}>
                                            <Text style={{ color: '#fff' }}>{global.t('Password')}</Text>
                                        </View>
                                        <View style={{ width: '70%', }}>
                                            <TextInput
                                                secureTextEntry={true}
                                                placeholder={global.t('Fill_Password')}
                                                placeholderTextColor='#ffffff85'
                                                style={styles.inputstyle4}
                                                onChangeText={(text) => this.updataValue(text, 'password')}
                                                autoCorrect={false}
                                                autoCapitalize="none"
                                            />
                                        </View>
                                    </Item>
                                    {/*************************** Code  ***************************/}
                                    {/* <Item style={{ borderBottomColor: 'white', width: '100%', marginLeft: 0, marginBottom: 5 }}>
                                        <Item style={{ width: '30%', borderBottomWidth: 0 }}>
                                            <TextInput
                                                placeholder="+6"
                                                placeholderTextColor='white'
                                                style={styles.InputStyle}
                                                onChangeText={(text) => this.updataValue(text, 'code')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                editable={this.state.isEditable}
                                            />
                                        </Item>

                                    </Item> */}

                                    {/*************************** Address ***************************/}
                                    {/* <Item style={{ borderColor: 'white', height: 50, width: '100%', marginLeft: 0 }}>
                                        <TextInput
                                            placeholder={global.t('Address')}
                                            placeholderTextColor='white'
                                            style={styles.inputstyle4}
                                            onChangeText={(text) => this.updataValue(text, 'address')}
                                            autoCorrect={false}
                                            autoCapitalize={false}
                                        />
                                    </Item> */}
                                    {/*************************** City ***************************/}
                                    <Item style={{ borderBottomColor: 'white', width: '100%', marginLeft: 0, }}>
                                        {/* <Item style={{ width: '30%', borderBottomWidth: 0 }}>
                                            <TextInput
                                                placeholder={global.t('City')}
                                                placeholderTextColor='white'
                                                style={styles.InputStyle}
                                                onChangeText={(text) => this.updataValue(text, 'city')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                            //multiline={true}
                                            />
                                        </Item> */}
                                        {/*************************** Postcode  ***************************/}
                                        {/* <Item style={{ width: '70%', borderBottomWidth: 0 }}>
                                            <TextInput
                                                placeholder={global.t('Postcode')}
                                                placeholderTextColor='white'
                                                style={styles.InputStyle3}
                                                onChangeText={(text) => this.updataValue(text, 'postcode')}
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                            />
                                        </Item> */}
                                    </Item>
                                    {/*************************** State ***************************/}
                                    <Item style={{ width: '100%', marginLeft: 0 }}>
                                        <View style={{ borderBottomWidth: 0.5, borderBottomColor: "#fff", width: '99%' }}>
                                            <Select
                                                onSelect={this.onSelect.bind(this)}
                                                defaultText={langPrint === 'zh' ? this.state.stateObj.fname_cn : this.state.stateObj.f_name}
                                                style={{ borderWidth: 0, width: '100%', height: 70, paddingLeft: 0, }}
                                                textStyle={{ color: '#fff', textAlign: 'left', width: '100%', paddingLeft: 0, paddingTop: 16 }}
                                                backdropStyle={{ backgroundColor: "#00000078" }}
                                                optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                                                animationType='none'
                                                transparent={true}
                                                indicator='down'
                                                indicatorColor='white'
                                                indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                                                indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 25 : 20, }}
                                            >

                                                {
                                                    stateData.map((item) => {
                                                        return (
                                                            <Option value={item.f_id}>
                                                                {langPrint === 'zh' ? item.fname_cn : item.f_name}
                                                            </Option>
                                                        )
                                                    })

                                                }


                                            </Select>
                                        </View>
                                    </Item>
                                </View>
                            </Form>
                        </View>
                    </ScrollView>

                    {/*************************** Submit button ***************************/}
                    <View style={styles.Buttonwarp}>
                        <Button block style={styles.bottomStyle} onPress={() => this.onSubmit()}>
                            <Text style={styles.BottomText} uppercase={false}>{global.t('Create_Account')}</Text>
                        </Button>
                    </View>
                    {/* <SafeAreaView style={{ flex: 0, backgroundColor: '#fc564e' }} /> */}
                </View>

            </ImageBackground >
        );
    }
}


export default RegisterScreen;

const styles = StyleSheet.create({
    Container: { width: '100%', height: '100%' },
    imagecontainer: {
        width: '100%', height: 80, resizeMode: 'contain', marginLeft: 'auto', marginRight: 'auto',
        ...Platform.select({
            ios: { marginTop: 50, },
            android: { marginTop: 40, }
        })
    },
    formContainer: { width: '100%', marginTop: 30, marginBottom: 100, alignItems: 'center', },
    headerText: { fontSize: 24, textAlign: "center", margin: 10, color: "white", fontWeight: "bold", marginTop: 50 },
    Buttonwarp: {
        position: 'absolute', flex: 0.1, left: 0, right: 0, alignItems: 'center', width: '100%',
        ...Platform.select({
            ios: { bottom: -30, },
            android: { bottom: -20, }
        }), flexDirection: 'row',
        ...Platform.select({
            ios: { height: 100, },
            android: { height: 80, }
        }),
    },
    bottomStyle: {
        width: '100%', backgroundColor: '#fc564e',
        ...Platform.select({
            ios: { height: 70 },
            android: { height: 60 }
        })
    },
    BottomText: { fontSize: 16 },
    InputStyle: { borderRightColor: 'white', borderRightWidth: 1, height: 70, width: '100%', padding: 10, textAlign: 'left', color: 'white', },
    InputStyle2: { height: 70, textAlign: 'left', color: 'white', padding: 10, width: '100%', },
    InputStyle3: { height: 70, color: 'white', textAlign: 'left', width: '100%', padding: 10 },
    inputstyle4: { textAlign: 'left', color: 'white', width: '100%', height: 70, padding: 10 },
    pickerStyle: { position: 'absolute', bottom: 0, left: 0, right: 0 },
    errormsg: { textAlign: 'center', width: '100%', position: 'absolute', color: 'yellow', marginTop: 10 },



})
