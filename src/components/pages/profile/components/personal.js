import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TextInput, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Text, Footer, FooterTab, Button, } from 'native-base';

import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";



class Personal extends Component {

    constructor() {
        super();
        this.state = {
            phone: '0123456789',
            location: 'Puchong',
            TextInputDisableStatus: true,
            AdsInputDisableStatus: true

        }
    }
    
    

    onPressButton = () => {
        this.setState({
            TextInputDisableStatus: false,
            phone: this.refs.thisNumInput.value
        })
    }

    onPressAdsButton = () => {
        this.setState({
            AdsInputDisableStatus: false,
            location: this.refs.thisAdsInput.value
        })
    }


    render(item) {
        return (

            <View style={styles.BottomContainer} >
                <Form>
                    <View style={styles.TopTextContainer}>
                        <View style={{ width: '50%', paddingLeft: 10 }}>
                            <Text style={styles.TopTextTitle}>{global.t('Message')}</Text>
                        </View>
                    </View>
                    <Item stackedLabel style={{ marginBottom: 20, borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>Phone</Label>
                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <TextInput
                                value={this.state.phone}
                                underlineColorAndroid='transparent'
                                style={[styles.TextInputStyle,
                                { backgroundColor: this.state.TextInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                editable={this.state.TextInputDisableHolder}
                                ref="thisNumInput"
                            />
                            <IconANT
                                name="form"
                                onPress={this.onPressButton}
                                size={20}
                                style={{ color: this.state.TextInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                            />
                        </View>


                    </Item>
                    <Item stackedLabel style={{ borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>Address</Label>
                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <TextInput
                                value={this.state.location}
                                underlineColorAndroid='transparent'
                                style={[styles.TextInputStyle,
                                { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                editable={this.state.TextInputDisableHolder}
                                ref="thisAdsInput"
                            />
                            <IconANT
                                name="form"
                                onPress={this.onPressAdsButton}
                                size={20}
                                style={{ color: this.state.AdsInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                            />
                        </View>
                    </Item>
                    <View style={styles.AddressContainer}>
                        <Item stackedLabel style={styles.AddressItem}>
                            <Label style={styles.LabelStyle}>City</Label>
                            <TextInput
                                value={this.state.location}
                                underlineColorAndroid='transparent'
                                style={[styles.TextInputStyle,
                                { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                editable={this.state.TextInputDisableHolder}
                                ref="thisAdsInput"
                            />
                        </Item>
                        <Item stackedLabel style={styles.AddressItem}>
                            <Label style={styles.LabelStyle}>Postcode</Label>
                            <TextInput
                                value={this.state.location}
                                underlineColorAndroid='transparent'
                                style={[styles.TextInputStyle,
                                { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                editable={this.state.TextInputDisableHolder}
                                ref="thisAdsInput"
                            />
                        </Item>
                    </View>
                    <Item stackedLabel style={{ marginTop: 20, borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>State</Label>
                        <TextInput
                            value={this.state.location}
                            underlineColorAndroid='transparent'
                            style={[styles.TextInputStyle,
                            { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                            editable={this.state.TextInputDisableHolder}
                            ref="thisAdsInput"
                        />
                    </Item>
                    <TouchableOpacity>
                        <View style={styles.subbtnWarp}>
                            <Text style={{ textAlign: 'center', color: '#fff' }}>Submit</Text>
                        </View>
                    </TouchableOpacity>
                </Form>
            </View>


        );
    }
}


export default Personal;

const styles = StyleSheet.create({
    GenaralContainer: { backgroundColor: 'white', paddingTop: 20, paddingBottom: 20, paddingLeft: 30, paddingRight: 50, },
    TopTextContainer: { flex: 1, flexDirection: 'row', width: '100%', },
    TopTextTitle: { fontSize: 20, color: '#de2d30', fontWeight: 'bold', marginBottom: 10 },
    TopIconContainer: { width: '50%', alignItems: 'flex-end', },
    TopIconStyle: { color: '#de2d30', },
    LabelStyle: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', },
    InputStyle: { color: 'black' },
    BottomContainer: { backgroundColor: '#e9ecef', paddingTop: 20, paddingBottom: 100, paddingLeft: 30, paddingRight: 50 },
    AddressContainer: { flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'space-between', },
    AddressItem: { width: '40%', marginTop: 20, borderColor: '#959da5' },
    TextInputStyle: { textAlign: 'left', height: 40, marginBottom: 0, marginTop: 10, width: '80%', marginRight: 'auto' },
    subbtnWarp: {
        width: '70%', backgroundColor: '#de2d30', padding: 15, marginTop: 50, marginRight: 'auto',
        marginLeft: 'auto', borderRadius: 50
    }


})
