import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TextInput, TouchableOpacity, Platform, SafeAreaView } from 'react-native';
import { Form, Item, Input, Label, Text, Button, Toast, ListItem, Icon, Left, Body, Right, } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import RNLanguages from 'react-native-languages';
import i18n from '../../../../lang/index';
import en from '../../../../lang/locales/en.json';
import zh from '../../../../lang/locales/zh.json';
import RNRestart from "react-native-restart"
import { UserProfileApi, StateApi } from '../../../../../PostApi';
import { Select, Option } from "react-native-chooser";


/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
const buttonText = "Okay";
const duration = 3000;
const style = { top: '15%' };



class Genaral extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // access_token: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            city: '',
            name: '',
            postcode: '',
            state: '',
            TextInputDisableStatus: true,
            PassInputDisableStatus: true,
            TextPhoneDisableStatus: true,
            AdsInputDisableStatus: true,
            CityInputDisableStatus: true,
            PostInputDisableStatus: true,
            StateInputDisableStatus: true,
            Error: '',
            tokenId: null,
            data: [],
            isEditable: false,
            langPrint: '',
            stateData: [],
            state: global.t('State'),
            stateObj: { f_name: 'State', fname_cn: '州属' },
            catcherr: null,
            error: null,

        }

    }
    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
            console.log(asyncStorageRes, '----------------AsyncStorage')
            this.setState({
                tokenId: asyncStorageRes
            })
            //console.log(this.state.tokenId, '-------------fetchDataApi1111')

            /************************************ Fetch Api Data  ***********************************/
            UserProfileApi(this.state.tokenId).then((fetchData) => {
                console.log(fetchData, '-----------------fetchData')
                this.setState({
                    data: fetchData[0],
                    name: fetchData[0].name,
                    phone: fetchData[0].phone,
                    state: fetchData[0].state
                })
            }).catch((error) => {
                console.log(error, 'response----------error');
                this.setState({
                    catcherr: error.message
                });
            });


            // this.onSubmit(this.state.tokenId)

        });



        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })

        StateApi().then((fetchData) => {
            console.log(fetchData, '-------state')
            this.setState({
                stateData: fetchData
            })
        })


    }

    /****************************************** Select Option State ******************************************/
    onSelect(value, label) {
        console.log(value, 'value-----------')
        stateObj = this.state.stateData.find(element => {
            return element.f_id === value
        })
        this.setState({ state: value, stateObj: stateObj });
    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        if (field == 'email') { this.setState({ TextInputDisableStatus: false, email: this.refs.thisTextInput.value, email: text }) }
        if (field == 'name') { this.setState({ TextInputDisableStatus: false, name: this.refs.thisTextInput.value, name: text }) }
        if (field == 'access_token') { this.setState({ TextInputDisableStatus: false, access_token: this.refs.thisTextInput.value, access_token: text }) }
        if (field == 'password') { this.setState({ PassInputDisableStatus: false, password: this.refs.thisTextInput.value, password: text }) }
        if (field == 'phone') { this.setState({ TextPhoneDisableStatus: false, phone: this.refs.thisNumInput.value, phone: text }) }
        //if (field == 'address') { this.setState({ AdsInputDisableStatus: false, address: this.refs.thisAdsInput.value, address: text }) }
        // if (field == 'city') { this.setState({ CityInputDisableStatus: false, city: this.refs.thiscityInput.value, city: text }) }
        // if (field == 'postcode') { this.setState({ PostInputDisableStatus: false, postcode: this.refs.thispostInput.value, postcode: text }) }
        if (field == 'state') { this.setState({ StateInputDisableStatus: false, state: this.refs.thisstateInput.value, state: text }) }
    }

    /****************************************** Change Language ******************************************/
    change = async (language) => {
        console.info("== changing language to:" + language)
        await AsyncStorage.setItem("language", language)
        RNRestart.Restart()
    }
    // state = {
    //     currentLanguage: RNLanguages.language
    // };

    // _changeLanguage = (language) => {
    //     this.setState({ currentLanguage: language });
    // };

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(tokenId = this.state.tokenId, password = this.state.password, phone = this.state.phone, name = this.state.name,
        address = this.state.address, city = this.state.city, postcode = this.state.postcode, state = this.state.state) {
        console.log(tokenId, 'onSubmit------------------fetch------------tokenId')

        /****************************** Validations form Toast *****************************/
        // if (this.state.access_token === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: 'Please Fill In The Access Token',
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // if (this.state.email === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Email'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // if (this.state.password === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Password'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // if (this.state.name === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Name'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // else if (this.state.phone === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Phone'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // else if (this.state.address === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Address'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // else if (this.state.city === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_City'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // else if (this.state.postcode === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_Postcode'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }

        // else if (this.state.state === "") {
        //     this.setState({
        //         Error: Toast.show({
        //             text: global.t('Fill_State'),
        //             buttonText: buttonText, position: position,
        //             duration: duration, textStyle: textStyle, style: style,
        //         })
        //     })
        // }
        // else {
            // this.setState({ Error: '' })
        // }
        
        /************************************ Fetch API  ***********************************/
        let url = `https://goldendestinations.com/api/new/update_profile.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&access_token=${tokenId}&name=${name}&phone=${phone}&password=${password}&city=${city}&address=${address}&postcode=${postcode}&state=${state}`;
        console.log(url, '-----------------url-----onSubmit')
        fetch(url).then((res) => res.json())
            .then(function (myJson) {
                //console.log(myJson, '-----------myJson');
                if (myJson[0].status === 1) {
                    return (
                        alert(global.t("ok_change_Profile")),
                        myJson.json(Actions.Home())
                    )  
                }
                else {
                    alert(myJson[0].error)
                }
            })
    }

    /************************************ Logout ***********************************/
    _logout() {
        AsyncStorage.removeItem('USER_TOKEN');
        Actions.Home();
        // RNRestart.Restart()
    }


    render() {
        const { Error, data, isEditable, email, phone, address, city, state, postcode, name, langPrint, stateData, stateObj, catcherr } = this.state;
        // console.log(this.state.catcherr, '----------------catcherr')
        return (
            <View style={{ width: '100%', height: '100%', backgroundColor: '#fff',}}>
                <View style={{ flex: 1, }}>
                    <ScrollView style={{ width: '100%', }}>
                        <View style={{ flex: .8, }}>
                            <Form style={{marginBottom: 50}}>
                                {/********************************** Genaral **********************************/}
                                <View style={styles.GenaralWarp}>
                                    {/* <Text style={styles.errormsg}>{Error}</Text> */}
                                    <View style={styles.TopTextContainer}>
                                        <View style={{ width: '100%', paddingLeft: 15, flexDirection: 'row' }}>
                                            <Text style={[styles.TopTextTitle, { width: '60%' }]}>
                                                {global.t('Profile')}
                                            </Text>
                                            <Text style={{ width: '40%', textAlign: 'right' , color: '#0095ff'}}>
                                                Hi, {data.name}
                                            </Text>
                                        </View>
                                    </View>
                                    {/******************* Email  ********************/}
                                    <Item stackedLabel style={{ marginBottom: 10, borderColor: '#ccc' }}>
                                        <Label style={styles.LabelStyle}>{global.t('Email')}</Label>
                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={email}
                                                placeholder={data.email}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.TextInputDisableStatus ? '#FFF' : '#FFF' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisTextInput"
                                                autoCorrect={false}
                                                autoCapitalize="none"
                                                onChangeText={(text) => this.updataValue(text, 'email')}
                                                editable={isEditable}
                                            />
                                            {/* <IconANT
                                        name="form"
                                        onPress={this.onPressButton}
                                        size={20}
                                        style={{ color: this.state.TextInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                    /> */}
                                        </View>
                                    </Item>
                                    {/***************** Password  ********************/}
                                    <Item stackedLabel style={{ borderColor: '#ccc',}}>
                                        <ListItem icon style={styles.PasswordListItam} onPress={() => Actions.changepassword()}>
                                            <Body style={{ borderBottomWidth: 0 }}>
                                                <Text style={styles.passStyle}>{global.t('Change_Password')}</Text>
                                            </Body>
                                            <Right style={{ borderBottomWidth: 0 }}>
                                                <Icon active name="arrow-forward" />
                                            </Right>
                                        </ListItem>


                                        {/* <Label style={styles.LabelStyle}>{global.t('Password')}</Label>
                                <View style={{ flexDirection: 'row', width: '100%' }}>
                                    <TextInput
                                        value={this.state.password}
                                        underlineColorAndroid='transparent'
                                        style={[styles.TextInputStyle,
                                        { backgroundColor: this.state.PassInputDisableStatus ? '#FFF' : '#FFF' }]}
                                        editable={this.state.TextInputDisableHolder}
                                        ref="thisTextInput"
                                        secureTextEntry
                                        autoCorrect={false}
                                        autoCapitalize={false}
                                        onChangeText={(text) => this.updataValue(text, 'password')}
                                    />
                                    <IconANT
                                        name="form"
                                        onPress={this.onPressPassButton}
                                        size={20}
                                        style={{ color: this.state.PassInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                    />
                                </View> */}
                                    </Item>
                                    {/******************* Change language  ******************
                            <Item stackedLabel style={styles.langwarpContainer}>
                                <View style={{ width: '65%' }}>
                                    <Label style={styles.LabelStylelang}>{global.t('Change_language')}</Label>
                                </View>
                                <View style={{ width: '35%', }}>
                                    <View style={styles.langwarp}>
                                        <TouchableOpacity onPress={() => this.change('zh')} style={{ marginRight: 15, textAlign: 'right', }} >
                                            <Image source={require('../../../../assets/images/icon/cn.png')} style={styles.langicon} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.change('en')} style={{ textAlign: 'right', }}>
                                            <Image source={require('../../../../assets/images/icon/en.png')} style={styles.langicon} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </Item>
                            */}
                                </View>

                                {/************************************* Personal *************************************/}
                                <View style={styles.BottomContainer} >
                                    {/* <View style={styles.TopTextContainer}>
                                        <View style={{ width: '50%', paddingLeft: 15 }}>
                                            <Text style={styles.TopTextTitle}>{global.t('Personal')}</Text>
                                        </View>
                                    </View> */}
                                    {/******************* name  *******************/}
                                    <Item stackedLabel style={{ marginBottom: 10, borderColor: '#ccc' }}>
                                        <Label style={styles.LabelStyle}>{global.t('Name')}</Label>
                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={name}
                                                placeholder={data.name}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.TextInputDisableStatus ? '#fff' : '#fff' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisTextInput"
                                                autoCorrect={false}
                                                autoCapitalize="none"
                                                onChangeText={(text) => this.updataValue(text, 'name')}
                                            />
                                            <IconANT
                                                name="form"
                                                onPress={this.onPressPhoneButton}
                                                size={20}
                                                style={{ color: this.state.TextInputDisableStatus ? '#fff' : '#fff' }}
                                            />
                                        </View>
                                    </Item>
                                    {/******************* Phone  *******************/}
                                    <Item stackedLabel style={{ marginBottom: 10, borderColor: '#ccc' }}>
                                        <Label style={styles.LabelStyle}>{global.t('Phone')}</Label>
                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={phone}
                                                placeholder={data.phone}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.TextPhoneDisableStatus ? '#fff' : '#fff' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisNumInput"
                                                autoCorrect={false}
                                                autoCapitalize="none"
                                                onChangeText={(text) => this.updataValue(text, 'phone')}
                                            />
                                            <IconANT
                                                name="form"
                                                onPress={this.onPressPhoneButton}
                                                size={20}
                                                style={{ color: this.state.TextPhoneDisableStatus ? '#fff' : '#fff' }}
                                            />
                                        </View>
                                    </Item>
                                    {/******************* Address  *******************/}
                                    {/* <Item stackedLabel style={{ borderColor: '#959da5' }}>
                                        <Label style={styles.LabelStyle}>{global.t('Address')}</Label>
                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={address}
                                                placeholder={data.address}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisAdsInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'address')}
                                            />
                                            <IconANT
                                                name="form"
                                                onPress={this.onPressAdsButton}
                                                size={20}
                                                style={{ color: this.state.AdsInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                            />
                                        </View>
                                    </Item> */}
                                    <View style={styles.AddressContainer}>
                                        {/******************* City  *******************/}
                                        {/* <Item stackedLabel style={styles.AddressItem}>
                                            <Label style={styles.LabelStyle}>{global.t('City')}</Label>
                                            <TextInput
                                                value={city}
                                                placeholder={data.city}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thiscityInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'city')}
                                            />
                                        </Item> */}
                                        {/******************* Postcode  *******************/}
                                        {/* <Item stackedLabel style={styles.AddressItem}>
                                            <Label style={styles.LabelStyle}>{global.t('Postcode')}</Label>
                                            <TextInput
                                                value={postcode}
                                                placeholder={data.postcode}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thispostInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'postcode')}
                                            />
                                        </Item> */}
                                    </View>
                                    {/******************* State  *******************/}
                                    <Item stackedLabel style={{ borderColor: '#ccc' }}>
                                        <Label style={styles.LabelStyle}>{global.t('State')}</Label>

                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <View style={{ width: '55%' }}>
                                                <TextInput
                                                    //value={state}
                                                    placeholder={langPrint === 'zh' ? data.state_cn : data.state}
                                                    placeholderTextColor="#000"
                                                    underlineColorAndroid='transparent'
                                                    style={[styles.TextInputStyle,
                                                    { backgroundColor: this.state.AdsInputDisableStatus ? '#fff' : '#fff' }]}
                                                    editable={this.state.TextInputDisableHolder}
                                                    ref="thisAdsInput"
                                                    autoCorrect={false}
                                                    autoCapitalize="none"
                                                    onChangeText={(text) => this.updataValue(text, 'state')}
                                                    editable={isEditable}
                                                />
                                            </View>
                                            <View style={{ width: '45%' }}>
                                                <Select
                                                    onSelect={this.onSelect.bind(this)}
                                                    defaultText={langPrint === 'zh' ? stateObj.fname_cn : stateObj.f_name}
                                                    style={{ borderWidth: 0, width: '95%', height: 50, paddingLeft: 0, }}
                                                    textStyle={{ color: '#000', textAlign: 'left', width: '100%', paddingLeft: 0, paddingTop: 10 }}
                                                    backdropStyle={{ backgroundColor: "#00000078" }}
                                                    optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                                                    animationType='none'
                                                    transparent={true}
                                                    indicator='down'
                                                    indicatorColor='#000'
                                                    indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                                                    indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 15 : 10, }}
                                                >
                                                    {
                                                        stateData.map((item) => {
                                                            return (
                                                                <Option value={item.f_id}>
                                                                    {langPrint === 'zh' ? item.fname_cn : item.f_name}
                                                                </Option>
                                                            )
                                                        })
                                                    }
                                                </Select>
                                            </View>
                                        </View>

                                        {/* 

                                        <View style={{ flexDirection: 'row', width: '100%' }}>
                                            <TextInput
                                                value={state}
                                                placeholder={data.state}
                                                placeholderTextColor="#000"
                                                underlineColorAndroid='transparent'
                                                style={[styles.TextInputStyle,
                                                { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                                editable={this.state.TextInputDisableHolder}
                                                ref="thisAdsInput"
                                                autoCorrect={false}
                                                autoCapitalize={false}
                                                onChangeText={(text) => this.updataValue(text, 'state')}
                                            />
                                            <IconANT
                                                name="form"
                                                onPress={this.onPressAdsButton}
                                                size={20}
                                                style={{ color: this.state.AdsInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                            />
                                        </View> */}
                                    </Item>

                                    {/* <Text>{catcherr ? catcherr : 'no error'}</Text>  */}


                                    {/* <Item stackedLabel style={{ marginTop: 10, borderColor: '#959da5' }}>
                                        <Label style={styles.LabelStyle}>{global.t('State')}</Label>
                                        <TextInput
                                            value={state}
                                            placeholder={data.state}
                                            placeholderTextColor="#000"
                                            underlineColorAndroid='transparent'
                                            style={[styles.TextInputStyle,
                                            { backgroundColor: this.state.AdsInputDisableStatus ? '#e9ecef' : '#e9ecef' }]}
                                            editable={this.state.TextInputDisableHolder}
                                            ref="thisstateInput"
                                            autoCorrect={false}
                                            autoCapitalize={false}
                                            onChangeText={(text) => this.updataValue(text, 'state')}
                                        />
                                        <IconANT
                                            name="form"
                                            onPress={this.onPressAdsButton}
                                            size={20}
                                            style={{ color: this.state.AdsInputDisableStatus ? '#d1d5da' : '#de2d30' }}
                                        />
                                    </Item> */}
                                </View>
                            </Form>
                            {/* <View style={{ width: '100%', alignItems: 'flex-end', justifyContent: 'flex-end', marginBottom: 100 }}>
                                <View style={{ right: 20 }}>
                                    <Button iconLeft info onPress={() => this._logout()}>
                                        <IconANT name='logout' style={{ color: '#fff', fontSize: 20, paddingLeft: 10 }} />
                                        <Text>{global.t('Logout')}</Text>
                                    </Button>
                                </View>

                            </View> */}

                        </View>
                    </ScrollView>
                    {/*************************** Submit button ***************************/}
                    <View style={styles.Buttonwarp}>
                        <Button block style={styles.bottomStyle} onPress={() => this.onSubmit()}>
                            <Text style={styles.BottomText} uppercase={false}>{global.t('Submit')}</Text>
                        </Button>
                    </View>
                    {/* <SafeAreaView style={{ flex: 0, backgroundColor: '#fc564e' }} />  */}
                </View>
            </View>

        );
    }
}


export default Genaral;


const styles = StyleSheet.create({
    TopTextContainer: { flexDirection: 'row', width: '100%', },
    TopTextTitle: { fontSize: 20, color: '#de2d30', fontWeight: 'bold', marginBottom: 10 },
    TopIconContainer: { width: '50%', alignItems: 'flex-end', },
    TopIconStyle: { color: '#de2d30', },
    LabelStyle: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', textAlign: 'left', },
    passStyle: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', textAlign: 'left', fontSize: 15 },
    InputStyle: { color: 'black', },
    BottomContainer: { paddingTop: 20, paddingLeft: 20, paddingRight: 30, marginBottom: 60 },
    AddressContainer: { flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'space-between', },
    AddressItem: { width: '40%', marginTop: 10, borderColor: '#959da5' },
    TextInputStyle: { textAlign: 'left', height: 40, marginBottom: 0, marginTop: 10, width: '80%', marginRight: 'auto' },
    langwarpContainer: { marginTop: 10, flexDirection: 'row', borderBottomWidth: 0 },
    langwarp: { width: '100%', marginLeft: 'auto', flexDirection: 'row', justifyContent: 'flex-end', },
    langicon: { width: 50, height: 30, },
    subbtnWarp: {
        width: '70%', backgroundColor: '#de2d30', padding: 15, marginTop: 50, marginRight: 'auto',
        marginLeft: 'auto', borderRadius: 50
    },
    LabelStylelang: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', textAlign: 'left', marginTop: -20 },
    Buttonwarp: {
        position: 'absolute', bottom: 0, flex: 0.1, left: 0, right: 0,
        alignItems: 'center', width: '100%',
    },
    bottomStyle: {
        width: '100%',
        backgroundColor: '#fc564e', borderRadius: 0,
        ...Platform.select({
            ios: { height: 70 },
            android: { height: 60 }
        })
    },
    BottomText: { fontSize: 16, color: '#fff' },
    errormsg: { textAlign: 'right', width: '100%', position: 'absolute', right: 0, color: 'blue', padding: 10 },
    PasswordListItam: { width: '100%', paddingTop: 10, marginLeft: 0, borderBottomWidth: 0 },
    GenaralWarp: {  paddingLeft: 20, paddingRight: 30, paddingTop: 20, }

})
