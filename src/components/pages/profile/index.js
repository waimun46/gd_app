import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Content, Form, Item, Input, Label, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";

import Genaral from './components/genaral';
import Personal from './components/personal';

import LoginModal from '../login';


class ProfilePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tokenId: 0
        }
    }

    componentDidMount() {
        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
            //console.log(JSON.parse(asyncStorageRes), 'USER_TOKEN----------------tokenId')
            //console.log(asyncStorageRes, 'USER_TOKEN----------------tokenId')
            this.setState({
                tokenId: asyncStorageRes
            })

        });
        // this.UserProfile();
    }

    render() {

        return (

            <View style={{ backgroundColor: '#e9ecef' }}>

                {
                    this.state.tokenId === null ? (
                        Actions.Home()
                        
                    ) : (
                            <Genaral />
                        )
                }

            </View>
        );
    }
}


export default ProfilePage;

const styles = StyleSheet.create({

})
