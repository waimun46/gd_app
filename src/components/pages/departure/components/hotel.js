import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Text, Platform, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { DepartingDetailApi } from '../../../../../PostApi';



class HotelPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            AsyncStorageId: 0,
            isLoding: true,
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            console.log(asyncStorageRes, '----------------HotelPage')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------HotelPage')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                console.log(fetchData, '-----------------HotelPage')
                this.setState({
                    data: fetchData[0].hotel_detail,
                    isLoding: false
                })
            })
            // this.fetchDataApi(this.state.AsyncStorageId);
        });


    }

    keyExtractor = (item, index) => item.key;

    renderItem = ({ item, index }) => {
        return (
            <View style={styles.container}>
                <View style={styles.containerwarp} >
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <View style={styles.leftcontent}>
                            <View style={{ padding: 5, paddingBottom: 15, paddingTop: 15, }}>
                                <Text style={[styles.titledate, { color: '#000' }]}>{global.t('IN')}</Text>
                                <Text style={styles.leftnumber}>{item.checkin}</Text>

                                {/* <Text style={styles.leftdate}>{item.monthstart}</Text> */}
                            </View>
                            <View style={{ padding: 5, paddingBottom: 15, paddingTop: 15, backgroundColor: '#eee' }}>
                                <Text style={[styles.titledate, { color: 'red' }]}>{global.t('OUT')}</Text>
                                <Text style={styles.leftnumberactive}>{item.checkout}</Text>

                                {/* <Text style={styles.leftdate}>{item.monthend}</Text> */}
                            </View>
                        </View>
                        <View style={styles.rightcontentwarp}>
                            <Text style={styles.rightcontenttitle}>{item.hotelname}</Text>
                            <View style={{ flex: 1, flexDirection: 'row', }}>
                                <Text style={styles.righttitletext}>{global.t('Address')}: </Text>
                                <Text style={styles.righttext}>1{item.hoteladdress} </Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', }}>
                                <Text style={styles.righttitletext}>{global.t('Phone_no')}: </Text>
                                <Text style={styles.righttext}>{item.telnum}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    ListEmpty = () => {
        return (
            <View style={styles.MainContainer}>
                <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
                <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
            </View>
        );
    };

    render() {
        const { data, isLoding } = this.state;
        return (
            <View style={{ backgroundColor: 'white', }}>
                {
                    isLoding ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                            <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                            <ActivityIndicator size="large" color="#de2d30" />
                        </View>
                    ) : (
                            <ScrollView>
                                <FlatList
                                    data={data}
                                    renderItem={this.renderItem}
                                    keyExtractor={this.keyExtractor}
                                    style={{ paddingBottom: 50 }}
                                    ListEmptyComponent={this.ListEmpty}
                                />
                            </ScrollView>
                        )
                }

            </View>
        );
    }
}


export default HotelPage;

const styles = StyleSheet.create({
    container: { flex: 1, flexDirection: 'column', justifyContent: 'space-between', },
    containerwarp: { width: '100%', height: 'auto', },
    leftcontent: { width: '30%', borderRightWidth: 1, borderRightColor: '#bbc0c4', },
    leftnumberactive: { textAlign: 'center', fontSize: 14, fontWeight: 'bold', color: '#a9a9a9' },
    leftnumber: { textAlign: 'center', fontSize: 14, fontWeight: 'bold', color: '#a9a9a9' },
    leftdate: { textAlign: 'center', fontSize: 14, fontWeight: 'bold', color: '#a9a9a9', textTransform: 'uppercase' },
    rightcontentwarp: { width: '70%', padding: 10, borderBottomWidth: 1, borderBottomColor: '#bbc0c4', },
    rightcontenttitle: { fontSize: 18, fontWeight: 'bold', color: 'black', marginBottom: 10 },
    righttitletext: { width: '35%', fontWeight: 'bold', color: 'black', fontSize: 14, },
    righttext: { width: '65%', fontSize: 14, },
    titledate: { fontSize: 14, fontWeight: 'bold', textAlign: 'center', marginBottom: 10 },
    MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },

})



