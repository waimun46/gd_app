import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Text, Platform, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconION from "react-native-vector-icons/Ionicons";
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import { DepartingDetailApi } from '../../../../../PostApi';
import HTML from 'react-native-render-html';


class Note extends Component {

    constructor(props) {
        super(props) 
        this.state = {
            data: [],
            AsyncStorageId: 0,
            langPrint: ''
        }
    }
 
    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------AsyncStorage-----Note')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------fetchDataApi----Note')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                //console.log(fetchData, '-----------------fetchData-----Note')
                this.setState({
                    data: fetchData[0]
                })
            })

        });

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })

    }


    /************************************ Logout ***********************************/
    _logout() {
        AsyncStorage.removeItem('USER_DATA');
        Actions.Departurelogin();
    }


    render() {
        const { data, langPrint } = this.state;
        return (
            <ScrollView style={{ backgroundColor: 'white', padding: 25 }}>
                 <View style={{  paddingBottom: 50 }}>

                    <View style={{ width: '100%', height:200}} >
                        <View style={styles.TopWarp}>
                            <View style={styles.ImageContainer}>
                                <Image source={{ uri: data.tourmanager_photo }}
                                    style={styles.ImageStyle}
                                />
                            </View>
                            <View style={styles.TextContainer}>
                                <Text style={styles.Text1} numberOfLines={1}>{global.t('Tour_Manager')}</Text>
                                <Text style={styles.Text2} numberOfLines={1}>{data.tourmanager_name}</Text>
                                <Text style={styles.Text3}>{data.tourmanager_phone_num}</Text>
                                <Text style={styles.Text9} numberOfLines={1}>{global.t('Tour_Code')}</Text>
                                <Text style={styles.Text11}>{data.tour_code}</Text>
                                <TouchableOpacity style={styles.logoutbtnstyle} onPress={() => this._logout()}>
                                    <Text style={styles.logouttext}>{global.t('Logout')}</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>

                    <View style={{ width: '100%', height: 'auto', }} >
                        <View style={styles.CenterWarp}>
                            <Text style={styles.Text1} numberOfLines={1}>{global.t('Package_Title')}: </Text>

                            {
                                langPrint === 'zh' ? (
                                    <Text style={styles.Text8b}>{data.title_cn} </Text>
                                ) : (
                                        <Text style={styles.Text8b}>{data.title_en} </Text>
                                    )
                            }

                            <Text style={styles.Text6}>{global.t('Meeting_Point')}: </Text>

                            {
                                langPrint === 'zh' ? (
                                    <HTML
                                        html={data.note_cn}
                                        tagsStyles={{
                                            p: { marginBottom: 10 },
                                        }}
                                    />
                                ) : (
                                        <HTML
                                            html={data.note_en}
                                            tagsStyles={{
                                                p: { marginBottom: 10 },
                                            }}
                                        />
                                    )
                            }

                        </View>
                    </View>

                </View>


            </ScrollView>
        );
    }
}


export default Note;

const styles = StyleSheet.create({
    WarpContainer: { flex: 1, flexDirection: 'column', justifyContent: 'space-between', },
    TopWarp: { flex: 1, flexDirection: 'row', borderBottomColor: '#ccc', borderBottomWidth: 1, },
    ImageContainer: {
        width: '30%', shadowColor: "#000",
        ...Platform.select({
            ios: {
                shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 6,
            },
            android: {
                shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 6,
            }
        }),

    },
    ImageStyle: { resizeMode: 'cover', width: 100, height: 150, borderRadius: 8, },
    TextContainer: { width: '70%', paddingTop: 10, paddingLeft: 25, },
    Text1: { color: '#de2d30', fontSize: 18, fontWeight: 'bold' },
    Text1b: { color: '#de2d30', fontSize: 18, fontWeight: 'bold', marginBottom: 15 },
    Text2: { color: 'black', fontSize: 16, marginTop: 5, fontWeight: 'bold', },
    Text3: { color: '#9e9ea6', marginTop: 5 },
    Text4: { color: '#9e9ea6', marginTop: 5 },
    Text5: { color: '#9e9ea6', marginTop: 20 },
    Text6: { fontWeight: 'bold', marginTop: 5, color: '#de2d30', marginBottom: 5 },
    Text7: { fontWeight: 'bold', marginTop: 5, marginBottom: 30 },
    Text8: { color: '#9e9ea6', marginTop: 10, },
    Text8b: { color: 'black', marginTop: 10, marginBottom: 10, fontWeight: 'bold', },
    Text9: { color: '#de2d30', marginTop: 10, fontWeight: 'bold', },
    Text11: { color: '#9e9ea6', marginTop: 5 },
    CenterWarp: { marginTop: 30, },
    BottomWarp: { marginTop: 30, },
    logoutbtnstyle: { backgroundColor: '#0095ff', width: 60, padding: 5, marginTop: 10, marginBottom: 20, borderRadius: 5 },
    logouttext: { color: '#fff', textAlign: 'center' }
})
