import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, Modal, Platform, TextInput, Toast } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Card, CardItem, } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconFOU from "react-native-vector-icons/Foundation";
import IconANT from "react-native-vector-icons/AntDesign";
import IconENT from "react-native-vector-icons/Entypo";
import AsyncStorage from '@react-native-community/async-storage';



class ModalScreen extends Component {
   constructor(props) {
      super(props);
      this.state = {
         isSelectList: true,
         langPrint: ''
      };
   }

   componentDidMount() {
      AsyncStorage.getItem("language").then(langStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Note')
         this.setState({
            langPrint: langStorageRes
         })
      })
   }

   /************************************ addFavorite function ***********************************/
   addFavorite(index, tourcode = this.state.modalData.tour_code, access_token = this.state.tokenId) {
      console.log(tourcode, 'tourcode', access_token, 'addFavorite---add')
      const items = this.state.data
      const modalAdd = this.state.modalData
      let obj = this
      let url = `https://www.goldendestinations.com/api/new/like.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

      fetch(url).then((res) => res.json())
         .then(function (resJson) {
            console.log(resJson, '-----------resJson add');
            if (resJson[0].status === 1) {
               console.log(resJson, 'ok-------------add');
               foundItem = items.findIndex(x => x.tour_code == tourcode)
               modalAdd.isFavourite = "true"
               items[foundItem] = modalAdd
               console.log(items, 'items')
               obj.setState({
                  data: items
               })
            }
            else if (access_token === null) {
               alert(global.t('pls_login_add'))
            }
            else {
               alert(resJson[0].error)

            }
         })


      console.log('add')
   }

   /************************************ removeFavorite function ***********************************/
   removeFavorite(index, tourcode = this.state.modalData.tour_code, access_token = this.state.tokenId, ) {
      console.log(tourcode, 'tourcode', access_token, 'removeFavorite---remove')
      const items = this.state.dataFilter || this.state.calenderFilter
      const modalRemove = this.state.modalData
      let obj = this
      let url = `https://www.goldendestinations.com/api/new/unlike.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

      fetch(url).then((res) => res.json())

         .then(function (resJson) {
            console.log(resJson, '-----------resJson remove');
            if (resJson[0].status === 1) {
               console.log(resJson, 'remove-------------remove');
               foundItem = items.findIndex(x => x.tour_code == tourcode)
               modalRemove.isFavourite = "false"
               items[foundItem] = modalRemove
               console.log(items, 'items')
               obj.setState({
                  data: items
               })
            }
            else {
               alert(resJson[0].error)
            }
         })


      console.log('remove')
   };


   /*************************************** Download PDF File ***************************************/
   downloadFile() {
      function getLocalPath(url) {
         const filename = url.split('/').pop();
         return `${RNFS.DocumentDirectoryPath}/${filename}`;
      }

      const url = this.state.modalData.itinerary;
      const localFile = getLocalPath(url);

      const options = {
         fromUrl: url,
         toFile: localFile
      };
      RNFS.downloadFile(options).promise
         .then(() => FileViewer.open(localFile))
         .then(() => {
            // success
         })
         .catch(error => {
            // error
         });
   }


   /************************************ sharing function ***********************************/
   sharing() {
      const { langPrint, modalData } = this.state;
      console.log('sharing');
      Share.share(
         {
            message: langPrint === 'zh' ?
               `${global.t('Destination')}: ${modalData.cn_title}` + "\n" +
               `${global.t('Tour_Code')}: ${modalData.tour_code}` + "\n" +
               `${global.t('Departure')}: ${modalData.departure}` + "\n" +
               `${global.t('Airline')}: ${modalData.airline === null ? localdata[0].airlinelocal_cn : modalData.airline}` + "\n" +
               `${global.t('Status')}: ${modalData.status_cn === "" ? localdata[0].statuslocal_cn : modalData.status_cn}` + "\n" +
               `${global.t('Price')}: RM ${modalData.price}` + "\n" +
               `${global.t('Itinerary')}: ${modalData.itinerary}`
               :
               `Destination: ${modalData.title}` + "\n" +
               `Tourcode: ${modalData.tour_code}` + "\n" +
               `Departure: ${modalData.departure}` + "\n" +
               `Airline: ${modalData.airline === null ? localdata[0].airlinelocal_en : modalData.airline}` + "\n" +
               `Status: ${modalData.status === "" ? localdata[0].statuslocal_en : modalData.status}` + "\n" +
               `Price: RM ${modalData.price}` + "\n" +
               `Itinerary: ${modalData.itinerary}`
            ,
            title: 'Golden Destinations',
            url: modalData.itinerary
         }
      )
   }

   closeModal() {
      this.setState({
         isSelectList: false
      })
      Actions.search()
   }



   render() {
      const { isSelectList, langPrint } = this.state;
      const modalData = this.props.dataItem
      return (
         <Modal
            transparent={true}
            animationType={'fade'}
            visible={isSelectList}
            onRequestClose={() => { console.log('close modal') }}>
            <View style={styles.modalBackground}>
               <View style={styles.activityIndicatorWrapper}>
                  <TouchableOpacity onPress={() => this.closeModal()}>
                     <Text>close</Text>
                  </TouchableOpacity>

                  <Card >
                     <CardItem style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#fcf5f5', marginRight: 0 }}>
                        <Body style={{ marginRight: 0, }}>
                           <View style={{ flexDirection: 'row' }}>
                              <View style={{ alignItems: 'flex-start', width: '5%', }}>
                                 <Image source={{ uri: modalData.brand_logo }} style={{ width: 20, height: 20 }} />
                              </View>
                              <View style={{ width: '95%' }}>
                                 <Text note style={{ fontWeight: 'bold', textAlign: 'center', marginLeft: 5 }}>
                                    {langPrint === 'zh' ? modalData.cn_title : modalData.title}
                                 </Text>
                              </View>
                           </View>
                        </Body>
                     </CardItem>
                     <CardItem cardBody >
                        <View style={{ flexDirection: 'row' }}>
                           <View style={{ paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15 }}>
                              <View style={styles.textWarp}>
                                 <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Tour_Code')}: </Text>
                                 <Text note style={styles.TextInner2}>{modalData.tour_code}</Text>
                              </View>
                              <View style={styles.textWarp}>
                                 <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Airline')}: </Text>
                                 {
                                    langPrint === 'zh' ? (
                                       <Text note style={styles.TextInner2}>{modalData.airline === null ? localdata[0].airlinelocal_cn : modalData.airline}</Text>
                                    ) : (
                                          <Text note style={styles.TextInner2}>{modalData.airline === null ? localdata[0].airlinelocal_en : modalData.airline}</Text>
                                       )
                                 }
                              </View>
                              <View style={styles.textWarp}>
                                 <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Status')}: </Text>
                                 {
                                    langPrint === "zh" ? (
                                       <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                          {modalData.status_cn === "" ? localdata[0].statuslocal_cn : modalData.status_cn}
                                       </Text>
                                    ) : (
                                          <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                             {modalData.status === "" ? localdata[0].statuslocal_en : modalData.status}
                                          </Text>
                                       )
                                 }
                              </View>
                              <View style={styles.textWarp}>
                                 <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Departure')}: </Text>
                                 <Text note style={styles.TextInner2}>{modalData.departure}</Text>
                              </View>

                              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                 <Text note style={{ width: '30%', color: '#00953b', marginTop: 2 }}>{global.t('Price')}: </Text>
                                 <Text note style={styles.Textprice}>RM {modalData.price}</Text>
                              </View>
                           </View>

                        </View>
                     </CardItem>
                     <CardItem style={{ borderTopWidth: .5, borderTopColor: '#ccc' }}>
                        <View style={{ flexDirection: 'row' }}>
                           <View style={{ width: '33.33%', }}>
                              <TouchableOpacity onPress={modalData.isFavourite === "true" ? this.removeFavorite.bind(this) : this.addFavorite.bind(this)}>
                                 <IconFOU name="star" size={20} style={{
                                    textAlign: 'center',
                                    color: modalData.isFavourite === "true" ? "#ffc43f" : "#ccc"
                                 }} />
                              </TouchableOpacity>
                           </View>
                           <View style={{ width: '33.33%', borderLeftWidth: .5, borderLeftColor: '#ccc', borderRightColor: '#ccc', borderRightWidth: .5 }}>
                              <TouchableOpacity onPress={this.sharing.bind(this)} >
                                 <IconFOU name="share" size={20} style={{ textAlign: 'center', color: '#0095ff' }} />
                              </TouchableOpacity>
                           </View>
                           <View style={{ width: '33.33%' }}>
                              <TouchableOpacity onPress={this.downloadFile.bind(this)}>
                                 <IconFOU name="download" size={20} style={{ textAlign: 'center', color: '#de2d30' }} />
                              </TouchableOpacity>
                           </View>
                        </View>

                     </CardItem>
                  </Card>
               </View>
            </View>
         </Modal>

      );
   }
}


export default ModalScreen;

const styles = StyleSheet.create({

   modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
   activityIndicatorWrapper: {
      backgroundColor: '#FFFFFF', height: 300, width: '90%', borderRadius: 10, display: 'flex',
      alignItems: 'center', justifyContent: 'space-around'
   },


   IconColor: { color: '#de2d30', textAlign: 'center', paddingTop: 30, },
   TextTitle: { color: '#de2d30', fontWeight: 'bold', },
   TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
   TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, marginTop: 2 },
   Textprice: { color: '#00953b', width: '35%', marginTop: 2 },
   textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
   textWarp: { flexDirection: 'row', marginTop: 2, },
   downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
   dwnwarp: { width: '100%', },
   bodystyle: {
      width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
      paddingTop: 0, borderBottomWidth: 0
   },
   texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
   dwntext: { marginTop: 15, textAlign: 'center', marginRight: 0, color: '#de2d30' }

})
