import React, { Component } from 'react';
import {
    StyleSheet, View, ScrollView, TouchableHighlight, FlatList, Image, Platform, Picker, Modal, SafeAreaView,
    TouchableOpacity, Dimensions, ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Content, List, ListItem, Text, Left, Right, Icon, Body } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import { DepartingDetailApi } from '../../../../../PostApi'
import { Select, Option } from "react-native-chooser";
import HTML from 'react-native-render-html';
const { width } = Dimensions.get('window');
const height = width * 0.6;

class ItineraryPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            data: [],
            AsyncStorageId: 0,
            value: 'Day 1',
            selectDate: [
                { lat: 0, lng: 0 }
            ],
            childVisible: false,
            langPrint: '',
            dataDay1: [],
            dataWeather: [],
            catcherr: null

        };
    }

    onSelect(value) {
        this.setState({
            selectDate: this.state.data.filter((element) => {
                return element.day_num == value
            }),
            value: value,
            childVisible: true

        });
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------AsyncStorage-----ItineraryPage')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------fetchDataApi----ItineraryPage')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                //console.log(fetchData, '-----------------fetchData-----ItineraryPage')

                this.setState({
                    data: fetchData[0].daily_itinerary,
                    selectDate: fetchData[0].daily_itinerary,
                    dataDay1: fetchData[0].daily_itinerary[0],
                    lan_data: fetchData[0].daily_itinerary[0].lat,

                })
            })

        });

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----ItineraryPage')
            this.setState({
                langPrint: langStorageRes
            })
        })


    }

    componentDidUpdate() {
        this.getWeather();
    }

    getWeather(lat = this.state.selectDate[0].lat === "" ? "37.8267" : this.state.selectDate[0].lat,
        lng = this.state.selectDate[0].lng === "" ? "-122.4233" : this.state.selectDate[0].lng) {
        console.log(lat, lng, '-----------------------------lan_id')

        let url = `https://www.goldendestinations.com/api/new/weather.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&lat=${lat}&lng=${lng}`;
        const obj = this;
        fetch(url).then((res) => res.json())
            .then(function (myJson) {
                console.log(myJson, '-----------------myJson-----getWeather')
                console.log(url, '-----------------url-----getWeather')
                //console.log(JSON.stringify(myJson), '-------------stringify')
                obj.setState({
                    dataWeather: myJson
                })
            })
            .catch((error) => {
                console.log(error, 'response----------error');
                this.setState({
                    catcherr: error.message
                });
            });

    }




    _renderItem = ({ item }) => {
        //console.log(item, '----------ItineraryPage------item')
        const { langPrint } = this.state;
        return (

            <View>

                <View>

                    <View style={{ padding: 20 }}>
                        {
                            langPrint === 'zh' ? (
                                <Text style={{ fontSize: 25 }}>{item.title_cn}</Text>
                            ) : (
                                    <Text style={{ fontSize: 25 }}>{item.title}</Text>
                                )
                        }
                        <Text style={styles.subtext}>{global.t('Best_natural_views')}</Text>
                    </View>
                </View>

                {/*************** description ***************/}
                <View style={{ backgroundColor: '#e9ecef', }}>
                    <View style={{ padding: 20 }}>
                        <Text style={{ fontSize: 20, marginBottom: 20 }}>{global.t('Activity')}</Text>
                        {
                            langPrint === 'zh' ? (
                                <HTML
                                    html={item.action_cn}
                                    tagsStyles={{
                                        p: { marginBottom: 15 },
                                    }}
                                    imagesInitialDimensions={{ width: '100%', height: 480 }}
                                />
                            ) : (
                                    <HTML
                                        html={item.action}
                                        tagsStyles={{
                                            p: { marginBottom: 15 },
                                        }}
                                        imagesInitialDimensions={{ width: '100%', height: 480 }}
                                    />
                                )
                        }

                    </View>
                </View>



            </View>

        )
    }


    render() {

        const { data, selectDate, childVisible, value, dataDay1, langPrint, catcherr, dataWeather } = this.state;
        //console.log(selectDate[0].lat, '----------------ItineraryPage--------dataDay1')
        console.log(this.state.dataWeather, '-----------dataWeather')

        return (
            <ScrollView >
                {/* <Text>{catcherr ? catcherr : 'no error'}</Text>  */}
                <Select
                    onSelect={this.onSelect.bind(this)}
                    defaultText={value}
                    onChange={(e) => this.onSelectCurrency(e.target.value)}
                    style={{ borderWidth: 0, borderBottomWidth: .5, borderBottomColor: "#ccc", width: '100%', height: 50, backgroundColor: '#fff' }}
                    textStyle={{ color: 'red', textAlign: 'center', fontSize: 17, width: '100%', }}
                    backdropStyle={{ backgroundColor: "#00000078" }}
                    optionListStyle={{ backgroundColor: "#F5FCFF", height: 200 }}
                    animationType='none'
                    transparent={true}
                    indicator='down'
                    indicatorColor='red'
                    indicatorSize={Platform.OS === 'ios' ? 15 : 12}
                    indicatorStyle={{ marginTop: Platform.OS === 'ios' ? 10 : 2, right: 30 }}

                >
                    {
                        data.map((item, i) => {
                            return (
                                <Option value={item.day_num} key={i}>{item.day_num}</Option>
                            )
                        })
                    }
                </Select>
                <ScrollView horizontal={true}>
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        {
                            dataWeather.map((item, i) => {
                                return (
                                    <View style={styles.cardwarpper}>
                                        <Text style={styles.locationstyle}>{item.Date}</Text>
                                        <Text style={[styles.locationstyle, {marginTop: 0}]}>{item.Summary}</Text>
                                        <View style={styles.imgstatuswarp}>
                                            <Image
                                                style={{ width: 80, height: 60, }}
                                                source={{ uri: item.Icon }}
                                            />
                                        </View>
                                        <Text style={styles.weatherstatus}>{item.Temperature}</Text>
                                    </View>
                                )
                            })
                        }
                        {/* <Image
                        source={require('../../../../assets/images/img/co01.png')}
                        style={styles.TopImage}
                    /> */}
                    </View>
                </ScrollView>
                {
                    childVisible ? (
                        <FlatList
                            data={selectDate}
                            renderItem={this._renderItem}
                        />
                    ) : (

                            <View>

                                <View>

                                    <View style={{ padding: 20 }}>
                                        {
                                            langPrint === 'zh' ? (
                                                <Text style={{ fontSize: 25 }}>{dataDay1.title_cn}</Text>
                                            ) : (
                                                    <Text style={{ fontSize: 25 }}>{dataDay1.title}</Text>
                                                )
                                        }
                                        <Text style={styles.subtext}>{global.t('Best_natural_views')}</Text>
                                    </View>
                                </View>
                                {/*************** description ***************/}
                                <View style={{ backgroundColor: '#e9ecef', }}>
                                    <View style={{ padding: 20 }}>
                                        <Text style={{ fontSize: 20, marginBottom: 20 }}>{global.t('Activity')}</Text>
                                        {
                                            langPrint === 'zh' ? (
                                                <HTML
                                                    html={dataDay1.action_cn}
                                                    tagsStyles={{
                                                        p: { marginBottom: 15 },
                                                    }}
                                                    imagesInitialDimensions={{ width: '100%', height: 480 }}
                                                />
                                            ) : (
                                                    <HTML
                                                        html={dataDay1.action}
                                                        tagsStyles={{
                                                            p: { marginBottom: 15 },
                                                        }}
                                                        imagesInitialDimensions={{ width: '100%', height: 480 }}
                                                    />
                                                )
                                        }

                                    </View>
                                </View>

                            </View>
                        )

                }




            </ScrollView>
        );
    }
}


export default ItineraryPage;

const styles = StyleSheet.create({
    TopImage: { width: "100%", resizeMode: 'cover', flex: 1, position: 'relative' },
    ListItemstyle: { borderBottomWidth: 0, marginLeft: 0 },
    ListItemViewstyle: { paddingRight: 10 },
    ListItemiconstyle: { fontSize: 15 },
    subtext: { color: '#777', marginTop: 5 },
    icontext: { color: '#777' },
    scrollContainer: { height, },
    image: { width: 150, height, marginRight: 5 },
    locationstyle: { fontSize: 12, fontWeight: 'bold', color: '#fff', margin: 10, textTransform: 'uppercase', textAlign: 'center' },
    time: {
        fontSize: 16, color: '#fff',
        ...Platform.select({
            ios: { paddingBottom: 30, },
            android: { paddingBottom: 10, }
        })
    },
    cardwarpper: {
        flex: 1, flexDirection: 'column', alignItems: 'center', width: 100, marginRight: 1,
        backgroundColor: '#8eabcd',
    },
    imgstatuswarp: { flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    weatherstatus: { fontSize: 16, color: '#fff', margin: 10 }
})
