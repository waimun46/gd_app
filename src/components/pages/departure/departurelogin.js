import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, ImageBackground, TouchableOpacity, TextInput, Platform } from 'react-native';
import { Container, Header, Text, Tab, Tabs, ScrollableTab, Icon, TabHeading, Form, Item, Input, Label, Button, Toast } from 'native-base';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import IconANT from "react-native-vector-icons/AntDesign";

/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
// const buttonText = global.t('close');
const duration = 3000;
const style = { top: '25%' };



class DepartureLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            code: '',
            password: '',
            Error: '',
            id: 0
        }
    }

    /****************************************** Updata Value onChange TextInput ******************************************/
    updataValue(text, field) {
        console.log(text)
        if (field == 'code') { this.setState({ code: text }) }
        if (field == 'password') { this.setState({ password: text }) }
    }

    /****************************************** Submit Register Fetch API  ******************************************/
    onSubmit(code = this.state.code, password = this.state.password) {
        console.log(code, password, '--------------code,password')
        /****************************** Validations form Toast *****************************/
        if (this.state.code === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Code'),
                    buttonText: global.t('close'), 
                    position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else if (this.state.password === "") {
            this.setState({
                Error: Toast.show({
                    text: global.t('Fill_Password'),
                    buttonText: global.t('close'), 
                    position: position,
                    duration: duration, textStyle: textStyle, style: style,
                })
            })
        }
        else {
            this.setState({ Error: '' })
            /************************************ Fetch API  ***********************************/
            let url = `https://goldendestinations.com/api/new/departing_login.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&code=${code}&password=${password}`;
            fetch(url).then((res) => res.json())
                .then(function (myJson) {
                    console.log(myJson, '-----------myJson');
                    if (myJson[0].status == 1) {
                        /****************** Store Data AsyncStorage ******************/
                        AsyncStorage.setItem('USER_DATA', myJson[0].id);
                        return myJson.json(Actions.Departure())
                    } else {
                        alert(myJson[0].error);
                    }
                })
        }
    }


    render() {
        const { Error } = this.state;
        return (
            <ImageBackground
                source={require('../../../assets/images/background/dpbg.png')}
                style={styles.Container}
            >
                <ScrollView>
                    <View >
                        <Text style={{ color: 'yellow', textAlign: 'center', position: 'absolute', width: '100%', marginTop: 100 }}>{Error}</Text>
                        <View style={styles.contentWarp}>
                            <Form >
                                <View style={styles.forminnerwarp}>
                                    {/*************************** Tour Code ***************************/}
                                    <Item style={styles.Item_1_style}>
                                        <Label style={styles.labelstyle}>{global.t('Tour_Code')}</Label>
                                        <TextInput
                                            placeholder={global.t('Placeholder_Fill_Code')}
                                            placeholderTextColor='#ffffffad'
                                            style={styles.inputstyle}
                                            onChangeText={(text) => this.updataValue(text, 'code')}
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                        />
                                    </Item>
                                    {/*************************** Pass Code ***************************/}
                                    <Item style={styles.Item_2_style}>
                                        <Label style={styles.labelstyle}>{global.t('Pass_Code')}</Label>
                                        <TextInput
                                            placeholder={global.t('Placeholder_Fill_Password')}
                                            placeholderTextColor='#ffffffad'
                                            secureTextEntry={true}
                                            style={styles.inputstyle}
                                            onChangeText={(text) => this.updataValue(text, 'password')}
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            
                                        />
                                    </Item>

                                    {/* <Item style={[styles.Item_2_style, {borderBottomWidth:0}]}>
                                        <Text style={{textAlign: 'center', width: '100%', color: '#fff'}}>{global.t('coming_soon')}</Text>
                                    </Item> */}
                                </View>
                                {/*************************** Submit button ***************************/}
                                <View style={styles.btnsubwarp}>
                                    {/* <Button transparent style={styles.btnsty} onPress={() => Actions.Home()}>
                                        <Text style={{ color: '#00953b', fontSize: 16 }}>ok</Text>
                                    </Button> */}
                                    <Button transparent style={styles.btnsty} onPress={() => this.onSubmit()}>
                                        <Text style={{ color: '#00953b', fontSize: 16 }}>{global.t('Login')}</Text>
                                    </Button>
                                </View>
                            </Form>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}


export default DepartureLogin;

const styles = StyleSheet.create({
    Container: { width: '100%', height: '100%', },
    contentWarp: {
        width: '80%', marginLeft: 'auto', marginRight: 'auto',
        ...Platform.select({
            ios: { marginTop: 200, },
            android: { marginTop: 100, }
        })
    },
    forminnerwarp: { width: '100%', backgroundColor: '#de2d30', borderTopLeftRadius: 10, borderTopRightRadius: 10, padding: 20 },
    Item_1_style: { borderColor: 'white', width: '100%', marginLeft: 0, height: 50 },
    Item_2_style: { marginTop: 20, marginBottom: 20, borderColor: 'white', width: '100%', marginLeft: 0, height: 50 },
    labelstyle: { color: '#fff', width: '30%', fontSize: 14 },
    inputstyle: { color: 'white', height: 50, width: '70%' },
    btnsubwarp: { width: '100%', backgroundColor: 'white', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, padding: 10 },
    btnsty: { width: '100%', justifyContent: "center", },

})
