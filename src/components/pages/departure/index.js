import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Text, Tab, Tabs, ScrollableTab, Icon, TabHeading } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconION from "react-native-vector-icons/Ionicons";
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import Note from './components/note';
import InformationPage from './components/information';
import ItineraryPage from './components/itinerary';
import HotelPage from './components/hotel';
import FlightPage from './components/flight';
import ConverterPage from './components/converter';
import NewConverter from './components/newConverter';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar } from 'react-native-scrollable-tab-view-forked'


import { DepartingDetailApi } from '../../../../PostApi';




class DepartureInfor extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentTab: 0,
            data: [],
            AsyncStorageId: 0
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            //console.log(asyncStorageRes, '----------------AsyncStorage')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            //console.log(this.state.AsyncStorageId, '-------------fetchDataApi1111')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                //console.log(fetchData, '-----------------fetchData')
                this.setState({
                    data: fetchData
                })
            })
            // this.fetchDataApi(this.state.AsyncStorageId);
        });


    }


    render() {
        console.log(this.state.AsyncStorageId, '-----------id')
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <ScrollableTabView

                    renderTabBar={() => (
                        <ScrollableTabBar
                            style={styles.scrollStyle}
                            tabStyle={styles.tabStyle}
                        />
                    )}
                    tabBarTextStyle={styles.tabBarTextStyle}
                    tabBarInactiveTextColor={'black'}
                    tabBarActiveTextColor={'red'}
                    tabBarUnderlineStyle={styles.underlineStyle}
                    initialPage={0}
                >
                    <NewConverter key={'5'} tabLabel={global.t('Converter')} />
                    <Note key={'1'} tabLabel={global.t('Tour')} />
                    <FlightPage key={'2'} tabLabel={global.t('Flight')} />
                    <HotelPage key={'3'} tabLabel={global.t('Hotel')} />
                    <ItineraryPage key={'4'} tabLabel={global.t('Itinerary')} />
                    <InformationPage key={'6'} tabLabel={global.t('Tips')} />
                </ScrollableTabView>
            </View>
        );
    }
}


export default DepartureInfor;

const styles = StyleSheet.create({
    IconStyle: { paddingRight: 5 },
    TabStyle: { backgroundColor: "white" },
    TabHeadingStyle: { backgroundColor: "white" },
    activeTabStyle: {
        color: 'red'
    },
    tabStyle: {
        color: 'red'
    },
    tabStyle: {},
    scrollStyle: {
        backgroundColor: 'white',

        // justifyContent: 'center',
    },
    tabBarTextStyle: {
        fontSize: 14,
        fontWeight: 'normal',

    },
    underlineStyle: {
        height: 3,
        backgroundColor: 'red',
        borderRadius: 3,
        width: 30,
    },
})
