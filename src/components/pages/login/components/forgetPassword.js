import React, { Component } from 'react';
import { StyleSheet, View, Platform, ScrollView, TextInput, Alert } from 'react-native';
import { Form, Item, Input, Text, Button, Toast } from 'native-base';
import { Actions } from 'react-native-router-flux';
import FA from 'react-native-vector-icons/FontAwesome';
import ANT from 'react-native-vector-icons/AntDesign';

/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
const duration = 3000;
const style = { top: '10%' };



class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    };
  }

  onSubmit(email = this.state.email) {
    if (this.state.email === "") {
      this.setState({
        Error: Toast.show({
          text: global.t('Fill_Email'),
          buttonText: global.t('close'),
          position: position,
          duration: duration, textStyle: textStyle, style: style,
        })
      })
    } else {
      let url = `https://www.goldendestinations.com/api/new/forgot_password.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&email=${email}`;
      console.log('url---email', url);
      let that = this;
      fetch(url).then((res) => res.json())
        .then((postData) => {
          if (postData[0].status === 1) {
            Alert.alert(global.t('temporary_password'), '',
              [
                {
                  text: 'OK', onPress: () => Actions.Home()
                },
              ],
              { cancelable: false },
            );
          } else {
            Alert.alert(postData[0].error, '',
              [
                {
                  text: 'OK', onPress: () => that.setState({ email: '', })
                },
              ],
              { cancelable: false },
            );
          }
        })

    }

  }

  /****************************************** Updata Value onChange TextInput ******************************************/
  updataValue(text, field) {
    console.log(text)
    if (field == 'email') { this.setState({ email: text }) }
  }

  render() {
    const { email } = this.state;
    return (
      <View style={styles.container}>

        <ScrollView style={{ width: '100%', }}>
          <View style={{ alignItems: 'center' }}>
            <View style={styles.iconContainer}>
              <View style={styles.iconWarp}>
                <FA name="lock" size={75} color='#fff' />
              </View>
              <Text note style={{ textAlign: 'center', padding: 10, marginTop: 10 }}>{global.t('email_recover')}</Text>
            </View>


            <Form style={{ width: '100%', marginTop: 20 }}>
              <Item style={styles.itemWarp}>
                <FA name='envelope' size={20} style={styles.iconStyInput} />
                <TextInput
                  value={email}
                  placeholder={global.t('Fill_Email')}
                  placeholderTextColor='gray'
                  style={styles.InputStyle}
                  onChangeText={(text) => this.updataValue(text, 'email')}
                  autoCorrect={false}
                  autoCapitalize="none"
                />
              </Item>
            </Form>
          </View>
        </ScrollView>

        {/*************************** Submit button ***************************/}
        <View style={styles.Buttonwarp}>
          <Button block style={styles.bottomStyle} onPress={() => this.onSubmit()}>
            <Text style={styles.BottomText} uppercase={false}>{global.t('Submit')}</Text>
          </Button>
        </View>


      </View>
    );
  }
}

export default ForgetPassword;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', },
  iconWarp: { width: 130, height: 130, borderRadius: 100, backgroundColor: '#ccc', alignItems: 'center', justifyContent: 'center' },
  iconContainer: { alignItems: 'center', marginTop: 50, width: '80%', },
  iconStyInput: { paddingRight: 10, width: '10%', textAlign: 'center', color: 'gray' },
  itemWarp: { marginLeft: 0, paddingLeft: 20 },
  Buttonwarp: {
    position: 'absolute', flex: 0.1, left: 0, right: 0, alignItems: 'center', width: '100%',
    ...Platform.select({
      ios: { bottom: -30, },
      android: { bottom: -20, }
    }), flexDirection: 'row',
    ...Platform.select({
      ios: { height: 100, },
      android: { height: 80, }
    }),
  },
  bottomStyle: {
    width: '100%', backgroundColor: '#fc564e', borderRadius: 0,
    ...Platform.select({
      ios: { height: 70 },
      android: { height: 60 }
    }),
  },
  BottomText: { fontSize: 16 },
  InputStyle: { height: 70, textAlign: 'left', color: '#000', padding: 10, width: '100%', paddingLeft: 0 },

});
