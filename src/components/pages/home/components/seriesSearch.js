import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Dimensions, TouchableOpacity } from 'react-native';
import { Button, Icon, Text, Card, CardItem, Toast } from 'native-base';
import YearMonthPicker from './components/yearMonthPicker';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import IconENT from "react-native-vector-icons/Entypo";

/************************ Validations Toast Style **************************/
const textStyle = { color: "yellow" };
const position = "top";
const duration = 3000;
const style = { top: '15%' };



class SeriesSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startYear: '',
      endYear: 10000,
      selectedYear: '',
      selectedMonth: '',
      destination: '',

    };
  }

  /**************************************************** showPicker date ***************************************************/
  showPicker = () => {
    const { startYear, endYear, selectedYear, selectedMonth } = this.state;
    this.picker
      .show({ startYear, endYear, selectedYear, selectedMonth })
      .then(({ year, month }) => {
        this.setState({
          selectedYear: year,
          selectedMonth: month
        })
      })
  }

  /****************************************** Updata Value onChange TextInput ******************************************/
  updataValue(text, field) {
    console.log(text)
    if (field == 'destination') { this.setState({ destination: text }) }

  }

  onSearch() {
    const { destination, selectedYear, selectedMonth } = this.state;
    /****************************** Validations form Toast *****************************/
    if (destination === "") {
      this.setState({
        Error: Toast.show({
          text: global.t('Please_destination'),
          buttonText: global.t('close'),
          position: position,
          duration: duration, textStyle: textStyle, style: style,
        })
      })
    }
    // else if (selectedYear === "") {
    //   this.setState({
    //     Error: Toast.show({
    //       text: global.t('Please_month'),
    //       buttonText: global.t('close'),
    //       position: position,
    //       duration: duration, textStyle: textStyle, style: style,
    //     })
    //   })
    // }
    // else if (selectedMonth === "") {
    //   this.setState({
    //     Error: Toast.show({
    //       text: global.t('Please_month'),
    //       buttonText: global.t('close'),
    //       position: position,
    //       duration: duration, textStyle: textStyle, style: style,
    //     })
    //   })
    // }
    else {
      Actions.seriesListing({ keyword: destination, year: selectedYear, month: selectedMonth })
      this.setState({
        selectedYear: '',
        selectedMonth: '',
        destination: ''
      })
    }
  }


  render() {
    const { selectedYear, selectedMonth, destination } = this.state;
    return (
      <View style={styles.container}>
        <Card>
          <CardItem style={styles.cardContainer}>
            <View style={styles.leftWarp}>

              {/***************************************** herder title  *****************************************/}
              <View style={styles.leftTitle}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>{global.t('series_tour_search')}</Text>
                {
                  selectedYear === '' ? null : <Text style={styles.yearMonthText}>{selectedYear}/{selectedMonth}</Text>
                }
              </View>

              {/***************************************** search and date select  *****************************************/}
              <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '90%' }}>
                  <TextInput
                    value={destination}
                    placeholder={global.t('Search_destinations')}
                    placeholderTextColor='#00000080'
                    style={styles.inputstyle}
                    onChangeText={(text) => this.updataValue(text, 'destination')}
                    autoCorrect={false}
                    autoCapitalize="none"
                  />
                </View>
                <View style={{ width: '10%' }}>
                  <IconANT name='calendar' style={{ fontSize: 22, color: '#de2d30' }} onPress={this.showPicker} />
                  <YearMonthPicker ref={(picker) => this.picker = picker} />
                </View>
              </View>
            </View>

            {/***************************************** search btn  *****************************************/}
            <View style={styles.rightWarp}>
              <TouchableOpacity onPress={() => this.onSearch()} style={{ width: '100%', alignItems: 'center' }}>
                <IconENT name='arrow-long-right' style={{ fontSize: 25, color: '#fff' }} />
              </TouchableOpacity >
            </View>

          </CardItem>
        </Card>
      </View>
    );
  }
}

export default SeriesSearch;

const styles = StyleSheet.create({
  container: { flex: 1, marginTop: 20, paddingLeft: 10, paddingRight: 10, marginBottom: 220 },
  inputstyle: { borderBottomWidth: .5, borderBottomColor: '#ccc', padding: 5, width: '100%', marginBottom: 10 },
  showPickerBtn: { backgroundColor: '#eee', alignItems: 'flex-start', justifyContent: 'center', },
  yearMonthText: { fontSize: 13, color: '#0095ff', paddingLeft: 5, fontWeight: 'bold' },
  cardContainer: { paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, height: 130 },
  leftWarp: { width: '78%', padding: 20 },
  rightWarp: { width: '22%', backgroundColor: '#de2d30', padding: 20, height: 130, justifyContent: 'center', alignItems: 'center' },
  leftTitle: { marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },


})

