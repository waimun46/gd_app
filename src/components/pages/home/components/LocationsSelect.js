import React, { Component } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Dimensions, ScrollView, FlatList, Animated, Modal, Platform, WebView } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { TourImgApi, LandingApi } from '../../../../../PostApi';
import ANT from 'react-native-vector-icons/AntDesign';
import Carousel from 'react-native-snap-carousel';
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
// import { WebView } from 'react-native-webview';


const temImg = "https://cdn.vox-cdn.com/thumbor/WhZyVZT7r8HLrE2pE6gK1JVRXwg=/0x0:1200x800/1200x800/filters:focal(504x304:696x496)/cdn.vox-cdn.com/uploads/chorus_image/image/64895811/youtube.0.jpg";

const localData = [
   { id: 1, img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', },
   { id: 2, img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
   { id: 3, img: 'https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' },
   {
      id: 4,
      img: 'https://cdn.vox-cdn.com/thumbor/WhZyVZT7r8HLrE2pE6gK1JVRXwg=/0x0:1200x800/1200x800/filters:focal(504x304:696x496)/cdn.vox-cdn.com/uploads/chorus_image/image/64895811/youtube.0.jpg',
      video: 'https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4'
   },
]
const { width: viewportWidth } = Dimensions.get('window');
const { widthVideo, heightVideo } = Dimensions.get('window');
const screenWidth = Dimensions.get('window').width;
const DEVICE_WIDTH = Dimensions.get('window').width;

class LocationsSelect extends Component {
   scrollRef = React.createRef();

   constructor(props) {
      super(props);
      this.state = {
         data: [0, 1, 2, 3, 4, 5],
         size: { widthVideo, heightVideo },
         currentTime: 0,
         duration: 0,
         isFullScreen: false,
         isLoading: true,
         paused: true,
         playerState: PLAYER_STATES.PLAYING,
         screenType: 'content',
         bunnyVideo: false,
         songVideo: false,
         loop: true,
         timer: 7000,
         pageLenght: '',
         imageSlide: true,
         selectIndex: 0,
         stopVideo: false,
         modalVisible: false,
         modalData: { video: '', },
         landingData: []
      }
   }

   /************************************************************ openModalProduct ***********************************************************/
   openModalSlider(title) {
      const modalData = this.state.landingData.find(element => {
         return element.title === title
      })
      this.setState({
         modalVisible: true,
         modalData: { video: modalData.video }
      });
   };

   /************************************************************ closeImage ***********************************************************/
   closeModal = () => {
      console.log('openImage')
      this.setState({
         modalVisible: false
      })
   }



   componentDidMount() {
      /************************* Fetch TourImgApi in PostApi *************************/
      TourImgApi().then((fetchData) => {
         //console.log(fetchData, 'fetchData------------------1');
         this.setState({
            data: fetchData
         })
      })

      /************************* Fetch LandingApi in PostApi *************************/
      LandingApi().then((fetchData) => {
         //console.log(fetchData, 'fetchData------------------1');
         this.setState({
            landingData: fetchData
         })
      })

      this.looperImage()
      this.onPageLoop();

      this.interval = setInterval(() => {
         this.setState(prev => ({
            selectIndex: prev.selectIndex === this.state.landingData.length - 1 ? 0 : prev.selectIndex + 1
         }),
            () => {
               this.scrollRef.current.scrollTo({
                  animated: true,
                  y: 0,
                  x: DEVICE_WIDTH * this.state.selectIndex
               })
            })
      }, 3000)
      // }, this.state.landData[this.state.selectIndex].interval ? )

   }


   stopSlider = () => {
      clearInterval(this.interval);
   }

   onPageLoop = (pages) => {
      this.setState({
         pageLenght: pages
      })
   }

   looperImage = () => {
      setTimeout(() => {
         this.setState({
            loop: false,
            bunnyVideo: true,
            // isLoading: true
         })
      }, 14000)

      setTimeout(() => {
         this.setState({
            loop: false, 
            songVideo: true,
         })
      }, 600000)
   }

   onSeek = seek => {
      //Handler for change in seekbar
      this.videoPlayer.seek(seek);
   };

   onPaused = playerState => {
      console.log('onPaused------playStatus');
      //Handler for Video Pause
      this.setState({
         paused: !this.state.paused,
         playerState,
         stopVideo: true
      });

      this.stopSlider();

   };




   onReplay = () => {
      //Handler for Replay
      this.setState({ playerState: PLAYER_STATES.PLAYING, });
      this.videoPlayer.seek(0);

   };

   onProgress = data => {
      const { isLoading, playerState } = this.state;
      // Video Player will continue progress even if the video already ended
      if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
         this.setState({ currentTime: data.currentTime });
      }
   };

   onLoad = data => {
      console.log('start---onLoad')
      this.setState({
         duration: data.duration,
         isLoading: false,
      });
   }

   onLoadStart = data => {
      console.log('start----onLoadStart')
      this.setState({ isLoading: true, });
   }

   onEnd = () => {
      console.log('end')
      this.setState({
         playerState: PLAYER_STATES.ENDED,
         bunnyVideo: false,
      });

   }
   onError = () => alert('Oh! ', error);

   exitFullScreen = () => {
      alert('Exit full screen');
   };

   enterFullScreen = () => { };

   onFullScreen = () => {
      if (this.state.screenType == 'content')
         this.setState({ screenType: 'cover' });
      else this.setState({ screenType: 'content' });
   };
   renderToolbar = () => (
      <View>
         <Text> toolbar </Text>
      </View>
   );
   onSeeking = currentTime => {
      console.log('start----onSeeking')
      this.setState({ currentTime });
   }


   _renderItem = ({ item, index }) => {
      console.log("rendering,", index, item)
      return (
         <View style={styles.slideBanner}>
            <View style={styles.bannerwarp}>
               <Image source={{ uri: item.img }} style={styles.bannerSty} />
            </View>
         </View>
      );
   }





   setSelectIndex = event => {
      const viewSize = event.nativeEvent.layoutMeasurement.width;
      const contentOffset = event.nativeEvent.contentOffset.x;
      const selectIndex = Math.floor(contentOffset / viewSize);
      this.setState({
         selectIndex
      })
   }

   playVideo = () => {
      console.log('playVideo')
      if (this.video) {
         this.video.playAsync();
      }
   }


   pauseVideo = () => {
      if (this.video) {
         this.video.pauseAsync();
      }
   }

   render() {
      const { selectIndex, modalVisible, modalData, landingData } = this.state;
      // console.log('localData', localData.length)
      // console.log('selectIndex', selectIndex)

      // console.log('paused', this.state.paused)
      //console.log('landingData---------', landingData)

      // <TouchableOpacity onPress={() => this.openModalSlider(item.id)}>
      //    <View style={{ width: screenWidth, height: 200, }}>
      //       {/* <Image source={{ uri: item.img }} style={styles.bannerSty} /> */}
      //       <WebView
      //          style={{ marginTop: (Platform.OS == 'ios') ? 20 : 0, }}
      //          javaScriptEnabled={true}
      //          domStorageEnabled={true}
      //          source={{ uri: 'https://www.youtube.com/embed/' + item.image }}
      //       />
      //    </View>
      // </TouchableOpacity>
      return (
         <View style={{ backgroundColor: '#fff', flex: 1 }}>
            <View style={{ position: 'relative' }}>
               <ScrollView
                  horizontal={true}
                  pagingEnabled
                  onMomentumScrollEnd={this.setSelectIndex}
                  ref={this.scrollRef}
               >
                  <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
                     {
                        landingData.map((item, index) => {
                           return (
                              item.types === '2' ?
                                 <View style={{ width: screenWidth, height: 200, }}>
                                    {/* <Image source={{ uri: item.img }} style={styles.bannerSty} /> */}
                                    {/* <WebView
                                       // style={{ marginTop: (Platform.OS == 'ios') ? 20 : 0, }}
                                       javaScriptEnabled={true}
                                       domStorageEnabled={true}
                                       source={{ uri: 'https://www.youtube.com/embed/' + item.image }}
                                    /> */}
                                    <TouchableOpacity onPress={() => this.openModalSlider(item.title)}>
                                       <Image source={{ uri: item.image }} style={styles.bannerSty} />
                                    </TouchableOpacity>
                                 </View>
                                 :
                                 <View style={{ width: screenWidth, height: 200, }}>
                                    <Image source={{ uri: item.image }} style={styles.bannerSty} />
                                 </View>
                           )
                        })
                     }

                     {/* <Image
                        source={require('../../../../assets/images/img/co01.png')}
                        style={styles.TopImage}
                    /> */}
                  </View>
               </ScrollView>
               <View style={{
                  position: 'absolute', bottom: 20, height: 10, display: 'flex',
                  flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%'
               }}>
                  {
                     landingData.map((item, i) => {

                        return (
                           <View key={i} style={{
                              width: 6, height: 6, borderRadius: 3, margin: 5,
                              backgroundColor: '#fff', opacity: i === selectIndex ? 0.5 : 1
                           }} />
                        )
                     })
                  }
               </View>
            </View>



            {/* <ScrollView horizontal={true} >
               <View style={{ flex: 1, flexDirection: 'row', }}>
                  {
                     localData.map((item, i) => {
                        return (
                           <View style={styles.slideBanner}>
                             <Image source={{ uri: item.img }} style={styles.bannerSty} />
                           </View>
                        )
                     })
                  }
               </View>
            </ScrollView> */}
            {/* <View style={{ height: 150  , flex:1}}>
               <FlatList
                  horizontal={true}
                  data={localData}
                  // renderItem={({ item }) => <SenderChatRow data={item} />}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => index}
               />
            </View> */}


            {/* <Carousel
               ref={(c) => { this._carousel = c; }}
               data={localData}
               renderItem={this._renderItem}
               sliderWidth={viewportWidth}
               itemWidth={viewportWidth}
               layout={'default'}
               firstItem={0}
               autoplay={true}
               autoplayDelay={4000}
               autoplayInterval={4000}
               loop={true}
            /> */}


            {/* <Video source={{ uri: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" }}
               ref={(ref) => {
                  this.player = ref
               }}
               onBuffer={this.onBuffer}
               onError={this.videoError}
               style={styles.backgroundVideo} /> */}
            {/* <Video
               onEnd={this.onEnd}
               onLoad={this.onLoad}
               onLoadStart={this.onLoadStart}
               onProgress={this.onProgress}
               paused={this.state.paused}
               ref={videoPlayer => (this.videoPlayer = videoPlayer)}
               resizeMode={this.state.screenType}
               resizeMode='cover'
               onFullScreen={this.state.isFullScreen}
               source={{ uri: 'https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4' }}
               style={styles.mediaPlayer}
               volume={1}
            /> */}


            <View style={[styles.Container, { marginTop: 5, marginBottom: 10 }]}>
               <TouchableOpacity style={{ width: '33.33%', padding: 5, height: 100 }}
                  onPress={() => Actions.hightLine({
                     code_id: this.state.data[0].tourcode,
                     keyword_id: this.state.data[0].keyword
                  })}
               >
                  <Image
                     source={{ uri: this.state.data[0].pic_url }}
                     style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                  />
                  <Text style={styles.TextLeftTop}>RM {this.state.data[0].starter_price}</Text>
               </TouchableOpacity>
               <TouchableOpacity style={{ width: '33.33%', height: 100, paddingTop: 5, paddingBottom: 5 }}
                  onPress={() => Actions.hightLine({
                     code_id: this.state.data[1].tourcode,
                     keyword_id: this.state.data[1].keyword
                  })}
               >
                  <Image
                     source={{ uri: this.state.data[1].pic_url }}
                     style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                  />
                  <Text style={[styles.TextLeftTop, { right: 0 }]}>RM {this.state.data[1].starter_price}</Text>
               </TouchableOpacity>
               <TouchableOpacity style={{ width: '33.33%', padding: 5, height: 100 }}
                  onPress={() => Actions.hightLine({
                     code_id: this.state.data[2].tourcode,
                     keyword_id: this.state.data[2].keyword
                  })}
               >
                  <Image
                     source={{ uri: this.state.data[2].pic_url }}
                     style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                  />
                  <Text style={styles.TextLeftTop}>RM {this.state.data[2].starter_price}</Text>
               </TouchableOpacity>
            </View>




            {/* <View style={styles.Container}>
               <View style={{ width: '70%', }}>
                  <TouchableOpacity style={styles.LeftTopContainer}
                     onPress={() => Actions.hightLine({
                        code_id: this.state.data[0].tourcode,
                        keyword_id: this.state.data[0].keyword
                     })}
                  >
                     <Image
                        source={{ uri: this.state.data[0].pic_url }}
                        style={styles.LeftTopImage}
                     />
                     <Text style={styles.TextLeftTop}>RM {this.state.data[0].starter_price}</Text>
                  </TouchableOpacity>

                  <View style={styles.LeftBottomContainer}>

                     <TouchableOpacity style={styles.LeftBottomLeftImage}
                        onPress={() => Actions.hightLine({
                           code_id: this.state.data[1].tourcode,
                           keyword_id: this.state.data[1].keyword
                        })}
                     >
                        <Image
                           source={{ uri: this.state.data[1].pic_url }}
                           style={styles.LeftBottomLeftImageImg}
                        />
                        <Text style={styles.TextLeftBottom}>RM {this.state.data[1].starter_price}</Text>
                     </TouchableOpacity>


                     <TouchableOpacity style={styles.LeftBottomRightImage}
                        onPress={() => Actions.hightLine({
                           code_id: this.state.data[2].tourcode,
                           keyword_id: this.state.data[2].keyword
                        })}
                     >
                        <Image
                           source={{ uri: this.state.data[2].pic_url }}
                           style={styles.LeftBottomRightImageImg}
                        />
                        <Text style={styles.TextLeftBottom}>RM {this.state.data[2].starter_price}</Text>
                     </TouchableOpacity>

                  </View>
               </View>
               <View style={{ width: '30%', }}>
                  <View style={styles.RightTopContainer}>
                     <TouchableOpacity style={styles.RightImagePadding}
                        onPress={() => Actions.hightLine({
                           code_id: this.state.data[3].tourcode,
                           keyword_id: this.state.data[3].keyword
                        })}
                     >
                        <Image
                           source={{ uri: this.state.data[3].pic_url }}
                           style={styles.RightImageImg}
                        />
                        <Text style={styles.TextRight}>RM {this.state.data[3].starter_price}</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.RightImagePadding}
                        onPress={() => Actions.hightLine({
                           code_id: this.state.data[4].tourcode,
                           keyword_id: this.state.data[4].keyword
                        })}
                     >
                        <Image
                           source={{ uri: this.state.data[4].pic_url }}
                           style={styles.RightImageImg}
                        />
                        <Text style={styles.TextRight}>RM {this.state.data[4].starter_price}</Text>
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.RightImagePadding}
                        onPress={() => Actions.hightLine({
                           code_id: this.state.data[5].tourcode,
                           keyword_id: this.state.data[5].keyword
                        })}
                     >
                        <Image
                           source={{ uri: this.state.data[5].pic_url }}
                           style={styles.RightImageImg}
                        />
                        <Text style={styles.TextRight}>RM {this.state.data[5].starter_price}</Text>
                     </TouchableOpacity>
                  </View>
               </View>
            </View> */}


            <Modal
               animationType="slide"
               transparent={true}
               visible={modalVisible}
               onRequestClose={() => {
                  Alert.alert('Modal has been closed.');
               }}>
               <View style={styles.imgModalWarp}>
                  <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
                     <ANT name="close" size={30} style={{ color: '#fff', }} />
                  </TouchableOpacity>

                  <View style={{ width: screenWidth, height: 300, backgroundColor: '#000' }}>
                     <WebView
                        // style={{ marginTop: (Platform.OS == 'ios') ? 20 : 0, }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        source={{ uri: 'https://www.youtube.com/embed/' + modalData.video }}
                     />
                     {/* <Video
                        ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                        source={{ uri: modalData.video }}
                        rate={1.0}
                        volume={1.0}
                        isMuted={true}
                        paused={this.state.paused}
                        onEnd={this.onEnd}
                        onLoad={this.onLoad}
                        onLoadStart={this.onLoadStart}
                        onProgress={this.onProgress}
                        resizeMode="content"
                        resizeMode={this.state.screenType}
                        onFullScreen={this.state.isFullScreen}
                        style={{ width: '100%', height: '100%' }}
                     />
                     <MediaControls
                        duration={this.state.duration}
                        isLoading={this.state.isLoading}
                        mainColor="#333"
                        onFullScreen={this.onFullScreen}
                        onPaused={this.onPaused}
                        onReplay={this.onReplay}
                        onSeek={this.onSeek}
                        onSeeking={this.onSeeking}
                        playerState={this.state.playerState}
                        progress={this.state.currentTime}
                     // toolbar={this.renderToolbar()}
                     /> */}
                  </View>
               </View>
            </Modal>




            <View style={{ width: '100%' }}>
               <TouchableOpacity onPress={() => Actions.moreList(this.state.data)}>
                  <Text style={styles.ViewText}>{global.t('View_More')} ></Text>
               </TouchableOpacity>
            </View>
         </View>

      );
   }
}



export default LocationsSelect;


const styles = StyleSheet.create({
   Container: { flex: 1, flexDirection: 'row', },
   titleCenter: { position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', },
   ImgTitle1: {
      color: '#fff', fontSize: 30, fontWeight: 'bold', textShadowColor: 'black',
      textShadowOffset: { width: 1, height: 4 }, textShadowRadius: 5
   },
   ImgTitle: {
      color: '#fff', fontSize: 16, fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
      textShadowRadius: 2, textShadowColor: '#000',
   },
   //Left image
   LeftTopContainer: { height: 168, paddingRight: 8, },
   LeftTopImage: { width: "100%", resizeMode: 'stretch', flex: 1, borderRadius: 3, position: 'relative' },
   LeftBottomContainer: { flex: 1, flexDirection: 'row', height: 40, paddingRight: 8, width: '100%', paddingTop: 8, },
   LeftBottomLeftImage: { width: "50%", },
   LeftBottomLeftImageImg: { width: "100%", resizeMode: 'stretch', height: 80, borderRadius: 3, position: 'relative' },
   LeftBottomRightImage: { width: "50%", alignItems: 'flex-end', paddingLeft: 8, },
   LeftBottomRightImageImg: { width: "100%", resizeMode: 'stretch', height: 80, borderRadius: 3, position: 'relative' },

   TextLeftTop: { position: 'absolute', bottom: 5, right: 5, padding: 3, paddingLeft: 5, backgroundColor: '#de2d3091', color: 'white', },
   TextLeftBottom: {
      position: 'absolute', bottom: 8, textAlign: 'right', paddingLeft: 10,
      paddingRight: 10, backgroundColor: '#de2d3091', color: 'white', right: 0
   },
   TextRight: { position: 'absolute', bottom: 8, right: 0, backgroundColor: '#de2d3091', color: 'white', paddingRight: 10, paddingLeft: 10 },


   //Right image
   RightTopContainer: { flex: 1, flexDirection: 'column', },
   RightImagePadding: { paddingBottom: 8 },
   RightImageImg: { width: "100%", resizeMode: 'stretch', height: 80, borderRadius: 3, },
   //View more
   ViewText: { textAlign: 'center', color: '#de2d30', marginTop: -5, marginBottom: 13 },
   slideBanner: {
      height: 200, shadowColor: '#000', shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8, shadowRadius: 2, elevation: 20, width: '100%'
   },
   bannerwarp: { paddingVertical: 3, position: 'relative' },
   bannertextwarp: { position: 'absolute', top: 10, flexDirection: 'row', padding: 10, },
   bannerSty: { width: '100%', height: '100%', resizeMode: 'cover', },

   imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
   btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },
   imgModal: { width: '100%', height: 400, resizeMode: 'contain', marginTop: 20 },



})

