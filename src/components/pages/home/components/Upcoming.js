import React, { Component } from 'react';
import { Platform, StyleSheet, View, FlatList, TouchableOpacity, ActivityIndicator, Image, ScrollView, Modal, Share } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Card, CardItem, } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import IconFOU from "react-native-vector-icons/Foundation";
import IconANT from "react-native-vector-icons/AntDesign";
import IconENT from "react-native-vector-icons/Entypo";
import { Actions } from 'react-native-router-flux';
import { MonthGetDetialApi } from '../../../../../PostApi';
//import CalendarStrip from "react-native-calendar-strip";
import moment from 'moment';
import 'moment/min/locales';
import CalendarStrip from 'react-native-slideable-calendar-strip';
import { Select, Option } from "react-native-chooser";
import DatePicker from 'react-native-datepicker';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob'
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';



/************************* If Api Data Empty Show Local data *************************/
const localdata = [
  { "airlinelocal": "To Be Confirmed", "statuslocal_en": "Selling Fast", "statuslocal_cn": "热销中" }
]

const selectData = [
  { select_en: 'All', select_cn: '全部', }, { select_en: 'Bookable', select_cn: '可预订', },
]


const locale_cn = {
  name: 'cn',
  config: {
    months: '一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月'.split('_'),
    monthsShort: '1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月'.split('_'),
    weekdays: '星期日_星期一_星期二_星期三_星期四_星期五_星期六'.split('_'),
    weekdaysShort: '日_一_二_三_四_五_六'.split('_'),
    weekdaysMin: '日_一_二_三_四_五_六'.split('_'),
  }
};


const locale_en = {
  name: 'en',
  config: {
    months: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
    monthsShort: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
    weekdays: 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
    weekdaysShort: 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
    weekdaysMin: 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
  }
};

class Upcoming extends Component {

  constructor(props) {
    const selectedDate = moment().format('YYYY-MM-DD');
    
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      dataUnique: [],
      catcherr: null,
      langPrint: '',
      page: 1,
      loadingScroll: false,
      selectedDate: selectedDate,
      resetDate: false,
      //blockDate: [],
      dateMark: [],
      calenderData: [],
      calenderUnique: [],
      calenderTrigger: false,
      selectValue: global.t('All'),
      dataFilter: [],
      calenderFilter: [],
      chosenDate: selectedDate,
      isSelectList: false,
      dataGetModal: [],
      modalData: {
        title: '', cn_title: '', isFavourite: '', tour_code: '', destination: '', price: '',
        departure: '', status: '', status_cn: '', airline: '', itinerary: '', brand_logo: ''
      },
      tokenId: '',
      bookAbleDataCalander: [],
      bookAbleDataNormal: [],
      pressToGo: false,

    }
    IconANT.getImageSource('search1', 16, 'gray').then(
      (source) => this.searchIcon = source
    );

  }


  /*********************************************   componentDidMount  ********************************************/
  componentDidMount() {

    this.setState({ loadingScroll: true }, this.getData);

    AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
      //console.log(asyncStorageRes, '----------------AsyncStorage')
      this.setState({
        tokenId: asyncStorageRes
      })
      this.getDataToken();
    });
    /********************************************* Get Language Store Data From AsyncStorage  ********************************************/
    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes
      })
    })
    this.calenderFetchApi();
    this.calenderLang();
    this.dataApi();

  }

  /**************************************************** getDataToken ****************************************************/
  // getDataToken(access_token = this.state.tokenId, select_date = this.state.modalData.departure) {
  getDataToken = async (access_token = this.state.tokenId, start_date = this.state.modalData.departure, end_date = this.state.modalData.departure) => {
    console.log('start_date----getDataToken', access_token)
    const url = `https://www.goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&departure_date_start=${start_date}&departure_date_end=${end_date}&access_token=${access_token}`;
    fetch(url).then((res) => res.json())
      .then((resJson) => {
        this.setState({
          dataGetModal: resJson,

        })
      })
      .catch((error) => {
        console.error(error);
      });
  }


  /******************************************* selectList *******************************************/
  selectList(tour_code) {
    const modalData = this.state.dataGetModal.find(element => {
      return element.tour_code === tour_code
    })
    console.log('modalData------selectList', modalData)
    this.setState({
      isSelectList: true,
      modalData: {
        title: modalData.title, cn_title: modalData.cn_title, isFavourite: modalData.isFavourite, tour_code: modalData.tour_code,
        destination: modalData.destination, price: modalData.price,departure: modalData.departure,status: modalData.status,
        status_cn: modalData.status_cn, airline: modalData.airline, itinerary: modalData.itinerary, brand_logo: modalData.brand_logo
      }
    });
  }

  /******************************************* selectListCalender *******************************************/
  selectListCalender(tour_code) {
    const modalData = this.state.calenderData.find(element => {
      return element.tour_code === tour_code
    })
    console.log('modalData------selectListCalender', modalData)
    this.setState({
      isSelectList: true,
      modalData: {
        title: modalData.title, cn_title: modalData.cn_title, isFavourite: modalData.isFavourite, tour_code: modalData.tour_code,
        destination: modalData.destination, price: modalData.price, departure: modalData.departure, status: modalData.status,
        status_cn: modalData.status_cn, airline: modalData.airline, itinerary: modalData.itinerary, brand_logo: modalData.brand_logo
      }
    }, this.getDataToken);
  }

  /************************************ addFavorite function ***********************************/
  addFavorite(index, tourcode = this.state.modalData.tour_code, access_token = this.state.tokenId) {
    console.log(tourcode, 'tourcode', access_token, 'addFavorite---add')
    const items = this.state.dataGetModal
    const modalAdd = this.state.modalData
    let obj = this
    let url = `https://www.goldendestinations.com/api/new/like.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

    fetch(url).then((res) => res.json())
      .then(function (resJson) {
        console.log(resJson, '-----------resJson add');
        if (resJson[0].status === 1) {
          console.log(resJson, 'ok-------------add');
          foundItem = items.findIndex(x => x.tour_code == tourcode)
          modalAdd.isFavourite = "true"
          items[foundItem] = modalAdd
          console.log(items, 'items')
          obj.setState({
            dataGetModal: items,
            calenderData: items,
            modalData: modalAdd,
          })
        }
        else if (access_token === null) {
          alert(global.t('pls_login_add'))
        }
        else {
          alert(resJson[0].error)

        }
      })


    console.log('add')
  }

  /************************************ removeFavorite function ***********************************/
  removeFavorite(index, tourcode = this.state.modalData.tour_code, access_token = this.state.tokenId, ) {
    console.log(tourcode, 'tourcode', access_token, 'removeFavorite---remove')
    const items = this.state.dataGetModal
    const modalRemove = this.state.modalData
    let obj = this
    let url = `https://www.goldendestinations.com/api/new/unlike.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

    fetch(url).then((res) => res.json())
      .then(function (resJson) {
        console.log(resJson, '-----------resJson remove');
        if (resJson[0].status === 1) {
          console.log(resJson, 'remove-------------remove');
          foundItem = items.findIndex(x => x.tour_code == tourcode)
          modalRemove.isFavourite = "false"
          items[foundItem] = modalRemove
          console.log(items, 'items')
          obj.setState({
            dataGetModal: items,
            calenderData: items,
            modalData: modalRemove
          })
        }
        else {
          alert(resJson[0].error)
        }
      })


    console.log('remove')
  };


  /*************************************** Download PDF File ***************************************/
  downloadFile() {
    function getLocalPath(url) {
      const filename = url.split('/').pop();
      return `${RNFS.DocumentDirectoryPath}/${filename}`;
    }

    const url = this.state.modalData.itinerary;
    const localFile = getLocalPath(url);

    const options = {
      fromUrl: url,
      toFile: localFile
    };
    RNFS.downloadFile(options).promise
      .then(() => FileViewer.open(localFile))
      .then(() => {
        // success
      })
      .catch(error => {
        // error
      });
  }


  /************************************ sharing function ***********************************/
  sharing() {
    const { langPrint, modalData } = this.state;
    console.log('sharing');
    Share.share(
      {
        message: langPrint === 'zh' ?
          `${global.t('Destination')}: ${modalData.cn_title}` + "\n" +
          `${global.t('Tour_Code')}: ${modalData.tour_code}` + "\n" +
          `${global.t('Departure')}: ${modalData.departure}` + "\n" +
          `${global.t('Airline')}: ${modalData.airline === null ? localdata[0].airlinelocal_cn : modalData.airline}` + "\n" +
          `${global.t('Status')}: ${modalData.status_cn === "" ? localdata[0].statuslocal_cn : modalData.status_cn}` + "\n" +
          `${global.t('Price')}: RM ${modalData.price}` + "\n" +
          `${global.t('Itinerary')}: ${modalData.itinerary}`
          :
          `Destination: ${modalData.title}` + "\n" +
          `Tourcode: ${modalData.tour_code}` + "\n" +
          `Departure: ${modalData.departure}` + "\n" +
          `Airline: ${modalData.airline === null ? localdata[0].airlinelocal_en : modalData.airline}` + "\n" +
          `Status: ${modalData.status === "" ? localdata[0].statuslocal_en : modalData.status}` + "\n" +
          `Price: RM ${modalData.price}` + "\n" +
          `Itinerary: ${modalData.itinerary}`
        ,
        title: 'Golden Destinations',
        url: modalData.itinerary
      }
    )
  }

  /**************************************************** get first data display ****************************************************/
  getData = async (page = this.state.page, access_token = this.state.tokenId, ) => {
    const url = `https://www.goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&limit=30&page=${page}&access_token=${access_token}`;
    setTimeout(() => {
      fetch(url).then((res) => res.json())
        .then((resJson) => {
          let uniqData = []
          resJson.map(x => uniqData.filter(a => a.departure == x.departure).length > 0 ? null : uniqData.push(x));
          this.setState({
            data: this.state.data.concat(uniqData),
            // data: [...this.state.data, ...uniqData],
            //data: this.state.page === 1 ? Array.from(resJson) : [...this.state.data, ...uniqData],
            isLoading: false,
            loadingScroll: false,
            //blockDate: resJson.map((item) => { return item.departure }),
          })
          // this.setState((prevState, nextProps) => ({
          //   data:
          //     this.state.page === 1
          //       ? Array.from(resJson)
          //       : [...this.state.data, ...resJson],
          //       isLoading: false,
          //       loadingScroll: false
          // }));

        })
        .catch((error) => {
          console.error(error);
        });
    }, 2000)
  }

  /******************************************* get api dateMark data for calender *******************************************/
  dataApi() {
    let url = 'https://www.goldendestinations.com/api/new/date_tour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&departure_date_start=2019-10-01&departure_date_end=2020-10-01'
    fetch(url).then((res) => res.json())
      .then((fetchDate) => {
        this.setState({
          dateMark: fetchDate.departure_dates
        })
      })
  }

  /******************************************* get api calender start date to end date *******************************************/
  calenderFetchApi(start_date = this.state.chosenDate, end_date = this.state.chosenDate, access_token = this.state.tokenId, ) {
    console.log(start_date, end_date, 'fetchData')
    let url = `https://www.goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&departure_date_start=${start_date}&departure_date_end=${end_date}&access_token=${access_token}`;
    console.log(url, 'url')

    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        //console.log(fetchData, 'fetchData');
        // let calenderUnique = []
        // fetchData.map(x => calenderUnique.filter(a => a.departure == x.departure).length > 0 ? null : calenderUnique.push(x));
        let bookUnique = fetchData.filter(ele => { return ele.status !== 'Sold Out' })
        //console.log(bookUnique, 'bookUnique----------');
        this.setState({
          calenderData: fetchData,
          bookAbleDataCalander: bookUnique
        })
      })
  }

  /*************************************************** Select Option status ***************************************************/
  onSelect(value) {
    console.log(value, 'value-----------');
    dataFilter = this.state.dataUnique.filter(element => {
      return element.status !== 'Sold Out'
    }),
      calenderFilter = this.state.calenderData.filter(element => {
        return element.status !== 'Sold Out'
      }),

      this.setState({
        selectValue: value,
        dataFilter: dataFilter,
        calenderFilter: calenderFilter

      });
  }

  /******************************************* calenderLang *******************************************/
  calenderLang() {
    if (this.state.langPrint === 'zh') {
      moment.locale('zh-cn')
    } else {
      moment.locale('en')
    }
  }

  /******************************************* handleLoadMore *******************************************/
  handleLoadMore = () => {
    console.warn('handleLoadMore');
    // const pages = this.state.page + 1
    // if (pages < 25) {
    //   this.setState({
    //     page: pages,
    //     loadingScroll: true
    //   }, this.getData)

    // }

    this.setState({
      page: this.state.page + 1,
      loadingScroll: true
    }, this.getData)

  }

  /******************************************* renderFooter *******************************************/
  renderFooter = () => {
    return (
      this.state.calenderTrigger ? null :
        <TouchableOpacity onPress={this.handleLoadMore}>
          <View style={[styles.loadingFooter]}>
            {
              this.state.loadingScroll ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                  <ActivityIndicator color="#de2d30" />
                </View>
                : null
            }
            <Text style={styles.TextMore}>{global.t('View_More')}<IconANT name='right' size={15} /></Text>
          </View>
        </TouchableOpacity>
    )
  }


  /******************************************* ListEmpty *******************************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
      </View>
    );
  };


  /****************************************** dateSelected *******************************************/
  dateSelected(dateString) {
    console.log(dateString, 'dateSelected')
    this.setState({
      selectedDate: dateString
    })
  }


  /******************************************* Key Flatlist *******************************************/
  keyExtractor = (item, index) => item.key;


  /******************************************* RenderItem in Flatlist *******************************************/
  renderItem = ({ item, index }) => {
    const { langPrint } = this.state;
    return (
      <ListItem thumbnail>
        <Left>
          <View>
            {/* <View style={{ width: '100%', alignItems: 'center', marginTop: -5, marginBottom: 8 }}>
              <Image source={brand2} style={{ width: 30, height: 30 }} />
            </View> */}

            <View style={{ position: 'relative', }}>
              <Thumbnail style={{ height: 70, width: 70 }}
                square source={require('../../../../assets/images/background/iconbgred.png')}
              />
              <View style={styles.ThumbnailTextContainer}>
                <Text style={styles.ThumbnailText1}>{item.departure}</Text>
              </View>
            </View>
          </View>
        </Left>
        <Body>
          <TouchableOpacity onPress={() => this.selectList(item.tour_code)}>
            {/* <TouchableOpacity onPress={() => Actions.viewSelectList({data: item })}> */}
            <Text style={styles.TitleList}>{langPrint === 'zh' ? item.cn_title : item.title}</Text>
          </TouchableOpacity>
          <Text note>{global.t('Tour_Code')}:
            <Text style={styles.titlecolor}> {item.tour_code}</Text>
          </Text>
          <Text note >{global.t('Airline')}:
            <Text style={styles.titlecolor}> {item.airline === null ? localdata[0].airlinelocal : item.airline}</Text>
          </Text>
          <Text note >{global.t('Status')}:
          {
              langPrint === "zh" ? (
                <Text style={[styles.titlecolor, { fontWeight: 'bold', color: '#ff9601' }]}>
                  {item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}
                </Text>
              ) : (
                  <Text style={[styles.titlecolor, { fontWeight: 'bold', color: '#ff9601' }]}>
                    {item.status === "" ? localdata[0].statuslocal_en : item.status}</Text>
                )
            }
          </Text>
          {/* <Text note >{global.t('Departure')}:
            <Text style={styles.titlecolor}> {item.departure}</Text>
          </Text> */}
          <View style={{ flexDirection: 'row', width: '100%' }}>
            <View style={{ width: '50%' }}>
              <Text note >{global.t('Price')}:
                <Text style={styles.price}> RM {item.price}</Text>
              </Text>
            </View>

            <View style={{ width: '50%' }}>
              <View style={{ alignItems: 'flex-end', marginTop: -45, marginBottom: 19, paddingRight: 30 }}>
                <Image source={{ uri: item.brand_logo }} style={{ width: 25, height: 25 }} />
              </View>
              <TouchableOpacity onPress={() => Actions.upcomingdetail({ data: item })} >
                <Text style={styles.viewmore} uppercase={false}>{global.t('More')} ></Text>
              </TouchableOpacity>
            </View>

          </View>

        </Body>
        {/* <Right style={{ paddingRight: 0 }}>
          <Button transparent>
            <TouchableOpacity onPress={() => Actions.upcomingdetail({ data: item })}>
              <Text style={{ color: '#de2d30', paddingRight: 10, paddingLeft: 8 }} uppercase={false}>{global.t('View_More')} ></Text>
            </TouchableOpacity>
          </Button>
        </Right> */}
      </ListItem>
    )
  }


  /******************************************* dateRenderItem in Flatlist *******************************************/
  dateRenderItem = ({ item, index }) => {
    const { langPrint } = this.state;
    return (
      <ListItem thumbnail>
        <Left>
          <View>
            {/* <View style={{ width: '100%', alignItems: 'center', marginTop: -5, marginBottom: 8 }}>
              <Image source={brand2} style={{ width: 30, height: 30 }} />
            </View> */}

            <View style={{ position: 'relative', }}>
              <Thumbnail style={{ height: 70, width: 70 }}
                square source={require('../../../../assets/images/background/iconbgred.png')}
              />
              <View style={styles.ThumbnailTextContainer}>
                <Text style={styles.ThumbnailText1}>{item.departure}</Text>
              </View>
            </View>
          </View>
        </Left>
        <Body>
          <TouchableOpacity onPress={() => this.selectListCalender(item.tour_code)}>
            <Text style={styles.TitleList}>{langPrint === 'zh' ? item.cn_title : item.title}</Text>
          </TouchableOpacity>
          <Text note>{global.t('Tour_Code')}:
            <Text style={styles.titlecolor}> {item.tour_code}</Text>
          </Text>
          <Text note >{global.t('Airline')}:
            <Text style={styles.titlecolor}> {item.airline === null ? localdata[0].airlinelocal : item.airline}</Text>
          </Text>
          <Text note >{global.t('Status')}:
          {
              langPrint === "zh" ? (
                <Text style={[styles.titlecolor, { fontWeight: 'bold', color: '#ff9601' }]}>
                  {item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}
                </Text>
              ) : (
                  <Text style={[styles.titlecolor, { fontWeight: 'bold', color: '#ff9601' }]}>
                    {item.status === "" ? localdata[0].statuslocal_en : item.status}</Text>
                )
            }
          </Text>
          {/* <Text note >{global.t('Departure')}:
            <Text style={styles.titlecolor}> {item.departure}</Text>
          </Text> */}
          <View style={{ flexDirection: 'row', width: '100%' }}>
            <View style={{ width: '50%' }}>
              <Text note >{global.t('Price')}:
                <Text style={styles.price}> RM {item.price}</Text>
              </Text>
            </View>
            <View style={{ width: '50%' }}>
              <View style={{ alignItems: 'flex-end', marginTop: -45, marginBottom: 19, paddingRight: 30 }}>
                <Image source={{ uri: item.brand_logo }} style={{ width: 25, height: 25 }} />
              </View>
              <TouchableOpacity onPress={() => Actions.datelist({ data: item })} >
                <Text style={styles.viewmore} uppercase={false}>{global.t('More')} ></Text>
              </TouchableOpacity>
            </View>

          </View>
          {/* <View style={{ flexDirection: 'row', width: '100%' }}>
            <View style={{ width: '50%' }}>
              <Text note >{global.t('Price')}:
                <Text style={styles.price}> RM {item.price}</Text>
              </Text>
            </View>
            <TouchableOpacity onPress={() => Actions.datelist({ data: item })} style={{ width: '50%' }}>
              <Text style={styles.viewmore} uppercase={false}>{global.t('More')} ></Text>
            </TouchableOpacity>
          </View> */}

        </Body>
        {/* <Right style={{ paddingRight: 0 }}>
          <Button transparent>
            <TouchableOpacity onPress={() => Actions.upcomingdetail({ data: item })}>
              <Text style={{ color: '#de2d30', paddingRight: 10, paddingLeft: 8 }} uppercase={false}>{global.t('View_More')} ></Text>
            </TouchableOpacity>
          </Button>
        </Right> */}
      </ListItem>
    )
  }

  render() {

    const { dataUnique, selectedDate, data, isLoading, catcherr, langPrint, resetDate, dateMark, calenderData, isSelectList,
      calenderTrigger, selectValue, dataFilter, calenderFilter, chosenDate, modalData, bookAbleDataCalander, bookAbleDataNormal, pressToGo } = this.state;

    //console.log(this.state.data, 'render----------data')
    /************************* filter same date list *************************/
    const unique = dataUnique;
    data.map(x => unique.filter(a => a.departure == x.departure).length > 0 ? null : unique.push(x));

    let bookDataFilter = unique.filter(ele => { return ele.status !== 'Sold Out' });
    //console.log('bookDataFilter=========',bookDataFilter)


    /************************* find match select date list *************************/
    // const dataSelectDate = selectedDate;
    // const resData = unique.filter(i => dataSelectDate.includes(i.departure));

    /************************* find departure mark date in list *************************/
    const markDate = data.map((item) => { return item.departure })

    // console.log(unique, 'unique');
    //console.log(resData, 'resData');
    console.log(selectedDate, 'selectedDate');
    //console.log(markDate, 'blockDate');
    console.log(modalData, 'modalData');
    console.log('dataGetModal', this.state.dataGetModal)
    console.log('depature modal -----', this.state.modalData.departure)
    console.log('pressToGo----', pressToGo)

    // console.log(bookAbleDataCalander, 'bookAbleDataCalander-----')
    // console.log(bookAbleDataNormal, 'bookAbleDataNormal-----')

    // console.log('calenderData-----', calenderData)


    // const markedDates = [
    //   Object.keys(markDate).map(function (key) {
    //     let data = markDate[key];
    //     data = data;
    //     return {
    //       date: moment(data, "DD/MM/YYYY").format('YYYY-MM-DD'),
    //       //dots: [{ key: 10, color: 'red', selectedDotColor: 'blue' }],
    //     }
    //   })
    // ]

    // console.log(markedDates, 'markedDates');



    return (
      <View>
        {/* <Text>{catcherr ? catcherr : 'no error'}</Text>  */}
        {/* <View style={[styles.TitleTextWarp, { position: 'relative' }]}> */}
        <View style={[styles.TitleTextWarp,]}>
          <Text style={styles.TitleText}>{global.t('Upcoming_Departure')}</Text>

          {/**************************** date have icon select  **************************/}

          <View style={styles.datepickerWarp}>
            <DatePicker
              style={{ width: '100%', alignItems: 'flex-end', paddingRight: 8 }}
              date={this.state.chosenDate}
              mode="date"
              // placeholder="select date"
              hideText={true}
              format="YYYY-MM-DD"
              confirmBtnText={global.t('Confirm')}
              cancelBtnText={global.t('Cancel')}
              showIcon={true}
              iconComponent={<IconANT name='search1' style={{ fontSize: Platform.OS === 'ios' ? 15 : 12 }} color={'gray'} />}
              customStyles={{
                dateIcon: { position: 'absolute', marginLeft: 0, width: 16, height: 16, },
                dateInput: { marginLeft: 0, borderWidth: 0, alignItems: 'flex-end', },
                placeholderText: { color: 'gray', },
              }}
              // onDateChange={(date) => { this.setState({ chosenDate: date, calenderTrigger: true}) }}
              onDateChange={(date) => {
                this.setState({
                  chosenDate: date,
                  calenderTrigger: true,
                  selectValue: selectValue
                }, () => { this.calenderFetchApi() });
              }}
            />
          </View>

          {/**************************** date swipe select  **************************/}
          <CalendarStrip
            selectedDate={chosenDate}
            showWeekNumber={true}
            onPressDate={(date) => {
              this.setState({
                chosenDate: moment(date).format('YYYY-MM-DD'),
                calenderTrigger: true,
                selectValue: selectValue,
                pressToGo: false,
              }, () => { this.calenderFetchApi() });
            }}
            onPressGoToday={(today) => {
              console.log('onPressGoToday---------------')
              this.setState({
                chosenDate: moment(today).format('YYYY-MM-DD'),
                calenderTrigger: false,
                selectValue: selectValue,
                pressToGo: true,
              });
            }}
            // onSwipeDown={() => {
            //   alert('onSwipeDown');
            // }}
            markedDate={dateMark}
            weekStartsOn={1}
            style={{ color: 'red' }}
          />
        </View>

        <ScrollView >
          <View style={{ paddingBottom: 400 }}>
            {
              isLoading ? (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                  <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                  <ActivityIndicator size="large" color="#de2d30" />
                </View>
              ) : (
                  calenderTrigger === true ? (
                    <View>
                      {/**************************** when select calender display data  **************************/}
                      <Select
                        onSelect={this.onSelect.bind(this)}
                        defaultText={(<Text>
                          <Text style={{ color: '#de2d30', }}>{selectValue}   </Text>
                          <IconANT name='filter' style={{ fontSize: 15, color: '#de2d30', }} />
                        </Text>)}
                        style={styles.selectSty}
                        textStyle={styles.selectText}
                        backdropStyle={{ backgroundColor: "#00000078" }}
                        optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                        animationType='none'
                        transparent={true}
                        indicator='none'
                      // indicatorColor='#de2d30'
                      // indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                      // indicatorStyle={{ marginTop: Platform.OS === 'ios' ? -15 : -15, marginLeft: -10 }}
                      >
                        {
                          selectData.map((item) => {
                            return (
                              <Option value={langPrint === "zh" ? item.select_cn : item.select_en} >
                                {langPrint === "zh" ? item.select_cn : item.select_en}
                                {/* {item.select_en} */}
                              </Option>
                            )
                          })
                        }
                      </Select>
                      {/**************************** select status data  **************************/}

                      <FlatList
                        data={selectValue === global.t('Bookable') ? bookAbleDataCalander : calenderData}
                        //ref={(ref) => { this.flatListRef = ref; }}
                        renderItem={this.dateRenderItem}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                        // onEndReached={this.handleLoadMore}
                        // onEndReachedThreshold={0}
                        ListFooterComponent={this.renderFooter}
                        ListEmptyComponent={this.ListEmpty}
                      //initialNumToRender={10}
                      //bounces={false}
                      //ListFooterComponent={this.renderFooter.bind(this)}
                      // contentContainerStyle={{
                      //   paddingBottom: 500
                      // }}
                      />

                    </View>
                  ) : (
                      <View>
                        {/**************************** when no select calender display data   **************************/}
                        <Select
                          onSelect={this.onSelect.bind(this)}
                          defaultText={(<Text>
                            <Text style={{ color: '#de2d30', }}>{selectValue}   </Text>
                            <IconANT name='filter' style={{ fontSize: 15, color: '#de2d30', }} />
                          </Text>)}
                          style={styles.selectSty}
                          textStyle={styles.selectText}
                          backdropStyle={{ backgroundColor: "#00000078" }}
                          optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                          animationType='none'
                          transparent={true}
                          indicator='none'
                        // indicatorColor='#de2d30'
                        // indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                        // indicatorStyle={{ marginTop: Platform.OS === 'ios' ? -15 : -15, marginLeft: -10 }}
                        >
                          {
                            selectData.map((item) => {
                              return (
                                <Option value={langPrint === "zh" ? item.select_cn : item.select_en} >
                                  {langPrint === "zh" ? item.select_cn : item.select_en}
                                </Option>
                              )
                            })
                          }
                        </Select>
                        {/**************************** select status data  **************************/}
                        <FlatList
                          data={selectValue === global.t('Bookable') ? bookDataFilter : unique}
                          //ref={(ref) => { this.flatListRef = ref; }}
                          renderItem={this.renderItem}
                          extraData={this.state}
                          keyExtractor={(item, index) => index.toString()}
                          // onEndReached={this.handleLoadMore}
                          // onEndReachedThreshold={0}
                          ListFooterComponent={this.renderFooter}
                        //initialNumToRender={10}
                        //bounces={false}
                        //ListFooterComponent={this.renderFooter.bind(this)}
                        // contentContainerStyle={{
                        //   paddingBottom: 500
                        // }}
                        />
                      </View>

                    )

                )
            }
          </View>

          {/* 
          <TouchableOpacity onPress={() => this.btn()}>
            <Text style={styles.TextMore}> top <IconANT name='right' size={15} /></Text>
          </TouchableOpacity> */}
          {/* <TouchableOpacity onPress={() => Actions.gdSeries()}>
            <Text style={styles.TextMore}> {global.t('More')} <IconANT name='right' size={15} /></Text>
          </TouchableOpacity> */}


          <Modal
            transparent={true}
            animationType={'fade'}
            visible={isSelectList}
            onRequestClose={() => { console.log('close modal') }}>
            <View style={styles.modalBackground}>
              <TouchableOpacity onPress={() => this.setState({ isSelectList: false })} style={styles.btnClose}>
                <IconANT name="close" size={30} style={{ color: '#fff' }} />
              </TouchableOpacity>

              <View style={{ width: '90%', borderRadius: 5, backgroundColor: '#fff' }}>
                <Card>
                  <CardItem style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#fcf5f5', marginRight: 0 }}>
                    <Body style={{ marginRight: 0, }}>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ alignItems: 'flex-start', width: '5%', }}>
                          <Image source={{ uri: modalData.brand_logo }} style={{ width: 20, height: 20 }} />
                        </View>
                        <View style={{ width: '95%' }}>
                          <Text note style={{ fontWeight: 'bold', textAlign: 'center', marginLeft: 5 }}>
                            {langPrint === 'zh' ? modalData.cn_title : modalData.title}
                          </Text>
                        </View>
                      </View>
                    </Body>
                  </CardItem>
                  <CardItem cardBody >
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15 }}>
                        <View style={styles.textWarp}>
                          <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Tour_Code')}: </Text>
                          <Text note style={styles.TextInner2}>{modalData.tour_code}</Text>
                        </View>
                        <View style={styles.textWarp}>
                          <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Airline')}: </Text>
                          {
                            langPrint === 'zh' ? (
                              <Text note style={styles.TextInner2}>{modalData.airline === null ? localdata[0].airlinelocal_cn : modalData.airline}</Text>
                            ) : (
                                <Text note style={styles.TextInner2}>{modalData.airline === null ? localdata[0].airlinelocal_en : modalData.airline}</Text>
                              )
                          }
                        </View>
                        <View style={styles.textWarp}>
                          <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Status')}: </Text>
                          {
                            langPrint === "zh" ? (
                              <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                {modalData.status_cn === "" ? localdata[0].statuslocal_cn : modalData.status_cn}
                              </Text>
                            ) : (
                                <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                  {modalData.status === "" ? localdata[0].statuslocal_en : modalData.status}
                                </Text>
                              )
                          }
                        </View>
                        <View style={styles.textWarp}>
                          <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Departure')}: </Text>
                          <Text note style={styles.TextInner2}>{modalData.departure}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                          <Text note style={{ width: '30%', color: '#00953b', marginTop: 2 }}>{global.t('Price')}: </Text>
                          <Text note style={styles.Textprice}>RM {modalData.price}</Text>
                        </View>
                      </View>

                    </View>
                  </CardItem>
                  <CardItem style={{ borderTopWidth: .5, borderTopColor: '#ccc' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ width: '33.33%', }}>
                        <TouchableOpacity onPress={modalData.isFavourite === "true" ? this.removeFavorite.bind(this) : this.addFavorite.bind(this)}>
                          <IconFOU name="star" size={20} style={{
                            textAlign: 'center',
                            color: modalData.isFavourite === "true" ? "#ffc43f" : "#ccc"
                          }} />
                        </TouchableOpacity>
                      </View>
                      <View style={{ width: '33.33%', borderLeftWidth: .5, borderLeftColor: '#ccc', borderRightColor: '#ccc', borderRightWidth: .5 }}>
                        <TouchableOpacity onPress={this.sharing.bind(this)} >
                          <IconFOU name="share" size={20} style={{ textAlign: 'center', color: '#0095ff' }} />
                        </TouchableOpacity>
                      </View>
                      <View style={{ width: '33.33%' }}>
                        <TouchableOpacity onPress={this.downloadFile.bind(this)}>
                          <IconFOU name="download" size={20} style={{ textAlign: 'center', color: '#de2d30' }} />
                        </TouchableOpacity>
                      </View>
                    </View>

                  </CardItem>
                </Card>
              </View>
            </View>
          </Modal>




        </ScrollView>
      </View>
    );
  }
}

export default Upcoming;

const styles = StyleSheet.create({

  TitleTextWarp: {
    borderBottomWidth: 0.5,
    ...Platform.select({
      ios: { color: 'black', borderColor: '#c9c9c9', },
      android: { color: 'black', borderColor: '#c9c9c9', }
    })
  },
  TitleText: { textAlign: 'center', fontSize: 18, fontWeight: 'bold', paddingTop: 15, paddingBottom: 10 },
  ThumbnailTextContainer: { position: 'absolute', width: '100%', textAlign: 'center', },
  ThumbnailText1: {
    color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', padding: 7,
    ...Platform.select({
      ios: { paddingTop: 15, },
      android: { paddingTop: 15, }
    }),

  },
  ThumbnailText2: {
    color: 'white', fontWeight: 'bold',
    ...Platform.select({
      android: { marginTop: -4 }
    }),
  },
  TitleList: { fontWeight: 'bold', marginBottom: 5, color: '#606c77' },
  TextMore: { paddingBottom: 20, textAlign: 'center', padding: 20, paddingTop: 5, color: '#de2d30', alignItems: 'center', justifyContent: 'center' },
  TouchableOpacity_Inside_Text: { padding: 30, textAlign: 'center', color: '#de2d30' },
  viewmore: { color: '#de2d30', paddingRight: 10, paddingLeft: 8, textAlign: 'right', fontSize: 14, },
  titlecolor: { color: '#606c77', fontSize: 14 },
  price: { color: 'green', fontSize: 14, width: '50%' },
  loadMoreBtn: {
    padding: 20, borderRadius: 0, flexDirection: 'row',
    justifyContent: 'center', alignItems: 'center',
  },
  btnText: { color: '#de2d30', fontSize: 15, textAlign: 'center' },
  loadingFooter: { marginTop: 10, alignItems: 'center' },
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 30, marginBottom: 20 },
  selectSty: { borderWidth: 0, width: '100%', backgroundColor: '#f7f7f7', },
  selectText: { color: '#de2d30', textAlign: 'center', width: '100%', fontWeight: 'bold', },
  datepickerWarp: {
    width: '20%', paddingRight: 10, right: 40, paddingLeft: 10, alignItems: 'flex-end', position: 'absolute',
    top: 39, zIndex: 2, paddingTop: 8,
    ...Platform.select({
      ios: { top: 36, },
      android: { top: 39, }
    })
  },
  modalBackground: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  // activityIndicatorWrapper: {
  //   backgroundColor: '#FFFFFF', height: 300, width: '90%', borderRadius: 10, display: 'flex',
  //   alignItems: 'center', justifyContent: 'space-around'
  // },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 40, right: 20, width: '50%' },

  IconColor: { color: '#de2d30', textAlign: 'center', paddingTop: 30, },
  TextTitle: { color: '#de2d30', fontWeight: 'bold', },
  TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
  TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, marginTop: 2 },
  Textprice: { color: '#00953b', width: '35%', marginTop: 2 },
  textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
  textWarp: { flexDirection: 'row', marginTop: 2, },
  downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
  dwnwarp: { width: '100%', },
  bodystyle: {
    width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
    paddingTop: 0, borderBottomWidth: 0
  },
  texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
  dwntext: { marginTop: 15, textAlign: 'center', marginRight: 0, color: '#de2d30' }



})

