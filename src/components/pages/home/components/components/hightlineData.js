import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, ActivityIndicator, Share } from 'react-native';
import { Toast, Content, Card, CardItem, Text, Body } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconFOU from "react-native-vector-icons/Foundation";
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';


/************************* If Api Data Empty Show Local data *************************/
const localdata = [
   { "airlinelocal_en": "To Be Confirmed", "airlinelocal_cn": "待确认", "statuslocal_en": "Selling Fast", "statuslocal_cn": "热销中" }
]



class HightLineData extends Component {
   constructor(props) {
      super(props);
      this.state = {
         isLoading: true,
         data: [],
         langPrint: ''
      }

   }

   componentDidMount() {

      /************************************ Get Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
         console.log(asyncStorageRes, '----------------AsyncStorage')
         this.setState({
            tokenId: asyncStorageRes
         })

      });

      /************************************ Get Language Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem("language").then(langStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Note')
         this.setState({
            langPrint: langStorageRes
         })
         this.getData();
      })



   }


   getData(keyword = this.props.keyword_id, tourcode = this.props.code_id, access_token = this.state.tokenId, ) {
      console.log(keyword, tourcode, access_token, 'getdata--------------')
      let url = `https://goldendestinations.com/api/new/tour_highlights_detail.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&keyword=${keyword}&tourcode=${tourcode}&access_token=${access_token}`;
      const obj = this;
      console.log(url, 'url--------------')
      fetch(url).then((res) => res.json())
         .then(function (myJson) {
            //console.log(myJson, '------myJson')
            obj.setState({
               data: myJson,
               isLoading: false
            })
         })
   }


   /************************************ sharing function ***********************************/
   sharing(item) {
      const { langPrint } = this.state;
      console.log('sharing');
      Share.share(
         {
            message: langPrint === 'zh' ?
               `${global.t('Destination')}: ${item.cn_title}` + "\n" +
               `${global.t('Tour_Code')}: ${item.tour_code}` + "\n" +
               `${global.t('Departure')}: ${item.departure}` + "\n" +
               `${global.t('Airline')}: ${item.airine === null ? localdata[0].airlinelocal_cn : item.airine}` + "\n" +
               `${global.t('Status')}: ${item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}` + "\n" +
               `${global.t('Price')}: RM ${item.price}` + "\n" +
               `${global.t('Itinerary')}: ${item.itinerary}`
               :
               `Destination: ${item.title}` + "\n" +
               `Tourcode: ${item.tour_code}` + "\n" +
               `Departure: ${item.departure}` + "\n" +
               `Airline: ${item.airine === null ? localdata[0].airlinelocal_en : item.airine}` + "\n" +
               `Status: ${item.status === "" ? localdata[0].statuslocal_en : item.status}` + "\n" +
               `Price: RM ${item.price}` + "\n" +
               `Itinerary: ${item.itinerary}`
            ,
            title: 'Golden Destinations',
            url: item.itinerary
         }
      )
   }

   /************************************ addFavorite function ***********************************/
   addFavorite(item, index, tourcode = item.tour_code, access_token = this.state.tokenId) {
      console.log(tourcode, 'tourcode', access_token, 'addFavorite---add')
      let items = this.state.data
      let obj = this;
      let url = `https://www.goldendestinations.com/api/new/like.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;
      console.log(url, 'url--------------add')
      fetch(url).then((res) => res.json())
         .then(function (resJson) {
            console.log(resJson, '-----------resJson add');
            if (resJson[0].status === 1) {
               console.log(resJson, 'ok-------------add')
               foundItem = items.findIndex(x => x.tour_code === tourcode);
               item.isFavourite = "true";
               items[foundItem] = item;
               console.log(items, 'items')
               obj.setState({
                  data: items
               })
            }
            else if (access_token === null) {
               alert(global.t('pls_login_add'))
            }
            else {
               alert(resJson[0].error)
            }
         })

      console.log('add')
   }

   /************************************ removeFavorite function ***********************************/
   removeFavorite(item, index, tourcode = item.tour_code, access_token = this.state.tokenId, ) {
      console.log(tourcode, 'tourcode', access_token, 'removeFavorite---remove')
      let items = this.state.data;
      let obj = this;
      let url = `https://www.goldendestinations.com/api/new/unlike.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;
      fetch(url).then((res) => res.json())
         .then(function (resJson) {
            console.log(resJson, '-----------resJson remove');
            if (resJson[0].status === 1) {
               console.log(resJson, 'remove-------------remove');
               foundItem = items.findIndex(x => x.tour_code === tourcode);
               item.isFavourite = "false";
               items[foundItem] = item;
               console.log(items, 'items');
               obj.setState({
                  data: items
               })
            }
            else {
               alert(resJson[0].error)
            }
         })

      console.log('remove')
   };




   keyExtractor = (item) => item.key;

   /************************* renderItem *************************/
   renderItem = ({ item }) => {
      console.log(item.isFavourite, '--------item------props')
      const { langPrint } = this.state;

      return (
         <Card >
            <CardItem style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#fcf5f5', marginRight: 0 }}>
               <Body style={{ marginRight: 0, }}>
                  <View style={{ flexDirection: 'row' }}>
                     <View style={{ alignItems: 'flex-start', width: '5%', }}>
                        <Image source={{uri: item.brand_logo}} style={{ width: 20, height: 20 }} />
                     </View>
                     <View style={{ width: '95%' }}>
                        <Text note style={{ fontWeight: 'bold', textAlign: 'center', marginLeft: 5 }}>
                           {langPrint === 'zh' ? item.cn_title : item.title}
                        </Text>
                     </View>

                  </View>
               </Body>
            </CardItem>
            <CardItem cardBody >
               <View style={{ flexDirection: 'row' }}>
                  <View style={{ paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15 }}>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Tour_Code')}: </Text>
                        <Text note style={styles.TextInner2}>{item.tour_code}</Text>
                     </View>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Airline')}: </Text>
                        {
                           langPrint === 'zh' ? (
                              <Text note style={styles.TextInner2}>{item.airine === null ? localdata[0].airlinelocal_cn : item.airine}</Text>
                           ) : (
                                 <Text note style={styles.TextInner2}>{item.airine === null ? localdata[0].airlinelocal_en : item.airine}</Text>
                              )
                        }
                     </View>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Status')}: </Text>
                        {
                           langPrint === "zh" ? (
                              <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                 {item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}
                              </Text>
                           ) : (
                                 <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                    {item.status === "" ? localdata[0].statuslocal_en : item.status}
                                 </Text>
                              )
                        }
                     </View>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Departure')}: </Text>
                        <Text note style={styles.TextInner2}>{item.departure}</Text>
                     </View>

                     <View style={{ flexDirection: 'row', marginTop: 2 }}>
                        <Text note style={{ width: '30%', color: '#00953b', marginTop: 2 }}>{global.t('Price')}: </Text>
                        <Text note style={styles.Textprice}>RM {item.price}</Text>
                     </View>
                  </View>

               </View>
            </CardItem>
            <CardItem style={{ borderTopWidth: .5, borderTopColor: '#ccc' }}>
               <View style={{ flexDirection: 'row' }}>
                  <View style={{ width: '33.33%', }}>
                     <TouchableOpacity onPress={item.isFavourite === "true" ? this.removeFavorite.bind(this, item) : this.addFavorite.bind(this, item)}>
                        <IconFOU name="star" size={20} style={{
                           textAlign: 'center',
                           color: item.isFavourite === "true" ? "#ffc43f" : "#ccc"
                        }} />
                     </TouchableOpacity>
                  </View>
                  <View style={{ width: '33.33%', borderLeftWidth: .5, borderLeftColor: '#ccc', borderRightColor: '#ccc', borderRightWidth: .5 }}>
                     <TouchableOpacity onPress={this.sharing.bind(this, item)} >
                        <IconFOU name="share" size={20} style={{ textAlign: 'center', color: '#0095ff' }} />
                     </TouchableOpacity>
                  </View>
                  <View style={{ width: '33.33%' }}>
                     <TouchableOpacity onPress={this.downloadFile.bind(this, item)}>
                        <IconFOU name="download" size={20} style={{ textAlign: 'center', color: '#de2d30' }} />
                     </TouchableOpacity>
                  </View>
               </View>

            </CardItem>
         </Card>
      )
   }

   /************************* ListEmpty show content *************************/
   ListEmpty = () => {
      return (
         <View style={styles.MainContainer}>
            <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
            <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
         </View>
      );
   };

   /************************* Download PDF File *************************/
   downloadFile(item) {
      function getLocalPath(url) {
         const filename = url.split('/').pop();
         return `${RNFS.DocumentDirectoryPath}/${filename}`;
      }

      const url = item.itinerary;
      const localFile = getLocalPath(url);

      const options = {
         fromUrl: url,
         toFile: localFile
      };
      RNFS.downloadFile(options).promise
         .then(() => FileViewer.open(localFile))
         .then(() => {
            // success
         })
         .catch(error => {
            // error
         });
   }



   render() {
      const { isLoading, data } = this.state;
      console.log(this.props.code_id, '------------code');
      console.log(this.state.data, '-----------data')


      return (
         <Content style={{ borderBottomWidth: 0 }}>
            {
               isLoading ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                     <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                     <ActivityIndicator size="large" color="#de2d30" />
                  </View>
               ) : (
                     <View style={{paddingBottom: 50}}>
                        <FlatList
                           data={data}
                           extraData={this.state}
                           renderItem={this.renderItem}
                           keyExtractor={this.keyExtractor}
                           ListEmptyComponent={this.ListEmpty}
                        />
                     </View>

                  )
            }

         </Content>

      );
   }
}


export default HightLineData;

const styles = StyleSheet.create({

   IconColor: { color: '#de2d30', textAlign: 'center', paddingTop: 20, },
   TextTitle: { color: '#de2d30', fontWeight: 'bold', },
   TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
   TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, },
   Textprice: { color: '#00953b', width: '35%' },
   textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
   textWarp: { flexDirection: 'row', marginTop: 2, },
   downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
   dwnwarp: { width: '100%', },
   bodystyle: {
      width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
      paddingTop: 0, borderBottomWidth: 0
   },
   texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
   dwntext: { marginTop: 10, textAlign: 'center', marginRight: 0, color: '#de2d30' },
   MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },
})
