import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image, FlatList, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Card, CardItem, Text, Body } from 'native-base';
import { Actions } from 'react-native-router-flux';




class ListHotPick extends Component {

  constructor(props) {
    super(props);
    this.state = {
      langPrint: ''
    }
  }

  componentDidMount() {
    /************************************ Get Language Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes
      })
    })
  }

  /************************* Render Item in FlatList *************************/
  renderItem = ({ item }) => {
    const { langPrint } = this.state;
    return (

      <TouchableOpacity onPress={() => Actions.hightLine({
        code_id: item.tourcode,
        keyword_id: item.keyword
      })}>
        <Card style={{ flex: 0, marginBottom: 30 }}>
          <CardItem style={{ paddingTop: 0, paddingBottom: 0, paddingRight: 0, paddingLeft: 0 }}>
            <Body>
              <Image source={{ uri: item.pic_url }} style={{ height: 200, width: '100%', flex: 1 }} />
              <View style={{ padding: 20, flexDirection: 'row' }}>
                <Text style={{ width: '60%' }}>{langPrint === 'zh' ? item.destination_cn : item.destination}</Text>
                <Text style={{ width: '40%', textAlign: 'right', color: '#de2d30', fontWeight: 'bold' }}>RM {item.starter_price}</Text>
              </View>
            </Body>
          </CardItem>
        </Card>
      </TouchableOpacity>

    )
  }

  render() {
    console.log(this.props.data, '==========propsdata')
    return (

      <View style={styles.HotpickContainer}>
        <ScrollView >
          <View style={{ padding: 20 }}>
            <FlatList
              data={this.props.data}
              renderItem={this.renderItem}
            />
          </View>

          <TouchableOpacity onPress={() => Actions.gdSeries()}>
            <Text style={{ textAlign: 'right', paddingRight: 20, paddingLeft: 20,paddingBottom: 40,color: '#de2d30', }}>{global.t('View_More')} ></Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}


export default ListHotPick;

const styles = StyleSheet.create({
  HotpickContainer: { flex: 1, },
  Container: { width: '100%', height: 300, },
})
