import React, { Component } from 'react';
import {
  View,
  Picker,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Platform,
  Modal
} from 'react-native';
import IconANT from "react-native-vector-icons/AntDesign";

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;

export default class YearMonthPicker extends Component {
  constructor(props) {
    super(props);
    let { startYear, endYear, selectedYear, selectedMonth, visiable } = props;
    let years = this.getYears(startYear, endYear);
    let months = this.getMonths();
    selectedYear = selectedYear || years[0];
    selectedMonth = selectedMonth || ((new Date()).getMonth() + 1);
    this.state = {
      years,
      months,
      selectedYear,
      selectedMonth,
      visiable: visiable || false.render,

    }
  }

  show = async ({ startYear, endYear, selectedYear, selectedMonth }) => {
    let years = this.getYears(startYear, endYear);
    let months = this.getMonths();
    selectedYear = selectedYear || years[0];
    selectedMonth = selectedMonth || ((new Date()).getMonth() + 1);
    let promise = new Promise((resolve) => {
      this.confirm = (year, month) => {
        resolve({
          year,
          month
        });
      }
      this.setState({
        visiable: true,
        years,
        months,
        startYear: startYear,
        endYear: endYear,
        selectedYear: selectedYear,
        selectedMonth: selectedMonth,
      })
    })
    return promise;
  }

  dismiss = () => {
    this.setState({
      visiable: false
    })
  }

  getYears = (startYear) => {
    startYear = startYear || (new Date()).getFullYear();
    // endYear = endYear || (new Date()).getFullYear();
    let years = []
    for (let i = startYear; i <= startYear + 1; i++) {
      years.push(i)
    }
    return years;

  }

  getMonths = () => {
    let months = []
    for (let i = 1; i <= 12; i++) {
      months.push(i);
    }
    return months;
  }

  renderPickerItems = (data) => {
    let items = data.map((value, index) => {
      return (<Picker.Item key={'r-' + index} label={'' + value} value={value} />)
    })
    return items;
  }

  onCancelPress = () => {
    this.dismiss();
  }

  onConfirmPress = () => {
    const confirm = this.confirm;
    const { selectedYear, selectedMonth } = this.state;
    confirm && confirm(selectedYear, selectedMonth);
    this.dismiss();
  }



  render() {
    const { years, months, selectedYear, selectedMonth, visiable } = this.state;
    if (!visiable) return null;
    return (
      <Modal
        transparent={true}
        style={styles.modal}
        onPress={this.onCancelPress}
      >
        <View style={Platform.OS !== 'android' ? styles.outerContainerIOS : styles.outerContainerAndroid}>
          <View style={styles.toolBar}>
            <TouchableOpacity style={styles.toolBarButton} onPress={this.onCancelPress}>
              <Text style={styles.toolBarButtonText}>{global.t('Cancel')}</Text>
            </TouchableOpacity>
            <View style={{ flex: 1 }} />
            <TouchableOpacity style={styles.toolBarButton} onPress={this.onConfirmPress}>
              <Text style={styles.toolBarButtonText}>{global.t('Confirm')}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.innerContainer}>
            <Picker
              style={styles.picker}
              selectedValue={selectedYear}
              onValueChange={(itemValue, itemIndex) => this.setState({ selectedYear: itemValue })}
            >
              {this.renderPickerItems(years)}
            </Picker>
            <Picker
              style={styles.picker}
              selectedValue={selectedMonth}
              onValueChange={(itemValue, itemIndex) => this.setState({ selectedMonth: itemValue })}
            >
              {this.renderPickerItems(months)}
            </Picker>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#00000073',
  },
  outerContainerIOS: {
    backgroundColor: '#fff',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,

  },
  outerContainerAndroid: {
    backgroundColor: '#fff',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 130,
    paddingLeft: 10,
    paddingRight: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  toolBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 44,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#EBECED',

  },
  toolBarButton: {
    height: 44,
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  toolBarButtonText: {
    fontSize: 15,
    color: '#2d4664',
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  picker: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5
  }
})