import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, ActivityIndicator, Share, RefreshControl } from 'react-native';
import { Toast, Content, Card, CardItem, Text, Body } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob'
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { TourGetDetialApi } from '../../../../../../PostApi'
import IconFOU from "react-native-vector-icons/Foundation";
import IconANT from "react-native-vector-icons/AntDesign";
import { MonthGetDetialApi } from '../../../../../../PostApi';


// keyExtractor = (item) => item.key;

/************************* If Api Data Empty Show Local data *************************/
const localdata = [
   { "airlinelocal_en": "To Be Confirmed", "airlinelocal_cn": "待确认", "statuslocal_en": "Selling Fast", "statuslocal_cn": "热销中" }
]


/************************ Validations Toast Style **************************/
const textStyle = { color: "#83ff83" };
const position = "bottom";
const buttonText = "Okay";
const duration = 3000;
const style = { bottom: '25%' };


class DateList extends Component {
   constructor(props) {
      super(props);
      this.state = {
         dataUp: [],
         isLoading: true,
         langPrint: '',
         refreshing: false,
         start_date: this.props.data.departure,
         end_date: this.props.data.departure,
         titleprops: this.props.data.title,
         dataLenght: ''


      }

   }

   componentDidMount() {

      /************************************ Get Language Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem("language").then(langStorageRes => {
         //console.log(langStorageRes, '----------------langStorageRes-----Note')
         this.setState({
            langPrint: langStorageRes
         })
      })

      /************************************ Get all Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
         //console.log(asyncStorageRes, '----------------AsyncStorage')
         this.setState({
            tokenId: asyncStorageRes
         })
         this.getData();


      });

      // /*************************************** Fetch Api Data ***************************************/
      // MonthGetDetialApi().then((fetchData) => {
      //     console.log(fetchData, 'dataUp')
      //     this.setState({
      //         dataUp: fetchData,
      //         isLoading: false
      //     })
      // })




   }

   /************************************ sharing function ***********************************/
   sharing(item) {
      const { langPrint } = this.state;
      console.log('sharing');
      Share.share(
         {
            message: langPrint === 'zh' ?
               `${global.t('Destination')}: ${item.cn_title}` + "\n" +
               `${global.t('Tour_Code')}: ${item.tour_code}` + "\n" +
               `${global.t('Departure')}: ${item.departure}` + "\n" +
               `${global.t('Airline')}: ${item.airline === null ? localdata[0].airlinelocal_cn : item.airline}` + "\n" +
               `${global.t('Status')}: ${item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}` + "\n" +
               `${global.t('Price')}: RM ${item.price}` + "\n" +
               `${global.t('Itinerary')}: ${item.itinerary}`
               :
               `Destination: ${item.title}` + "\n" +
               `Tourcode: ${item.tour_code}` + "\n" +
               `Departure: ${item.departure}` + "\n" +
               `Airline: ${item.airline === null ? localdata[0].airlinelocal_en : item.airline}` + "\n" +
               `Status: ${item.status === "" ? localdata[0].statuslocal_en : item.status}` + "\n" +
               `Price: RM ${item.price}` + "\n" +
               `Itinerary: ${item.itinerary}`
            ,
            title: 'Golden Destinations',
            url: item.itinerary
         }
      )
   }

   /************************************ fetch first display data upcoming ***********************************/
   getData(item, access_token = this.state.tokenId, start_date = this.state.start_date, end_date = this.state.end_date) {

      console.log('start_date-----', start_date)

      let url = `https://www.goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&access_token=${access_token}&departure_date_start=${start_date}&departure_date_end=${end_date}`;
      console.log(url, '----url')

      fetch(url).then((res) => res.json())
         .then((fetchData) => {
            this.setState({
               dataUp: fetchData,
               isLoading: false,
               refreshing: false,
               start_date: fetchData.departure,
               start_date: end_date.departure
            })
         })

   }

   /************************************ addFavorite function ***********************************/
   addFavorite(item, index, tourcode = item.tour_code, access_token = this.state.tokenId) {
      console.log(tourcode, 'tourcode', access_token, 'addFavorite---add')
      const items = this.state.dataUp
      let obj = this
      let url = `https://www.goldendestinations.com/api/new/like.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

      fetch(url).then((res) => res.json())
         .then(function (resJson) {
            console.log(resJson, '-----------resJson add');
            if (resJson[0].status === 1) {
               console.log(resJson, 'ok-------------add');
               foundItem = items.findIndex(x => x.tour_code == tourcode)
               item.isFavourite = "true"
               items[foundItem] = item
               console.log(items, 'items')
               obj.setState({
                  dataUp: items
               })
            }
            else if (access_token === null) {
               alert(global.t('pls_login_add'))
            }
            else {
               alert(resJson[0].error)

            }
         })


      console.log('add')
   }

   /************************************ removeFavorite function ***********************************/
   removeFavorite(item, index, tourcode = item.tour_code, access_token = this.state.tokenId, ) {
      console.log(tourcode, 'tourcode', access_token, 'removeFavorite---remove')
      const items = this.state.dataUp
      let obj = this
      let url = `https://www.goldendestinations.com/api/new/unlike.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

      fetch(url).then((res) => res.json())

         .then(function (resJson) {
            console.log(resJson, '-----------resJson remove');
            if (resJson[0].status === 1) {
               console.log(resJson, 'remove-------------remove');
               foundItem = items.findIndex(x => x.tour_code == tourcode)
               item.isFavourite = "false"
               items[foundItem] = item
               console.log(items, 'items')
               obj.setState({
                  dataUp: items
               })
            }
            else {
               alert(resJson[0].error)
            }
         })


      console.log('remove')
   };


   /*************************************** Download PDF File ***************************************/
   downloadFile(item) {
      function getLocalPath(url) {
         const filename = url.split('/').pop();
         return `${RNFS.DocumentDirectoryPath}/${filename}`;
      }

      const url = item.itinerary;
      const localFile = getLocalPath(url);

      const options = {
         fromUrl: url,
         toFile: localFile
      };
      RNFS.downloadFile(options).promise
         .then(() => FileViewer.open(localFile))
         .then(() => {
            // success
         })
         .catch(error => {
            // error
         });
   }

   /*************************************** _renderItem function ***************************************/
   _renderItem = ({ item, index }) => {
      const { langPrint, active: sd, } = this.state;
      const localColor = { color: sd === index ? "#ffc43f" : "#fff" }

      return (
         <Card >
            <CardItem style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#fcf5f5', marginRight: 0 }}>
               <Body style={{ marginRight: 0, }}>
                  <View style={{ flexDirection: 'row' }}>
                     <View style={{ alignItems: 'flex-start', width: '5%', }}>
                        <Image source={{uri: item.brand_logo}} style={{ width: 20, height: 20 }} />
                     </View>
                     <View style={{ width: '95%' }}>
                        <Text note style={{ fontWeight: 'bold', textAlign: 'center', marginLeft: 5 }}>
                           {langPrint === 'zh' ? item.cn_title : item.title}
                        </Text>
                     </View>
                  </View>
               </Body>
            </CardItem>
            <CardItem cardBody >
               <View style={{ flexDirection: 'row' }}>
                  <View style={{ paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15 }}>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Tour_Code')}: </Text>
                        <Text note style={styles.TextInner2}>{item.tour_code}</Text>
                     </View>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Airline')}: </Text>
                        {
                           langPrint === 'zh' ? (
                              <Text note style={styles.TextInner2}>{item.airline === null ? localdata[0].airlinelocal_cn : item.airline}</Text>
                           ) : (
                                 <Text note style={styles.TextInner2}>{item.airline === null ? localdata[0].airlinelocal_en : item.airline}</Text>
                              )
                        }
                     </View>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Status')}: </Text>
                        {
                           langPrint === "zh" ? (
                              <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                 {item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}
                              </Text>
                           ) : (
                                 <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                                    {item.status === "" ? localdata[0].statuslocal_en : item.status}
                                 </Text>
                              )
                        }
                     </View>
                     <View style={styles.textWarp}>
                        <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Departure')}: </Text>
                        <Text note style={styles.TextInner2}>{item.departure}</Text>
                     </View>

                     <View style={{ flexDirection: 'row', marginTop: 5 }}>
                        <Text note style={{ width: '30%', color: '#00953b', marginTop: 2 }}>{global.t('Price')}: </Text>
                        <Text note style={styles.Textprice}>RM {item.price}</Text>
                     </View>
                  </View>

               </View>
            </CardItem>
            <CardItem style={{ borderTopWidth: .5, borderTopColor: '#ccc' }}>
               <View style={{ flexDirection: 'row' }}>
                  <View style={{ width: '33.33%', }}>
                     <TouchableOpacity onPress={item.isFavourite === "true" ? this.removeFavorite.bind(this, item) : this.addFavorite.bind(this, item)}>
                        <IconFOU name="star" size={20} style={{
                           textAlign: 'center',
                           color: item.isFavourite === "true" ? "#ffc43f" : "#ccc"
                        }} />
                     </TouchableOpacity>
                  </View>
                  <View style={{ width: '33.33%', borderLeftWidth: .5, borderLeftColor: '#ccc', borderRightColor: '#ccc', borderRightWidth: .5 }}>
                     <TouchableOpacity onPress={this.sharing.bind(this, item)} >
                        <IconFOU name="share" size={20} style={{ textAlign: 'center', color: '#0095ff' }} />
                     </TouchableOpacity>
                  </View>
                  <View style={{ width: '33.33%' }}>
                     <TouchableOpacity onPress={this.downloadFile.bind(this, item)}>
                        <IconFOU name="download" size={20} style={{ textAlign: 'center', color: '#de2d30' }} />
                     </TouchableOpacity>
                  </View>
               </View>

            </CardItem>
         </Card>
      )
   }


   /*************************************** ListEmpty function ***************************************/
   ListEmpty = () => {
      return (
         <View style={styles.MainContainer}>
            <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
            <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
         </View>
      );
   };


   render() {
      const { isLoading, dataUp, titleprops } = this.state;
      //console.log(this.props.data, 'this.props.data--------')

      //const dateprops = this.props.data.title;
      //console.log(dateprops, 'this.props.data--------title')

      /************************* find match props title data list *************************/
      const unique = this.state.dataUp;
      const res = unique.filter(i => titleprops.includes(i.title));
      console.log(unique, 'unique--------------dataUp');
      console.log(res, 'unique--------------res');

      console.log(titleprops, 'title')




      return (
         <Content style={{ borderBottomWidth: 0 }}>
            {
               isLoading ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                     <Text style={{ textAlign: 'center', marginBottom: 10 }}>{global.t('Fetching_data')}</Text>
                     <ActivityIndicator size="large" color="#de2d30" />
                  </View>
               ) : (
                     <View style={{ paddingBottom: 70 }}>
                        <FlatList
                           data={res}
                           extraData={this.state}
                           renderItem={this._renderItem}
                           keyExtractor={this.keyExtractor}
                           ListEmptyComponent={this.ListEmpty}
                        />
                     </View>
                  )
            }

         </Content>

      );
   }
}


export default DateList;

const styles = StyleSheet.create({

   IconColor: { color: '#de2d30', textAlign: 'center', paddingTop: 30, },
   TextTitle: { color: '#de2d30', fontWeight: 'bold', },
   TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
   TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, marginTop: 2 },
   Textprice: { color: '#00953b', width: '35%', marginTop: 2 },
   textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
   textWarp: { flexDirection: 'row', marginTop: 2, },
   downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
   dwnwarp: { width: '100%', },
   bodystyle: {
      width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
      paddingTop: 0, borderBottomWidth: 0
   },
   texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
   dwntext: { marginTop: 15, textAlign: 'center', marginRight: 0, color: '#de2d30' },
   MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },

})
