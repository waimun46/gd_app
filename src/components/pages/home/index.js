import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, TouchableOpacity, Text, BackHandler , Dimensions, Image} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import TimeLocations from './components/TimeLocations';
import LocationsSelect from './components/LocationsSelect';
import Upcoming from './components/Upcoming';
import SeriesSearch from './components/seriesSearch';
import LoginModal from '../login';
import IconANT from "react-native-vector-icons/AntDesign";


class HomesScreen extends Component {

   constructor(props) {
      super(props);
      this.state = {
         tokenId: 0,
         isShowToTop: false,
      }

   }

   componentDidMount() {

      /************************************ Get Store Data From AsyncStorage  ***********************************/
      AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
         //console.log(asyncStorageRes, 'HomesScreen----------------tokenId')
         this.setState({
            tokenId: asyncStorageRes
         })
         //console.log(this.state.tokenId, '-------------InforScreen')
      });

      // this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

   }

   // componentWillUnmount() {
   //     this.backHandler.remove()
   // }

   // handleBackPress = () => {
   //     //alert('are you sure')
   //     //this.exitApp();
   //     return true;
   // }

   // exitApp() {
   //     BackHandler.exitApp()
   // }


   scrollToTop() {
      console.log('----------scrollTo')
      this._scrollView.scrollTo({ x: 100, animated: true });
   }

   _onScroll(e) {
      let offsetY = e.nativeEvent.contentOffset.y;

      if (offsetY > 50) {
         this.setState({
            isShowToTop: true
         })
      } else {
         this.setState({
            isShowToTop: false
         })
      }
   }



   render() {
      //console.log(this.state.tokenId,'-------------------HomesScreen----render')
      const { tokenId } = this.state;
      return (

         <View style={styles.HotpickContainer}>

            <TimeLocations />
          
            <ScrollView ref={view => this._scrollView = view} onScroll={(e) => this._onScroll(e)} >

               {/* <ScrollView  > */}
               <LocationsSelect />
               {/* <View style={{ padding: 7, backgroundColor: '#e9ecef' }}></View> */}
               {/* <Upcoming /> */}
               <SeriesSearch />
               {
                  tokenId !== null ? (
                     null
                  ) : (
                        <LoginModal />
                     )
               }
            </ScrollView>

            {
               this.state.isShowToTop ? (
                  <TouchableOpacity onPress={() => this.scrollToTop()} style={styles.scrollBtn}>
                     <IconANT name='up' style={styles.iconTop} />
                  </TouchableOpacity>

               ) : (
                     null
                  )
            }

         </View>
      );
   }
}


export default HomesScreen;

const styles = StyleSheet.create({
   // HotpickContainer: { backgroundColor: 'white', },
   scrollBtn: {
      position: 'absolute', flex: 0.1, right: 10, bottom: -10, borderRadius: 50,
      backgroundColor: '#00000057', padding: 10, width: 50, height: 50, marginBottom: 100,
      color: 'red', textAlign: 'center'
   },
   iconTop: { fontSize: 22, color: 'white', textAlign: 'center', fontWeight: 'bold', paddingTop: 3 },

})
