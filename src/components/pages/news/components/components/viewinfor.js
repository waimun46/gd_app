import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image, Dimensions, Linking, WebView , TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, } from 'native-base';
import _ from 'lodash'
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
// import { TouchableOpacity } from 'react-native-gesture-handler';





class ViewInfor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            langPrint: '',
        }
    }

    componentDidMount() {
        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })
    }

    render() {
        const { langPrint } = this.state;
        const dataPrint = this.props.data;
        const htmlContent_en = this.props.data.description;
        const htmlContent_cn = this.props.data.description_cn;
        const video = this.props.data.video;
        console.log(video, 'video');
        console.log(dataPrint, 'dataPrint');

        /************************* IGNORED TAGS Api Data *************************/
        const tags = _.without(IGNORED_TAGS,
            'table', 'caption', 'col', 'colgroup', 'tbody', 'td', 'tfoot', 'th', 'thead', 'tr', 'iframe', 'pre', 
        )
        /************************* Render Table *************************/
        const renderers = {
            table: (x, c) => <ScrollView horizontal={true}><View style={tableColumnStyle}>{c}</View></ScrollView>,
            col: (x, c) => <View style={tableColumnStyle}>{c}</View>,
            colgroup: (x, c) => <View style={tableRowStyle}>{c}</View>,
            tbody: (x, c) => <View style={tableColumnStyle}>{c}</View>,
            tfoot: (x, c) => <View style={tableRowStyle}>{c}</View>,
            th: (x, c) => <View style={thStyle}>{c}</View>,
            thead: (x, c) => <View style={tableRowStyle}>{c}</View>,
            caption: (x, c) => <View style={tableColumnStyle}>{c}</View>,
            tr: (x, c) => <View style={tableRowStyle}>{c}</View>,
            td: (x, c) => <View style={tdStyle}>{c}</View>,
            iframe: (x, c) => <WebView source={{uri: video}}
                style={{ height: 200, width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'black' }}
            />,
            pre: (x, c) => <View style={imgStyle}>{c}</View>,
            // a: (x, c) => <TouchableOpacity onPress={(event, href) => { Linking.openURL(href)}}><Text>{c}</Text></TouchableOpacity>,
            // p: (x, c) => <View style={{paddingTop: 20}}>{c}</View>

        }
        /************************* Style Table *************************/
        const tableDefaultStyle = {flex: 1, justifyContent: 'flex-start',}
        const tableColumnStyle = {...tableDefaultStyle,flexDirection: 'column',alignItems: 'stretch',width: 800,}
        const tableRowStyle = {...tableDefaultStyle,flexDirection: 'row',alignItems: 'stretch',}
        const tdStyle = {...tableDefaultStyle,padding: 2,backgroundColor: '#f6f8fa',borderWidth: .5,}
        const thStyle = {...tdStyle,backgroundColor: '#fcf5f5',alignItems: 'center',}
        const imgStyle = {width: '100%',
         //    height: 1000
        }

        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>

                <ScrollView>
                    <View style={styles.TextContainer}>
                        <Text style={styles.TitleText}>{dataPrint.title === "zh" ? dataPrint.title_cn : dataPrint.title}</Text>
                        <View style={styles.ImageContainer}>
                            <Image source={{ uri: this.props.data.pic_url }} style={styles.ImageStyle} />
                        </View>
                        {/* <Text style={styles.TextStyle}>{this.props.data.description}</Text> */}
                    </View>
                    <HTML
                        alterChildren={(node) => {
                            if (node.name === 'iframe') {
                                delete node.attribs.width;
                                delete node.attribs.height;
                            }
                            return node.children;
                        }}
                        html={langPrint === "zh" ? htmlContent_cn : htmlContent_en}
                        imagesInitialDimensions={{ width: '100%' }}
                        tagsStyles={{
                            p: { lineHeight: 20, fontSize: 14, paddingBottom: 10},
                            h2: {fontSize: 14,lineHeight: 20, paddingBottom: 10},
                            h3: {fontSize: 14,lineHeight: 20, paddingBottom: 10}
                        }}
                        ignoredStyles={[
                            'font-family', 'display', 'width', 'padding','font-size',
                            // 'border-collapse','border-spacing','font-weigh','color','border','margin-top','margin-bottom','margin','page-break-after',
                            // 'font-variant-numeric','font-variant-east-asian','list-style', 'height','table-layout','direction','unicode-bidi',
                            // 'position','-webkit-font-smoothing','overflow','padding-bottom','line-height','cursor','text-decoration-line',
                            // 'white-space','box-sizing','overflow-wrap','font-weight',
                        ]}
                        ignoredTags={tags}
                        //ignoredTags={[...IGNORED_TAGS,'iframe',]}
                        renderers={renderers}
                        onLinkPress={(event, href) => { Linking.openURL(href) }}
                        //staticContentMaxWidth={Dimensions.get('window').width}s
                        //imagesMaxWidth={Dimensions.get('window').width}
                        //debug={true}
                        //containerStyle={{padding: 10}}
                        classesStyles={{
                            _3dgx: { fontSize: 14, paddingLeft: 5, paddingRight: 5 },
                            _4lmk: { paddingLeft: 5, paddingRight: 5 },
                            _50a1: { marginLeft: 0, marginRight: 0, marginTop: 30, marginBottom: 10, paddingLeft: 5, paddingRight: 5 },
                        }}


                    />
                    {/* 
                        <WebView
                            ref={'webview'}
                            automaticallyAdjustContentInsets={false}
                            style={styles.webView}
                            html={htmlCode} />
                        />
                         <WebView
                        ref={'webview'}
                            scalesPageToFit={true}
                            source={{ html: this.props.data.description }}
                        //injectedJavaScript="window.postMessage(document.description)"
                        />
                        <WebView
                            style={styles.TextStyle}
                            source={{ uri: urlapi }}
                            domStorageEnabled={true}
                            startInLoadingState={true}
                            javaScriptEnabled={true}
                        />
                        */}

                    <View style={{ paddingBottom: 50 }}></View>

                </ScrollView>
            </View >


        );
    }
}


export default ViewInfor;

const styles = StyleSheet.create({
    //    TextContainer: { paddingTop: 20, paddingBottom: 20 },
    TitleText: { textAlign: 'left', fontSize: 22, fontWeight: 'bold', color: '#de2d30', textTransform: 'uppercase', padding: 10, paddingTop: 20, },
    TextStyle: { color: '#999', textAlign: 'left', fontSize: 16, paddingBottom: 20 },
    ImageContainer: { paddingTop: 10, paddingBottom: 20, width: '100%' },
    ImageStyle: { height: 250, width: '100%', resizeMode: "cover", },


})
