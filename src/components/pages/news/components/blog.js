import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, TouchableOpacity, ActivityIndicator, ImageBackground } from 'react-native';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import { BlogArticleApi } from '../../../../../PostApi';

keyExtractor = (item) => item.key;


class Blog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            langPrint: '',
            banner: []

        }
    }

    componentDidMount() {

        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })

        /************************* Fetch Data in Api *************************/
        BlogArticleApi().then((fetchData) => {
            this.setState({
                data: fetchData,
                banner: fetchData[0],
                isLoading: false
            })
        })

    }

    /************************* Render Item in FlatList *************************/
    renderItem = ({ item }) => {
        const { langPrint } = this.state;
        return (
            item.title ?
                <List>
                    <ListItem thumbnail>
                        <Left >
                            <Thumbnail square
                                source={{ uri: item.pic_url }}
                                style={{ height: 80, width: 100 }}
                            />
                        </Left>
                        <Body style={{ height: 100 }}>
                            <Text  numberOfLines={3} style={styles.ListStyle}>{langPrint === 'zh' ? item.title_cn : item.title}</Text>

                        </Body>
                        <Right>
                            <TouchableOpacity onPress={() => Actions.viewinfor({ data: item })}>
                                <View style={styles.ButtonStyle}>
                                    <Text note style={styles.ButtonText}>{global.t('View')}</Text>
                                </View>
                            </TouchableOpacity>
                        </Right>
                    </ListItem>
                </List>
                : null
        )
    }

    render() {
        const { isLoading, banner, data } = this.state;
        return (
            <ScrollView>
                <ImageBackground
                    style={styles.TitleTextContanier}
                    source={{ uri: banner.top_banner }}
                >
                    <Text  numberOfLines={3} style={styles.TitleText}>{global.t('BLOG')}</Text>
                </ImageBackground>
                <View style={{marginBottom: 50}}>
                    {
                        isLoading ? (
                            <ActivityIndicator size="large" color="#de2d30" style={styles.loading} />
                        ) : (
                                <FlatList
                                    data={data}
                                    renderItem={this.renderItem}
                                />
                            )
                    }

                </View>
            </ScrollView>
        );
    }
}


export default Blog;

const styles = StyleSheet.create({
    TitleTextContanier: { padding: 50 },
    TitleText: {
        textAlign: 'center', fontWeight: 'bold', fontSize: 25, color: '#fff',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 2, textShadowColor: '#000',
    },
    Textstyle: { textAlign: 'center', paddingTop: 10, color: '#7e888b', fontSize: 14 },
    ButtonStyle: { backgroundColor: '#ff2951', borderRadius: 8, },
    ButtonText: { fontWeight: 'bold', color: 'white', paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5 },
    ListStyle: { fontWeight: 'bold', fontSize: 14, lineHeight: 20, paddingBottom: 10 },
    loading: { marginTop: 50 }
})
