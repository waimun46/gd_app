import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Content, Tab, Text, Tabs } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import EventNews from './components/eventnews';
import Blog from './components/blog';
import Promotions from './components/promotion';

import ScrollableTabView, { DefaultTabBar, ScrollableTabBar } from 'react-native-scrollable-tab-view-forked'


class Newspage extends Component {


    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <ScrollableTabView

                    renderTabBar={() => (
                        <DefaultTabBar
                            style={styles.scrollStyle}
                            tabStyle={styles.tabStyle}
                        />
                    )}
                    tabBarTextStyle={styles.tabBarTextStyle} 
                    tabBarInactiveTextColor={'black'}
                    tabBarActiveTextColor={'red'}
                    tabBarUnderlineStyle={styles.underlineStyle}
                    initialPage={0}
                    style={{width: '100%'}}
                >
                    <EventNews key={'1'} tabLabel={global.t('EVENTS_NEWS')} />
                    <Blog key={'2'} tabLabel={global.t('BLOG')} />
                    <Promotions key={'3'} tabLabel={global.t('PROMOTION')} />
                   


                </ScrollableTabView>

                {/* <ScrollView >
                    <View style={{ paddingBottom: 30 }}>
                        <Tabs
                            tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}
                            style={{ backgroundColor: 'white', }}
                        //locked
                        >
                    
                            <Tab
                                heading={global.t('EVENTS_NEWS')}
                                tabStyle={{ backgroundColor: "white", }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontWeight: 'bold', fontSize: 13 }}
                                activeTextStyle={{ color: 'red', fontWeight: 'bold', fontSize: 13 }}
                            >
                                <EventNews />
                            </Tab>

                    
                            <Tab
                                heading={global.t('BLOG')}
                                tabStyle={{ backgroundColor: "white", }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontWeight: 'bold', fontSize: 13 }}
                                activeTextStyle={{ color: 'red', fontWeight: 'bold', fontSize: 13 }}
                            >
                                <Blog />
                            </Tab>

                            <Tab
                                heading={global.t('PROMOTION')}
                                tabStyle={{ backgroundColor: "white" }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#ccc', fontWeight: 'bold', fontSize: 13 }}
                                activeTextStyle={{ color: 'red', fontWeight: 'bold', fontSize: 13 }}
                            >
                                <Promotions />
                            </Tab>
                        </Tabs>
                    </View>
                </ScrollView> */}

            </View>
        );
    }
}


export default Newspage;

const styles = StyleSheet.create({
    activeTabStyle: {
        color: 'red'
    },
    tabStyle: {
        color: 'red',
        width: '100%'
    },
    tabStyle: {paddingRight: 0, width: '100%',},
    scrollStyle: {
        backgroundColor: 'white',
        width: '100%',
        paddingRight: 0,
        // justifyContent: 'center',
    },
    tabBarTextStyle: {
        fontSize: 14,
        fontWeight: 'normal',
    },
    underlineStyle: {
        height: 3,
        backgroundColor: 'red',
        borderRadius: 3,
        width: 30,
    },
})
