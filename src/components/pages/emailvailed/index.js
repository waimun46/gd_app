import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, ImageBackground, Image, Dimensions, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';


class EmailVailed extends Component {

    constructor(props) {
        super(props)
        this.state = {
            langPrint: ''
        }
    }

    componentDidMount() {
        /************************************ Get Language Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem("language").then(langStorageRes => {
            //console.log(langStorageRes, '----------------langStorageRes-----Note')
            this.setState({
                langPrint: langStorageRes
            })
        })
    }


    render() {
        const {langPrint} =this.state;
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <ScrollView >
                    <View >
                        {
                            langPrint === "zh" ? (
                                <ImageBackground
                                    source={require('../../../assets/images/img/email_zh.png')}
                                    style={styles.ImageBackgroundStyle}
                                ></ImageBackground>
                            ) : (
                                    <ImageBackground
                                        source={require('../../../assets/images/img/email_en.png')}
                                        style={styles.ImageBackgroundStyle}
                                    >
                                    </ImageBackground>
                                )
                        }


                        <View style={styles.BottomContainer}>
                            <View>
                                {/* <Text style={styles.TextTitle}>{global.t("Congratulations")}</Text> */}
                                <Text style={styles.TextStyle}>{global.t("Thanks_Register")}</Text>
                            </View>
                            <View style={styles.BottomTextContainer}>
                                <TouchableOpacity onPress={() => Actions.Home()}>
                                    <Text style={styles.BottomText}>{global.t("Back_Home")}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


export default EmailVailed;

const styles = StyleSheet.create({
    ImageBackgroundStyle: {
        width: '100%', resizeMode: 'cover', flex: 1, alignItems: 'center',
        justifyContent: 'center', height: Dimensions.get('window').width
    },
    TextTitle: { textAlign: 'center', fontSize: 30, color: '#de2d30', fontWeight: 'bold', },
    TextStyle: { textAlign: 'center', fontSize: 25, marginTop: 20, color: '#00953b', lineHeight: 30 },
    BottomContainer: { padding: 40, flex: 1, alignItems: 'center', },
    BottomTextContainer: {
        width: '100%', justifyContent: 'center', alignItems: 'center',
        marginTop: 30, marginBottom: 50
    },
    BottomText: { textAlign: 'center', fontWeight: 'bold', }
})
