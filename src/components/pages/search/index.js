import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, TouchableOpacity, FlatList, ActivityIndicator, Share, Image, Platform, SafeAreaView, TextInput } from 'react-native';
import { Card, CardItem, Toast, Text, Body, ListItem, Header, Item, Input, Icon, Button, } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import IconFOU from "react-native-vector-icons/Foundation";
import IconENT from "react-native-vector-icons/Entypo";
import IconANT from "react-native-vector-icons/AntDesign";
import { Actions } from 'react-native-router-flux';
import Search from 'react-native-search-box';
import { TourGetDetialApi } from '../../../../PostApi';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { Select, Option } from "react-native-chooser";
import Upcoming from '../home/components/Upcoming'




/************************* If Api Data Empty Show Local data *************************/
const localdata = [
  { "airlinelocal_en": "To Be Confirmed", "airlinelocal_cn": "待确认", "statuslocal_en": "Selling Fast", "statuslocal_cn": "热销中" }
]

const selectData = [
  { select_en: 'All', select_cn: '全部', }, { select_en: 'Bookable', select_cn: '可预订', },
]


/************************ Validations Toast Style **************************/
const textStyle = { color: "#83ff83" };
const position = "bottom";
const buttonText = "Okay";
const duration = 3000;
const style = { bottom: '10%' };



class SearchList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      langPrint: '',
      isShowToTop: false,
      myfavorite: [],
      favoriteData: [],
      keyword: '',
      error: [],
      dataFirst: [],
      toggalFavorite: false,
      favoriteList: [],
      codeData: [],
      tokenId: '',
      page: 1,
      loadingScroll: false,
      refreshing: false,
      search_value: '',
      text: '',
      searchLoading: true,
      selectValue: global.t('All'),
      dataFilter: [],
      searchFilter: [],
      searchFilterData: [],



    }
    // this.arrayholder = [];

  }

  /*************************************************** Select Option status ***************************************************/
  onSelect(value) {
    console.log(value, 'value-----------');
    dataFilter = this.state.dataFirst.filter(element => {
      return element.status !== 'Sold Out'
    }),
      // searchFilter = this.state.searchFilterData.filter(element => {
      //   return element.status !== 'Sold Out'
      // }),

      this.setState({
        selectValue: value,
        dataFilter: dataFilter,
        // searchFilter: searchFilter

      });


  }



  componentDidMount() {

    // this.setState({
    //   loadingScroll: true
    // },this.getData);

    /************************************ Get Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
      console.log(asyncStorageRes, '----------------AsyncStorage')
      this.setState({
        tokenId: asyncStorageRes
      })
      this.getData();
      this.searchKeyword();
      this.clearList();
      this.firstData();
    });

    AsyncStorage.getItem("language").then(langStorageRes => {
      //console.log(langStorageRes, '----------------langStorageRes-----Note')
      this.setState({
        langPrint: langStorageRes
      })
    })

    // AsyncStorage.getItem("LIST_FAVORITE").then(listFavoriteStorageRes => {
    //   console.log(listFavoriteStorageRes, '----------------listFavoriteStorageRes')
    //   this.setState({
    //     favoriteList: JSON.parse(listFavoriteStorageRes)
    //   })
    //   let codeFavoriteList = [];
    //   if (this.state.favoriteList.length > 0) {
    //     let code = this.state.favoriteList
    //     code.map((item, i) => {
    //       codeFavoriteList.push({ tourcode: item.tourcode })
    //     });
    //   }
    //   console.log('codeFavoriteList', codeFavoriteList)
    //   this.setState({
    //     codeData: codeFavoriteList
    //   })


    // })




  }


  /************************************ fetch first display data upcoming ***********************************/
  firstData(item, access_token = this.state.tokenId) {
    console.log('access_token-----firstData', access_token)
    let url = `https://www.goldendestinations.com/api/new/apitour.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&access_token=${access_token}`;
    //console.log(url, '----url')
    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        this.setState({
          dataFirst: fetchData,
          isLoading: false,
          refreshing: false

        })
      })

  }

  searchKeyword = () => {
    console.log('searchKeyword', this.state.text)
    this.setState({
      keyword: this.state.text,
      searchLoading: true,
      selectValue: global.t('All'),
    }, () => {
      this.getData()
    })
  }

  // searchKeyword(){
  //   console.log('searchKeyword',this.state.search_value)
  //   this.setState({
  //     search_value: this.state.search_value
  //   })
  //   this.getData()
  // }

  /************************************ insert keyword data ***********************************/
  getData = async (keyword = this.state.keyword, access_token = this.state.tokenId, page = this.state.page) => {
    let url = `https://www.goldendestinations.com/api/new/search.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&keyword=${keyword}&limit=20&page=${page}&access_token=${access_token}`;
    console.log(url, '----url')

    setTimeout(() => {
      fetch(url).then((res) => res.json())
        .then((fetchData) => {

          let filterDataSearch = fetchData.filter(ele => { return ele.status !== 'Sold Out' });

          this.setState({
            data: this.state.data.concat(fetchData),
            searchFilterData: this.state.searchFilterData.concat(filterDataSearch),
            error: fetchData[0].error,
            // favoriteData: fetchData,
            isLoading: false,
            loadingScroll: false,
            refreshing: false,
            searchLoading: false
          })
          // this.arrayholder = fetchData;
        })
    }, 2000)




  }




  /************************************ onSearch function ***********************************/
  // onSearch = (text) => {
  //   this.setState({
  //     keyword: text
  //   }, () => {
  //     if (this.state.keyword && this.state.keyword.length > 1) {
  //       if (this.state.keyword.length % 2 === 0) {
  //         this.getData()
  //       }
  //     } else if (!this.state.keyword) {
  //     }
  //   })
  // }

  // onSearch = text => {
  //   this.setState({
  //     keyword: text,
  //   });

  //   const newData = this.arrayholder.filter(item => {
  //     const itemData = this.state.keyword
  //     // const itemData = `${item.title.toUpperCase()} ${item.tour_code.toUpperCase()} ${item.departure.toUpperCase()} ${item.status.toUpperCase()}`;
  //     const textData = text.toUpperCase();
  //     return itemData.indexOf(textData) > -1;
  //   });

  //   this.setState({
  //     data: newData,
  //     isLoading: false
  //   });
  // }

  /************************************ clearList function ***********************************/
  clearList = () => {
    console.log('onCancel');
    this.setState({
      data: [],
      keyword: '',
      text: '',
      dataFirst: [],
      page: 1,
      searchLoading: true,
      dataFilter: [],
      searchFilter: [],
      selectValue: global.t('All'),
      searchFilterData: []


    })
  }


  /************************************ scrollToTop function ***********************************/
  scrollToTop() {
    console.log('----------scrollTo')
    this._scrollView.scrollTo({ x: 100, animated: true });
  }

  _onScroll(e) {
    let offsetY = e.nativeEvent.contentOffset.y;

    if (offsetY > 50) {
      this.setState({
        isShowToTop: true
      })
    } else {
      this.setState({
        isShowToTop: false
      })
    }
  }

  /************************************ sharing function ***********************************/
  sharing(item) {
    const { langPrint } = this.state;
    console.log('sharing');
    Share.share(
      {
        message: langPrint === 'zh' ?
          `${global.t('Destination')}: ${item.cn_title}` + "\n" +
          `${global.t('Tour_Code')}: ${item.tour_code}` + "\n" +
          `${global.t('Departure')}: ${item.departure}` + "\n" +
          `${global.t('Airline')}: ${item.airline === null ? localdata[0].airlinelocal_cn : item.airline}` + "\n" +
          `${global.t('Status')}: ${item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}` + "\n" +
          `${global.t('Price')}: RM ${item.price}` + "\n" +
          `${global.t('Itinerary')}: ${item.itinerary}`
          :
          `Destination: ${item.title}` + "\n" +
          `Tourcode: ${item.tour_code}` + "\n" +
          `Departure: ${item.departure}` + "\n" +
          `Airline: ${item.airline === null ? localdata[0].airlinelocal_en : item.airline}` + "\n" +
          `Status: ${item.status === "" ? localdata[0].statuslocal_en : item.status}` + "\n" +
          `Price: RM ${item.price}` + "\n" +
          `Itinerary: ${item.itinerary}`
        ,
        title: 'Golden Destinations',
        url: item.itinerary
      }
    )
  }

  /************************************ addFavorite function ***********************************/
  addFavorite(item, index, tourcode = item.tour_code, access_token = this.state.tokenId) {
    console.log(tourcode, 'tourcode', access_token, 'addFavorite---add')
    let items = this.state.data;
    let obj = this;
    let url = `https://www.goldendestinations.com/api/new/like.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

    fetch(url).then((res) => res.json())
      .then(function (resJson) {
        console.log(resJson, '-----------resJson add');
        if (resJson[0].status === 1) {
          console.log(resJson, 'ok-------------add');
          fountIndex = items.findIndex(x => items.tour_code === tourcode);
          item.isFavourite = "true";
          items[fountIndex] = item;
          obj.setState({
            data: items
          })
        }
        else if (access_token === null) {
          alert(global.t('pls_login_add'))
        }
        else {
          alert(resJson[0].error)
        }
      })

    console.log('add')
  }

  /************************************ removeFavorite function ***********************************/
  removeFavorite(item, index, tourcode = item.tour_code, access_token = this.state.tokenId, ) {
    console.log(tourcode, 'tourcode', access_token, 'removeFavorite---remove')
    let items = this.state.data;
    let obj = this;
    let url = `https://www.goldendestinations.com/api/new/unlike.php?gdsecode=GDFX2019213VJVCQWDCZXZUIQW&tourcode=${tourcode}&access_token=${access_token}`;

    fetch(url).then((res) => res.json())
      .then(function (resJson) {
        console.log(resJson, '-----------resJson remove');
        if (resJson[0].status === 1) {
          console.log(resJson, 'remove-------------remove');
          fountIndex = items.findIndex(x => x.tour_code === tourcode);
          item.isFavourite = "false";
          items[fountIndex] = item;
          obj.setState({
            data: items
          })
        }
        else {
          alert(resJson[0].error)
        }
      })

    console.log('remove')
  };




  /*************************************** Download PDF File ***************************************/
  downloadFile(item) {
    function getLocalPath(url) {
      const filename = url.split('/').pop();
      return `${RNFS.DocumentDirectoryPath}/${filename}`;
    }

    const url = item.itinerary;
    const localFile = getLocalPath(url);

    const options = {
      fromUrl: url,
      toFile: localFile
    };
    RNFS.downloadFile(options).promise
      .then(() => FileViewer.open(localFile))
      .then(() => {
        // success
      })
      .catch(error => {
        // error
      });
  }


  /************************************ _renderItem function ***********************************/
  _renderItem = ({ item, index, }) => {
    const { langPrint, toggalFavorite, } = this.state;

    return (
      item.status === 0 ? null :
        <Card key={item.index}>
          <CardItem style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#fcf5f5', marginRight: 0 }}>
            <Body style={{ marginRight: 0, }}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ alignItems: 'flex-start', width: '5%', }}>
                  <Image source={{ uri: item.brand_logo }} style={{ width: 20, height: 20 }} />
                </View>
                <View style={{ width: '95%' }}>
                  <Text note style={{ fontWeight: 'bold', textAlign: 'center', marginLeft: 5 }}>
                    {langPrint === 'zh' ? item.cn_title : item.title}
                  </Text>
                </View>
              </View>
            </Body>
          </CardItem>
          <CardItem cardBody >
            <View style={{ flexDirection: 'row' }}>
              <View style={{ paddingTop: 10, paddingBottom: 10, paddingLeft: 15, paddingRight: 15 }}>
                <View style={styles.textWarp}>
                  <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Tour_Code')}: </Text>
                  <Text note style={styles.TextInner2}>{item.tour_code}</Text>
                </View>
                <View style={styles.textWarp}>
                  <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Airline')}: </Text>
                  {
                    langPrint === 'zh' ? (
                      <Text note style={styles.TextInner2}>{item.airline === null ? localdata[0].airlinelocal_cn : item.airline}</Text>
                    ) : (
                        <Text note style={styles.TextInner2}>{item.airline === null ? localdata[0].airlinelocal_en : item.airline}</Text>
                      )
                  }

                </View>
                <View style={styles.textWarp}>
                  <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Status')}: </Text>
                  {
                    langPrint === "zh" ? (
                      <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                        {item.status_cn === "" ? localdata[0].statuslocal_cn : item.status_cn}
                      </Text>
                    ) : (
                        <Text note style={[styles.TextInner2, { fontWeight: 'bold', color: '#ff9601' }]}>
                          {item.status === "" ? localdata[0].statuslocal_en : item.status}
                        </Text>
                      )
                  }
                </View>
                <View style={styles.textWarp}>
                  <Text note style={{ width: '30%', marginTop: 2 }}>{global.t('Departure')}: </Text>
                  <Text note style={styles.TextInner2}>{item.departure}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 2 }}>
                  <Text note style={{ width: '30%', color: '#00953b', marginTop: 2 }}>{global.t('Price')}: </Text>
                  <Text note style={styles.Textprice}>{item.price}</Text>
                </View>
              </View>

            </View>
          </CardItem>
          <CardItem style={{ borderTopWidth: .5, borderTopColor: '#ccc' }}>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: '33.33%', }}>
                <TouchableOpacity onPress={item.isFavourite === "true" ? this.removeFavorite.bind(this, item) : this.addFavorite.bind(this, item)}>
                  <IconFOU name="star" size={20} style={{
                    textAlign: 'center',
                    color: item.isFavourite === "true" ? "#ffc43f" : "#ccc"
                  }} />
                </TouchableOpacity>

              </View>
              <View style={{ width: '33.33%', borderLeftWidth: .5, borderLeftColor: '#ccc', borderRightColor: '#ccc', borderRightWidth: .5 }}>
                <TouchableOpacity onPress={this.sharing.bind(this, item)} >
                  {/* <TouchableOpacity onPress={() => this.renderChoices()} > */}
                  <IconFOU name="share" size={20} style={{ textAlign: 'center', color: '#0095ff' }} />
                </TouchableOpacity>
              </View>
              <View style={{ width: '33.33%' }}>
                <TouchableOpacity onPress={this.downloadFile.bind(this, item)}>
                  <IconFOU name="download" size={20} style={{ textAlign: 'center', color: '#de2d30' }} />
                </TouchableOpacity>
              </View>
            </View>

          </CardItem>
        </Card>
    )
  }

  /************************************ ListEmpty function ***********************************/
  ListEmpty = () => {
    return (
      <View style={styles.MainContainer}>
        <IconANT name="unknowfile1" size={60} style={{ textAlign: 'center', marginBottom: 10, color: '#ccc' }} />
        <Text style={{ textAlign: 'center', color: '#a9a9a9' }}>{global.t('No_Data_Found')}</Text>
      </View>
    );
  };


  renderFooter = () => {
    return (
      this.state.loadingScroll ?
        <View style={styles.loadingFooter}>
          <ActivityIndicator size="large" color="#de2d30" />
        </View> : null
    )
  }


  handleLoadMore = () => {
    console.warn('handleLoadMore');

    setTimeout(() => {
      this.setState({
        page: this.state.page + 1,
        loadingScroll: true
      }, this.getData)

    }, 1000)

  }

  // updataValue(text, field) {
  //   console.log(text)
  //   if (field == 'search_value') { this.setState({ search_value: text }) }

  // }


  render() {

    const { data, searchFilterData, isLoading, selectValue, keyword, langPrint, dataFirst, dataFilter, searchFilter, searchLoading } = this.state;
    //console.log(favoriteData, '-------favoriteData');
    //console.log(error, '-------error')
    //console.log(this.state.favoriteList, '-------favoriteList')
    console.log(data, '-------data')
    console.log('searchFilter', searchFilter)
    console.log(searchFilterData, 'searchFilterData')

    // this.state.dataFirst.map((item) =>{
    //   item.isFavourite = false
    //   return item
    // })

    return (

      <View style={{ backgroundColor: keyword === "" ? '#fff' : 'transparent', flex: 1 }}>
        {/* <Search
            ref="search_box"
            onSearch={this.onSearch}
            backgroundColor="#ccc"
            onChangeText={(text) => this.onSearch(text)}
            //autoCorrect={false}
            onCancel={this.clearList}
            afterCancel={this.clearList}
            onDelete={this.clearList}
            afterDelete={this.clearList}
            value={keyword}
            placeholder={global.t('search')}
            cancelTitle={global.t('cancel')}
            titleCancelColor="#000"
          //autoCapitalize="characters"
          /> */}

        <View style={{ flexDirection: 'row', backgroundColor: '#fff', padding: 5, borderBottomWidth: .5, borderBottomColor: '#ccc', }}>
          <Item style={{
            width: '80%', borderBottomWidth: 0, backgroundColor: '#e2e2e2',
            paddingLeft: 10, paddingRight: 10, height: 35, borderRadius: 50, marginTop: 5
          }}>
            <Icon name="ios-search" />
            <TextInput
              placeholder={global.t('search_input')} placeholderTextColor='#a9a9a9'
              onChangeText={(text) => this.setState({ text })}
              value={this.state.text}
              style={{ height: 40, width: '85%' }}
            />

            <TouchableOpacity onPress={() => this.clearList()}>
              <IconANT name="closecircle" size={18} />
            </TouchableOpacity>
          </Item>
          <Button transparent style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.searchKeyword()}>
            <Text style={{ fontSize: 13, paddingLeft: 0, paddingRight: 0, textAlign: 'center' }}>{global.t('search')}</Text>
          </Button>
        </View>



        {/* <ScrollView ref={view => this._scrollView = view} onScroll={(e) => this._onScroll(e)}> */}
        <View >
          {
            isLoading ? (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                <ActivityIndicator size="large" color="#de2d30" />
              </View>
            ) : (
                keyword === '' ? (
                  // <View>
                  //   <Select
                  //     onSelect={this.onSelect.bind(this)}
                  //     defaultText={(<Text>
                  //       <Text style={{color: '#de2d30', }}>{selectValue}   </Text>
                  //       <IconENT name='triangle-down' style={{ fontSize: 15, color: '#de2d30', }} />
                  //     </Text>)}
                  //     style={styles.selectSty}
                  //     textStyle={styles.selectText}
                  //     backdropStyle={{ backgroundColor: "#00000078" }}
                  //     optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                  //     animationType='none'
                  //     transparent={true}
                  //     indicator='none'
                  //   // indicatorColor='#de2d30'
                  //   // indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                  //   // indicatorStyle={{ marginTop: Platform.OS === 'ios' ? -15 : -15, marginLeft: -10 }}
                  //   >
                  //     {
                  //       selectData.map((item) => {
                  //         return (
                  //           <Option value={langPrint === "zh" ? item.select_cn : item.select_en} >
                  //             {langPrint === "zh" ? item.select_cn : item.select_en}
                  //             {/* {item.select_en} */}
                  //           </Option>
                  //         )
                  //       })
                  //     }
                  //   </Select>
                  //   <FlatList
                  //     data={selectValue === 'Bookable' || selectValue === '可预订' ? dataFilter : dataFirst}
                  //     extraData={this.state}
                  //     renderItem={this._renderItem}
                  //     keyExtractor={(item, index) => index.toString()}
                  //     ListFooterComponent={this.renderFooter}
                  //     ListEmptyComponent={this.ListEmpty}
                  //     // contentInset= {{bottom: 200}}
                  //     contentContainerStyle={{ paddingBottom: 250 }}
                  //   />
                  // </View>

                  <Upcoming />

                ) : (
                    searchLoading ? (
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                        <ActivityIndicator size="large" color="#de2d30" />
                      </View>
                    ) : (
                        <View>
                          <Select
                            onSelect={this.onSelect.bind(this)}
                            defaultText={(<Text>
                              <Text style={{ color: '#de2d30', }}>{selectValue}   </Text>
                              <IconENT name='triangle-down' style={{ fontSize: 15, color: '#de2d30', }} />
                            </Text>)}
                            style={styles.selectSty}
                            textStyle={styles.selectText}
                            backdropStyle={{ backgroundColor: "#00000078" }}
                            optionListStyle={{ backgroundColor: "#F5FCFF", height: 250, }}
                            animationType='none'
                            transparent={true}
                            indicator='none'
                          // indicatorColor='#de2d30'
                          // indicatorSize={Platform.OS === 'ios' ? 10 : 10}
                          // indicatorStyle={{ marginTop: Platform.OS === 'ios' ? -15 : -15, marginLeft: -10 }}
                          >
                            {
                              selectData.map((item) => {
                                return (
                                  <Option value={langPrint === "zh" ? item.select_cn : item.select_en} >
                                    {langPrint === "zh" ? item.select_cn : item.select_en}
                                    {/* {item.select_en} */}
                                  </Option>
                                )
                              })
                            }
                          </Select>
                          <FlatList
                            data={selectValue === 'Bookable' || selectValue === '可预订' ? searchFilterData : data}
                            extraData={this.state}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => index.toString()}
                            ListEmptyComponent={this.ListEmpty}
                            onEndReached={this.handleLoadMore}
                            onEndReachedThreshold={0.5}
                            // initialNumToRender="10"
                            ListFooterComponent={this.renderFooter}
                            contentContainerStyle={{ paddingBottom: 250 }}
                          />
                        </View>
                      )


                  )
              )
          }

          {/* </ScrollView> */}

          {
            this.state.isShowToTop ? (
              <TouchableOpacity onPress={() => this.scrollToTop()} style={styles.scrollBtn}>
                <IconANT name='up' style={styles.iconTop} />
              </TouchableOpacity>

            ) : (
                null
              )
          }

        </View>
        {/* <SafeAreaView style={{ backgroundColor: '#fc564e' }} /> */}



      </View>

    );
  }
}


export default SearchList;

const styles = StyleSheet.create({
  searchContainer: { backgroundColor: 'white', },
  listWarp: { marginBottom: 40 },
  IconColor: { color: '#de2d30', textAlign: 'center', paddingTop: 30, },
  TextTitle: { color: '#de2d30', fontWeight: 'bold', },
  TextInner: { width: '70%', paddingRight: 3, textTransform: 'capitalize' },
  TextInner2: { color: '#606c77', width: '70%', paddingRight: 3, marginTop: 2 },
  Textprice: { color: '#00953b', width: '50%', marginTop: 2 },
  Textshare: { width: '20%', },
  Textsharetext: { color: '#0095ff', fontWeight: 'bold', textAlign: 'center' },
  textLoc: { textTransform: 'uppercase', width: '70%', paddingRight: 5 },
  textWarp: { flexDirection: 'row', marginTop: 2, },
  downbtn: { color: '#de2d30', textAlign: 'center', padding: 10 },
  dwnwarp: { width: '100%', },
  bodystyle: {
    width: '100%', marginTop: 0, marginBottom: 0, paddingBottom: 0, marginLeft: 0,
    paddingTop: 0, borderBottomWidth: 0
  },
  texttitlestyle: { textAlign: 'center', paddingTop: 10, paddingBottom: 10, color: '#000', fontWeight: 'bold' },
  dwntext: { marginTop: 15, textAlign: 'center', marginRight: 0, color: '#de2d30' },
  scrollBtn: {
    position: 'absolute', flex: 0.1, right: 10, bottom: -10, borderRadius: 50,
    backgroundColor: '#00000057', padding: 10, width: 50, height: 50, marginBottom: 100,
    color: 'red', textAlign: 'center'
  },
  iconTop: { fontSize: 22, color: 'white', textAlign: 'center', fontWeight: 'bold', paddingTop: 3 },
  MainContainer: { justifyContent: 'center', flex: 1, marginTop: 200, },
  loadingFooter: { marginTop: 10, alignItems: 'center', marginBottom: 100 },
  selectSty: {
    borderBottomWidth: .5, borderTopWidth: 0.5, width: '100%',
    backgroundColor: '#fff', borderColor: '#ccc', borderLeftWidth: 0, borderRightWidth: 0
  },
  selectText: { color: '#de2d30', textAlign: 'center', width: '100%', fontWeight: 'bold' },


})
