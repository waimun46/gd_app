Project Run

# Android
- Download and install Android Studio 	
	* Reference( https://facebook.github.io/react-native/docs/getting-started )
	 	-(Make sure the boxes next to all of the following are checked)
	* Android SDK
	* Android SDK Platform
	* Performance (Intel ® HAXM) 
	* Android Virtual Device
	- In Android SDK select
		* Android 6.0 (Marshmallow)
		* Click Show Package Details
		* Select 
			- Google APIs
			- Android SDK Platform 23
			- Sources for Android 23
			- Intel x86 Atom_64 System Image
			- Google API Intel x86 Atom_64 System Image
		* Select SDK Tools
			- Click Show Package Details
			- In Android SDK Build-Tools 
				- Select 23.0.1
	* Click ok to install 
	* Open AVD Manager
	* Select the Nexus 5X API 28 X86 Phone in Action, click the pencil to edit emulator
	* Select the second list and click the change
	* Select Marshmallow  23  x86   Android 6.0 (Goolge)
	* Click ok and Finish
	* Click play button in Actions to open the emulator ( must install java jdk8 first )
	* Set ANDROID_HOME environment variable
	* Add the following lines to your $HOME/.bash_profile config file:
		export ANDROID_HOME=$HOME/Library/Android/sdk
		export PATH=$PATH:$ANDROID_HOME/emulator
		export PATH=$PATH:$ANDROID_HOME/tools
		export PATH=$PATH:$ANDROID_HOME/tools/bin
		export PATH=$PATH:$ANDROID_HOME/platform-tools
	*Reference( https://www.youtube.com/watch?v=Q0dERWCzoi0 )

- Install JAVA JDK8
	* Reference( https://www.youtube.com/watch?v=y6szNJ4rMZ0 )
	* Add the following lines to your $HOME/.bash_profile config file:
		export JAVA_HOME=$(/usr/libexec/java_home)	

# IOS
- Install XCODE 
	https://developer.apple.com/download/


# File open
- Open you Project file yarn install or npm install
- Open your android emulator
- react-native run-android  /  react-native run-ios 

