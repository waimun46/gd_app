/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import OneSignal from 'react-native-onesignal';
import { Platform, StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, TextInput, BackHandler } from 'react-native';
import { Icon, StyleProvider, Container, Root } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import getTheme from './src/themes/components';
import appColor from './src/themes/variables/appColor';
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import IconANT from "react-native-vector-icons/AntDesign";
import IconMAT from "react-native-vector-icons/MaterialIcons";
import IconSIM from "react-native-vector-icons/SimpleLineIcons";
import IconFA from "react-native-vector-icons/FontAwesome";
import { Router, Scene, Actions, Lightbox } from 'react-native-router-flux';
import HTML from 'react-native-render-html';

import WelcomePage from './src/components/pages/welcome';
import LoginModal from './src/components/pages/login';
import GdSeries from './src/components/pages/gdseries';
import GDPremium from './src/components/pages/gdseries/components/gdpremium';
import CountryInfor from './src/components/pages/gdseries/components/components/countryinfor';
import CountryInforList from './src/components/pages/gdseries/components/components/countryinforlist';
import HomesScreen from './src/components/pages/home';
import InforScreen from './src/components/pages/infor';
import ProfilePage from './src/components/pages/profile';
import Newspage from './src/components/pages/news';
import Agent from './src/components/pages/agent';
import AgentInfor from './src/components/pages/agent/components/agentinfor';
import DepartureLogin from './src/components/pages/departure/departurelogin';
import DepartureInfor from './src/components/pages/departure';
import ViewInfor from './src/components/pages/news/components/components/viewinfor';
import EmailVailed from './src/components/pages/emailvailed';
import RegisterScreen from './src/components/pages/register';
import ScanPage from './src/components/pages/scan';
import ViewDetailData from './src/components/pages/gdseries/components/components/viewdetailsData';
import CompanyProfile from './src/components/pages/infor/components/companyProfile';
import Feedback from './src/components/pages/infor/components/feedback';
import Privacy from './src/components/pages/infor/components/privacy';
import TermsCondition from './src/components/pages/infor/components/terms';
import GDConsortiums from './src/components/pages/infor/components/gdconsortiums';
import ViewDetailUpcoming from './src/components/pages/home/components/components/viewDetail';
import DateList from './src/components/pages/home/components/components/dateList';
import ModalScreen from './src/components/pages/departure/components/components/modalScreen';
import ChangePassword from './src/components/pages/profile/components/changePass';
import HightLineData from './src/components/pages/home/components/components/hightlineData';
import ListHotPick from './src/components/pages/home/components/components/moreList';
import SearchList from './src/components/pages/search';
import AddFavorite from './src/components/pages/infor/components/favorite';
import GtripList from './src/components/pages/gdseries/components/components/gtripList';
import SeriesListing from './src/components/pages/series/listing';
import SeriesShowPage from './src/components/pages/series/showpage';
import ForgetPassword from './src/components/pages/login/components/forgetPassword';
import ViewSelectList from './src/components/pages/home/components/components/viewSelectList';
import { UserProfileApi } from './PostApi';



/************************************ Not allow Font Scaling when Phone Setting font size ***********************************/
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

/************************************ Not allow Text input font Scaling when Phone Setting font size  ***********************************/
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;

/************************************ Not allow HTML api font Scaling when Phone Setting font size  ***********************************/
HTML.defaultProps = HTML.defaultProps || {};
HTML.defaultProps.allowFontScaling = false;


const iconLogo = () => (
  <Image source={require('./src/assets/images/logo.png')} style={{ width: 60, height: 60, marginTop: -30 }} />
)

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{ color: selected ? '#de2d30' : '#d6cccc' }}>{title}</Text>
  );
}

const Rightbtn = () => {
  return (
    <View style={{ flex: 1, flexDirection: 'row' }}>
      <IconANT name='search1' style={{ fontSize: 22, paddingRight: 15, color: 'white' }} onPress={() => Actions.search()} />
      <IconANT name='scan1' style={{ fontSize: 22, paddingRight: 15, color: 'white' }} onPress={() => Actions.scan()} />
      <IconANT name='infocirlceo' style={{ fontSize: 22, paddingRight: 15, color: 'white' }} onPress={() => Actions.Infor()} />
      <IconMCI name='account' style={{ fontSize: 22, paddingRight: 10, color: 'white' }} onPress={() => Actions.profile()} />
    </View>
  );
}



class App extends Component {

  constructor(props, properties) {
    super(props, properties);
    OneSignal.setLogLevel(6, 0);
    OneSignal.setSubscription(true);
    OneSignal.init("c22d64fd-d8d5-4f79-aece-cb42b17df336");
    OneSignal.registerForPushNotifications();
    this.state = {
      tokenId: 0,
      data: []
    }
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }


  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);

  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  

  componentDidMount() {

    OneSignal.getPermissionSubscriptionState((status) => {
      console.log("OneSignal----------status", status);
      console.log("OneSignal----------status===", status.subscriptionEnabled);
      // if(status.subscriptionEnabled === false){
        
      // }
    });

  

    OneSignal.checkPermissions((permissions) => {
      console.log("permissions=========",permissions);
    });

    /************************************ Get Store Data From AsyncStorage  ***********************************/
    AsyncStorage.getItem('USER_TOKEN').then(asyncStorageRes => {
      console.log(asyncStorageRes, '----------------profileId asyncStorageRes')
      this.setState({
        tokenId: asyncStorageRes
      })

      //   /************************************ Fetch Api Data  ***********************************/
      //   UserProfileApi(this.state.profileId).then((fetchData) => {
      //     console.log(fetchData, '-----------------profileId')
      //     this.setState({
      //       data: fetchData[0]
      //     })
      //   }).catch((error) => {
      //     console.log(error, 'profileId----------profileId');
      //     this.setState({
      //       catcherr: error.message
      //     });
      //   });

    });



  }



  /************************************ profile Logout btn ***********************************/
  logoutBtn = () => {
    return (
      <IconANT name='logout' style={{ fontSize: 22, paddingRight: 15, color: 'white' }} onPress={() => this._logout()} />
    )
  }

  /************************************ profile Logout function ***********************************/
  _logout() {
    AsyncStorage.removeItem('USER_TOKEN');
    Actions.Home();
    // RNRestart.Restart()
  }


  render() {

    const { profileId, data } = this.state;
    //console.log(this.state.profileId, '----------tokenId')

    return (
      <StyleProvider style={getTheme(appColor, getTheme)}>
        <Container >

          <StatusBar barStyle="light-content" backgroundColor="#de2d30" />
          <Root >
            <Router hideNavBar navigationBarStyle={{ backgroundColor: '#de2d30' }}
              iosBarStyle={"light-content"} tintColor='white'>
              <Lightbox >
                {/* 
              <Scene key="modal" modal   component={ModalScreen} hideTabBar={true}
                direction="vertical" hideNavBar={true} navTransparent={true}
                sceneStyle={{ backgroundColor: 'transparent' }} headerMode='screen' /> */}




                <Scene key="root" hideNavBar allowFontScaling={false} headerTitleAllowFontScaling={false}>

                  <Scene key="welcome" component={WelcomePage} initial={true} hideNavBar />

                  <Scene
                    headerLayoutPreset="left"
                    tabs={true}
                    icon={TabIcon}
                    titleStyle={{ color: 'white', fontSize: 16 }}
                    tabBarStyle={{ backgroundColor: '#fcf5f5' }}
                    swipeEnabled={true}
                    activeTintColor={'#de2d30'}
                    key="Homepage"
                  >


                    {/************************  home ************************/}
                    <Scene
                      tabBarLabel={global.t('Home')}
                      icon={({ focused }) => (
                        <IconANT name='home' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                      )}
                      key="Home"
                    >
                      {/* title={profileId === null ? global.t('Golden_Destination') : "Hi, " + data.name} */}

                      <Scene key="Home" initial={true}   renderBackButton={() => (null)} renderLeftButton={() => (null)} component={HomesScreen}
                        title={global.t('Golden_Destination')} renderRightButton={Rightbtn} />
                      <Scene headerLayoutPreset="center" titleStyle={{ color: 'white', fontSize: 16 }} tabBarStyle={{ backgroundColor: '#fcf5f5' }}
                        swipeEnabled={true} activeTintColor={'#de2d30'} key="register" component={RegisterScreen} hideTabBar={true}
                      />
                      <Scene key="success" renderBackButton={() => (null)} renderLeftButton={() => (null)} component={EmailVailed} hideNavBar={false} hideTabBar={true} />
                      <Scene key="moreList" component={ListHotPick} title={global.t('Hot_Pick_List')} renderRightButton={Rightbtn} />
                      <Scene key="forgetPass" component={ForgetPassword} title={global.t('Forget_Password')} hideTabBar={true} />

                      <Scene key="gdSeries" component={GdSeries} title={global.t('Series_Tour')} renderRightButton={Rightbtn} />
                      <Scene key="countryinforlist" component={CountryInforList} title={global.t('Package_Tour')} renderRightButton={Rightbtn} />
                      <Scene key="upcomingdetail" component={ViewDetailUpcoming} title={global.t('Upcoming_Package')} renderRightButton={Rightbtn} />
                      <Scene key="datelist" component={DateList} title={global.t('Upcoming_Package')} renderRightButton={Rightbtn} />
                      <Scene key="hightLine" component={HightLineData} title={global.t('Package_List')} renderRightButton={Rightbtn} />
                      <Scene key="seriesListing" component={SeriesListing} title={global.t('Tour_Package')} renderRightButton={Rightbtn} />
                      <Scene key="seriesShowpage"  component={SeriesShowPage} title={global.t('Tour_Package')} renderRightButton={Rightbtn} />

                      <Scene key="viewSelectList" modal component={ViewSelectList} hideTabBar={true}
                        direction="vertical" hideNavBar={true} navTransparent={true}
                        sceneStyle={{ backgroundColor: '#000' }} headerMode='screen'
                      />

                      {/*--- Rightbtn Screen ---*/}
                      <Scene key="search" component={SearchList} title={global.t('search')} hideTabBar={true} />
                      <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                      <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                      <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} renderRightButton={this.logoutBtn} />
                      <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />


                      {/*--- Infor Screen content ---*/}
                      <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                      <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                      <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                      <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                      <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                      <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                      <Scene key="favorite" component={AddFavorite} title={global.t('Wish_List')} hideTabBar={true} />

                    </Scene>


                    {/************************  News ************************/}
                    <Scene
                      tabBarLabel={global.t('News')}
                      icon={({ focused }) => (
                        <IconFA name='newspaper-o' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                      )}
                      key="Newspage"
                    >
                      <Scene key="News" component={Newspage} initial={true} title={global.t('News')} renderRightButton={Rightbtn} />
                      <Scene key="viewinfor" component={ViewInfor} title={global.t('News_Information')} renderRightButton={Rightbtn} />


                      {/*--- Rightbtn Screen ---*/}
                      <Scene key="search" component={SearchList} title={global.t('search')} hideTabBar={true} />
                      <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                      <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                      <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} renderRightButton={this.logoutBtn} />
                      <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                      {/*--- Infor Screen content ---*/}
                      <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                      <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                      <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                      <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                      <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                      <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                      <Scene key="favorite" component={AddFavorite} title={global.t('Wish_List')} hideTabBar={true} />
                    </Scene>



                    {/************************  GdSeries ************************/}
                    <Scene
                      tabBarLabel=" "
                      icon={iconLogo}
                    >
                      <Scene key="gdSeries" initial={true} component={GdSeries} title={global.t('Series_Tour')} renderRightButton={Rightbtn} />
                      <Scene key="gdPremium" component={GDPremium} title={global.t('Series_Selection')} renderRightButton={Rightbtn} />
                      <Scene key="countryinfor" component={CountryInfor} title={global.t('Country_Information')} renderRightButton={Rightbtn} />
                      <Scene key="gtriplist" component={GtripList} title={global.t('Country_Information')} renderRightButton={Rightbtn} />
                      <Scene key="countryinforlist" component={CountryInforList} title={global.t('Package_Tour')} renderRightButton={Rightbtn} />
                      <Scene key="viewdetails" component={ViewDetailData} title={global.t('Package_List')} renderRightButton={Rightbtn} />

                      {/*--- Rightbtn Screen ---*/}
                      <Scene key="search" component={SearchList} title={global.t('search')} hideTabBar={true} />
                      <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                      <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                      <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} renderRightButton={this.logoutBtn} />
                      <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                      {/*--- Infor Screen content ---*/}
                      <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                      <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                      <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                      <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                      <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                      <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                      <Scene key="favorite" component={AddFavorite} title={global.t('Wish_List')} hideTabBar={true} />
                    </Scene>


                    {/************************  Agent ************************/}
                    <Scene
                      tabBarLabel={global.t('gd_partners')}
                      icon={({ focused }) => (
                        <IconSIM name='people' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                      )}
                    >
                      <Scene key="Agent" component={Agent} title={global.t('gd_partners')} initial={true} renderRightButton={Rightbtn} />
                      <Scene key="Agentinfor" component={AgentInfor} title={global.t('partners_infor')} renderRightButton={Rightbtn} />

                      {/*--- Rightbtn Screen ---*/}
                      <Scene key="search" component={SearchList} title={global.t('search')} hideTabBar={true} />
                      <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                      <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                      <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} renderRightButton={this.logoutBtn} />
                      <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />

                      {/*--- Infor Screen content ---*/}
                      <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                      <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                      <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                      <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                      <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                      <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                      <Scene key="favorite" component={AddFavorite} title={global.t('Wish_List')} hideTabBar={true} />
                    </Scene>

                    {/************************  Departure ************************/}
                    <Scene
                      tabBarLabel={global.t('Departure_Info')}
                      icon={({ focused }) => (
                        <IconMAT name='flight-takeoff' size={25} style={{ color: focused ? '#de2d30' : '#d6cccc' }} />
                      )}
                    >
                      {/* <Scene key="Departurelogin" initial={true} component={DepartureLogin} title={global.t('Departure_Login')} renderRightButton={Rightbtn} />
                    <Scene key="Departure" component={DepartureInfor} title={global.t('Departure_Info')} renderRightButton={Rightbtn} /> */}

                      <Scene key="Departurelogin" renderBackButton={() => (null)} renderLeftButton={() => (null)} initial={true} component={DepartureLogin} title={global.t('Departure_Login')} renderRightButton={Rightbtn} />
                      <Scene key="Departure" renderBackButton={() => (null)} renderLeftButton={() => (null)} component={DepartureInfor} title={global.t('Departure_Info')} renderRightButton={Rightbtn} />

                      {/*--- Rightbtn Screen ---*/}
                      <Scene key="search" component={SearchList} title={global.t('search')} hideTabBar={true} />
                      <Scene key="scan" component={ScanPage} title={global.t('Scan_QRCode')} hideTabBar={true} />
                      <Scene key="Infor" component={InforScreen} title={global.t('Information')} hideTabBar={true} />
                      <Scene key="profile" component={ProfilePage} title={global.t('Personal_Profile')} hideTabBar={true} renderRightButton={this.logoutBtn} />
                      <Scene key="changepassword" component={ChangePassword} title={global.t('Change_Password')} hideTabBar={true} />


                      {/*--- Infor Screen content ---*/}
                      <Scene key="company" component={CompanyProfile} title={global.t('Company_Profile')} hideTabBar={true} />
                      <Scene key="consortiums" component={GDConsortiums} title={global.t('GD_Consortiums')} hideTabBar={true} />
                      <Scene key="privacy" component={Privacy} title={global.t('Privacy_Policy')} hideTabBar={true} />
                      <Scene key="terms" component={TermsCondition} title={global.t('Terms&Condition')} hideTabBar={true} />
                      <Scene key="feedback" component={Feedback} title={global.t('Feedback')} hideTabBar={true} />
                      <Scene key="Agentinforgd" component={AgentInfor} title={global.t('Agent_Information')} hideTabBar={true} />
                      <Scene key="favorite" component={AddFavorite} title={global.t('Wish_List')} hideTabBar={true} />
                    </Scene>



                  </Scene>



                </Scene>
              </Lightbox>
            </Router>
          </Root>
        </Container>
      </StyleProvider>

    );
  }
}
export default App;

